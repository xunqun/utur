package com.whilerain.utur.utils;

import android.content.Context;

import com.google.gson.Gson;

/**
 * Created by xunqun on 2015/11/15.
 */
public class SimpleDataCache<T extends SimpleDataCache.CachePoJo> {
    public void set(Context context, Class<T> tclass, String content){
        SharePrefHandler.getEditor(context).putString(tclass.getSimpleName(), content).commit();
    }

    public T get(Context context, Class<T> tclass){
        String userInfoJson = SharePrefHandler.getSharedPrefences(context).getString(tclass.getSimpleName(), "");
        T userInfo = null;
        if (userInfoJson.length() > 0) {
            Gson gson = new Gson();
            userInfo = gson.fromJson(userInfoJson, tclass);
        }
        return userInfo;
    }

    public static class CachePoJo{};
}
