package com.whilerain.utur.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by xunqun on 2015/10/29.
 */
public class SharePrefHandler {

    public static final String APP_MODE = "APP_MODE";
    public static final String ACCOUNT ="ACCOUNT";
    public static final String GCM_TOKEN = "GCM_TOKEN";
    public static final String TOTURIAL_SHOWN = "TOTURIAL_SHOWN";

    private static String file_key = "raise.pref";

    public static SharedPreferences getSharedPrefences(Context context){
        return getSharedPrefences(context, file_key);
    }

    public static SharedPreferences getSharedPrefences(Context context, String file){
        return context.getSharedPreferences(file, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor(Context context){
        return getEditor(context, file_key);
    }

    public static SharedPreferences.Editor getEditor(Context context, String file){
        return getSharedPrefences(context, file).edit();
    }

}
