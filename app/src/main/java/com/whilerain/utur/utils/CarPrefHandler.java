package com.whilerain.utur.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by xunqun on 2015/10/29.
 */
public class CarPrefHandler {

    public static final String APP_MODE = "APP_MODE";
    public static final String ACCOUNT ="ACCOUNT";

    private static String file_key = "car.pref";

    public static SharedPreferences getSharedPrefences(Context context){
        return context.getSharedPreferences(file_key, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor(Context context){
        return getSharedPrefences(context).edit();
    }
}
