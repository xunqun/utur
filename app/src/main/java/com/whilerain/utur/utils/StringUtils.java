package com.whilerain.utur.utils;

import android.content.Context;

import com.whilerain.utur.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by xunqun on 2017/1/17.
 */

public class StringUtils {
    public static String getAccountString(String countryCode, String contactNum){
        if(contactNum.charAt(0) == '0'){
            contactNum = contactNum.substring(1);
        }
        return countryCode + contactNum;
    }



    public static String localTimeToUtc(String format, String dateStr) throws ParseException{
        SimpleDateFormat df = new SimpleDateFormat(format);
        df.setTimeZone(TimeZone.getDefault());
        Date date = df.parse(dateStr);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = df.format(date);
        return formattedDate;
    }

    public static String utcToLocalTime(String inputformat,String outputFormat, String dateStr) throws ParseException {
        SimpleDateFormat idf = new SimpleDateFormat(inputformat);
        SimpleDateFormat odf = new SimpleDateFormat(outputFormat);
        idf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = idf.parse(dateStr);
        idf.setTimeZone(TimeZone.getDefault());
        String formattedDate = odf.format(date);
        return formattedDate;
    }

    public static String trimSpecialString(String t){
        return t.replace("\u0096", "");
    }

    public static String getStatusString(Context context, String status) {
        if(status.equalsIgnoreCase("proceeding")){
            return context.getString(R.string.state_proceeding);
        }else if(status.equalsIgnoreCase("queue")){
            return context.getString(R.string.state_queue);
        }else if(status.equalsIgnoreCase("ontheroad")){
            return context.getString(R.string.state_ontheroad);
        }else if(status.equalsIgnoreCase("aborted")){
            return context.getString(R.string.state_aborted);
        }else if(status.equalsIgnoreCase("finish")){
            return context.getString(R.string.state_finish);
        }else if(status.equalsIgnoreCase("accepted")){
            return context.getString(R.string.state_accepted);
        }else if(status.equalsIgnoreCase("paid")){
            return context.getString(R.string.state_paid);
        }
        return status;
    }

    public static String[] getCountryCodeAndPhoneViaAccount(String account){
        String[] result = new String[2];
        if(account.startsWith("86")) {// china
            result[0] = "86";
            result[1] = account.substring(2);
        }else{
            result[0] = account.substring(0, 3);
            result[1] = account.substring(3);
        }
        return result;
    }
}
