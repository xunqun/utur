package com.whilerain.utur.model;

/**
 * Created by xunqun on 2017/2/19.
 */

public class ApiReference {
    public enum TransType{
        TRANSPORT("transport"), SHARE("share"), PACKAGE("package"), TAXI("taxi");
        private String name;
        TransType(String n){
            name = n;
        }

        public String toString(){
            return name;
        }
    }
}
