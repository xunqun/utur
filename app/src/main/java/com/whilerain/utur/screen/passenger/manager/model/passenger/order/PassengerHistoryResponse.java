package com.whilerain.utur.screen.passenger.manager.model.passenger.order;

import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/4/24.
 */
public class PassengerHistoryResponse extends SimpleDataCache.CachePoJo {


    /**
     * status : true
     * result : {"orders":{"nowandthen":[{"id":"12","type":0,"category":"transport","order_id":"000106150000109000002","requestdate":"2016-06-23T00:00:00.000Z","start_time":"2016-06-23T01:00:00.000Z","finish_time":"2016-06-23T03:00:00.000Z","duration":120,"vacant_seat":3,"driver":{"name":"測試司機","picture":null,"phone":"886987654321"},"car":{"model":"Wish","licenseplate":"1234-XD","picture":null,"seat":4},"requests":[{"passenger_count":1,"travel":1,"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"end_location":{"gps":[21.9456877,120.7972097],"name":"墾丁","address":"屏東縣恆春鎮墾丁路237號","category":"station"},"name":"左營高鐵站 -> 墾丁","price":450,"discount":0.8,"passenger":{"name":"Shawn","phone":"886952117393"}}],"status":"proceeding"}],"wearedone":[]},"requests":{"nowandthen":[{"id":"15","request_id":"06160000113000005","transport_id":"000011300","travel":1,"date":"2016-06-23T00:00:00.000Z","timerange":["2016-06-23T05:00:00.000Z","2016-06-23T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"start_location_custom":null,"end_location":{"gps":[21.949965,120.778987],"name":"吉利通舖民宿","address":"屏東縣恆春鎮墾丁路602號","category":"hotel"},"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"左營高鐵站 -> 墾丁","price":450,"discount":1,"category":"transport"},{"id":"13","request_id":"06160000113000003","transport_id":"000011300","travel":1,"date":"2016-06-24T00:00:00.000Z","timerange":["2016-06-24T05:00:00.000Z","2016-06-24T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"start_location_custom":null,"end_location":{"gps":[21.9456877,120.7972097],"name":"墾丁","address":"屏東縣恆春鎮墾丁路237號","category":"station"},"end_location_custom":null,"passenger":3,"passenger_count":2,"status":"queue","name":"左營高鐵站 -> 墾丁","price":450,"discount":1,"category":"transport"},{"id":"34","request_id":"061780024","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","timerange":["2016-06-30T01:00:00.000Z","2016-06-30T04:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","price":2000,"category":"share"},{"id":"35","request_id":"061780025","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","timerange":["2016-06-30T01:00:00.000Z","2016-06-30T04:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","price":2000,"category":"share"},{"id":"33","request_id":"061780022","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","timerange":["2016-06-30T05:00:00.000Z","2016-06-30T08:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","price":2000,"category":"share"}],"wearedone":[]}}
     */

    private boolean status;
    /**
     * orders : {"nowandthen":[{"id":"12","type":0,"category":"transport","order_id":"000106150000109000002","requestdate":"2016-06-23T00:00:00.000Z","start_time":"2016-06-23T01:00:00.000Z","finish_time":"2016-06-23T03:00:00.000Z","duration":120,"vacant_seat":3,"driver":{"name":"測試司機","picture":null,"phone":"886987654321"},"car":{"model":"Wish","licenseplate":"1234-XD","picture":null,"seat":4},"requests":[{"passenger_count":1,"travel":1,"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"end_location":{"gps":[21.9456877,120.7972097],"name":"墾丁","address":"屏東縣恆春鎮墾丁路237號","category":"station"},"name":"左營高鐵站 -> 墾丁","price":450,"discount":0.8,"passenger":{"name":"Shawn","phone":"886952117393"}}],"status":"proceeding"}],"wearedone":[]}
     * requests : {"nowandthen":[{"id":"15","request_id":"06160000113000005","transport_id":"000011300","travel":1,"date":"2016-06-23T00:00:00.000Z","timerange":["2016-06-23T05:00:00.000Z","2016-06-23T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"start_location_custom":null,"end_location":{"gps":[21.949965,120.778987],"name":"吉利通舖民宿","address":"屏東縣恆春鎮墾丁路602號","category":"hotel"},"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"左營高鐵站 -> 墾丁","price":450,"discount":1,"category":"transport"},{"id":"13","request_id":"06160000113000003","transport_id":"000011300","travel":1,"date":"2016-06-24T00:00:00.000Z","timerange":["2016-06-24T05:00:00.000Z","2016-06-24T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"start_location_custom":null,"end_location":{"gps":[21.9456877,120.7972097],"name":"墾丁","address":"屏東縣恆春鎮墾丁路237號","category":"station"},"end_location_custom":null,"passenger":3,"passenger_count":2,"status":"queue","name":"左營高鐵站 -> 墾丁","price":450,"discount":1,"category":"transport"},{"id":"34","request_id":"061780024","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","timerange":["2016-06-30T01:00:00.000Z","2016-06-30T04:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","price":2000,"category":"share"},{"id":"35","request_id":"061780025","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","timerange":["2016-06-30T01:00:00.000Z","2016-06-30T04:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","price":2000,"category":"share"},{"id":"33","request_id":"061780022","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","timerange":["2016-06-30T05:00:00.000Z","2016-06-30T08:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":3,"passenger_count":1,"status":"queue","name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","price":2000,"category":"share"}],"wearedone":[]}
     */

    private ResultBean result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private OrdersBean orders;
        private RequestsBean requests;

        public OrdersBean getOrders() {
            return orders;
        }

        public void setOrders(OrdersBean orders) {
            this.orders = orders;
        }

        public RequestsBean getRequests() {
            return requests;
        }

        public void setRequests(RequestsBean requests) {
            this.requests = requests;
        }

        public static class OrdersBean {
            /**
             * id : 12
             * type : 0
             * category : transport
             * order_id : 000106150000109000002
             * requestdate : 2016-06-23T00:00:00.000Z
             * start_time : 2016-06-23T01:00:00.000Z
             * finish_time : 2016-06-23T03:00:00.000Z
             * duration : 120
             * vacant_seat : 3
             * driver : {"name":"測試司機","picture":null,"phone":"886987654321"}
             * car : {"model":"Wish","licenseplate":"1234-XD","picture":null,"seat":4}
             * requests : [{"passenger_count":1,"travel":1,"start_location":{"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"},"end_location":{"gps":[21.9456877,120.7972097],"name":"墾丁","address":"屏東縣恆春鎮墾丁路237號","category":"station"},"name":"左營高鐵站 -> 墾丁","price":450,"discount":0.8,"passenger":{"name":"Shawn","phone":"886952117393"}}]
             * status : proceeding
             */

            private List<Order> nowandthen;
            private List<Order> wearedone;

            public List<Order> getNowandthen() {
                return nowandthen;
            }

            public void setNowandthen(List<Order> nowandthen) {
                this.nowandthen = nowandthen;
            }

            public List<Order> getWearedone() {
                return wearedone;
            }

            public void setWearedone(List<Order> wearedone) {
                this.wearedone = wearedone;
            }

            public static class Order {
                private String id;
                private int type;
                private String category;
                private String order_id;
                private String requestdate;
                private String start_time;
                private String finish_time;
                private int duration;
                private int vacant_seat;
                /**
                 * name : 測試司機
                 * picture : null
                 * phone : 886987654321
                 */

                private DriverBean driver;
                /**
                 * model : Wish
                 * licenseplate : 1234-XD
                 * picture : null
                 * seat : 4
                 */

                private CarBean car;
                private String status;
                /**
                 * passenger_count : 1
                 * travel : 1
                 * start_location : {"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"}
                 * end_location : {"gps":[21.9456877,120.7972097],"name":"墾丁","address":"屏東縣恆春鎮墾丁路237號","category":"station"}
                 * name : 左營高鐵站 -> 墾丁
                 * price : 450
                 * discount : 0.8
                 * passenger : {"name":"Shawn","phone":"886952117393"}
                 */

                private List<RequestsBean> requests;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public String getOrder_id() {
                    return order_id;
                }

                public void setOrder_id(String order_id) {
                    this.order_id = order_id;
                }

                public String getRequestdate() {
                    return requestdate;
                }

                public void setRequestdate(String requestdate) {
                    this.requestdate = requestdate;
                }

                public String getStart_time() {
                    return start_time;
                }

                public void setStart_time(String start_time) {
                    this.start_time = start_time;
                }

                public String getFinish_time() {
                    return finish_time;
                }

                public void setFinish_time(String finish_time) {
                    this.finish_time = finish_time;
                }

                public int getDuration() {
                    return duration;
                }

                public void setDuration(int duration) {
                    this.duration = duration;
                }

                public int getVacant_seat() {
                    return vacant_seat;
                }

                public void setVacant_seat(int vacant_seat) {
                    this.vacant_seat = vacant_seat;
                }

                public DriverBean getDriver() {
                    return driver;
                }

                public void setDriver(DriverBean driver) {
                    this.driver = driver;
                }

                public CarBean getCar() {
                    return car;
                }

                public void setCar(CarBean car) {
                    this.car = car;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public List<RequestsBean> getRequests() {
                    return requests;
                }

                public void setRequests(List<RequestsBean> requests) {
                    this.requests = requests;
                }

                public static class DriverBean {
                    private String name;
                    private String picture;
                    private String phone;

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getPicture() {
                        return picture;
                    }

                    public void setPicture(String picture) {
                        this.picture = picture;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }
                }

                public static class CarBean {
                    private int id;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }
                }

                public static class RequestsBean {
                    private int passenger_count;
                    private int travel;
                    /**
                     * gps : [22.6871528,120.3088077]
                     * name : 高鐵左營站(客運)
                     * address : 高雄市左營區高鐵路105-107號
                     * category : station
                     */

                    private LocationBean start_location;
                    private LocationBean start_location_custom;
                    /**
                     * gps : [21.9456877,120.7972097]
                     * name : 墾丁
                     * address : 屏東縣恆春鎮墾丁路237號
                     * category : station
                     */

                    private LocationBean end_location;
                    private LocationBean end_location_custom;
                    private String name;
                    private int price;
                    private double discount;
                    /**
                     * name : Shawn
                     * phone : 886952117393
                     */

                    private PassengerBean passenger;

                    public int getPassenger_count() {
                        return passenger_count;
                    }

                    public void setPassenger_count(int passenger_count) {
                        this.passenger_count = passenger_count;
                    }

                    public int getTravel() {
                        return travel;
                    }

                    public void setTravel(int travel) {
                        this.travel = travel;
                    }

                    public LocationBean getStart_location() {
                        return start_location;
                    }

                    public void setStart_location(LocationBean start_location) {
                        this.start_location = start_location;
                    }

                    public LocationBean getEnd_location() {
                        return end_location;
                    }

                    public LocationBean getStart_location_custom() {
                        return start_location_custom;
                    }

                    public void setStart_location_custom(LocationBean start_location_custom) {
                        this.start_location_custom = start_location_custom;
                    }

                    public LocationBean getEnd_location_custom() {
                        return end_location_custom;
                    }

                    public void setEnd_location_custom(LocationBean end_location_custom) {
                        this.end_location_custom = end_location_custom;
                    }

                    public void setEnd_location(LocationBean end_location) {
                        this.end_location = end_location;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public double getDiscount() {
                        return discount;
                    }

                    public void setDiscount(double discount) {
                        this.discount = discount;
                    }

                    public PassengerBean getPassenger() {
                        return passenger;
                    }

                    public void setPassenger(PassengerBean passenger) {
                        this.passenger = passenger;
                    }

                    public static class LocationBean {
                        private String name;
                        private String address;
                        private String category;
                        private List<Double> gps;

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public String getAddress() {
                            return address;
                        }

                        public void setAddress(String address) {
                            this.address = address;
                        }

                        public String getCategory() {
                            return category;
                        }

                        public void setCategory(String category) {
                            this.category = category;
                        }

                        public List<Double> getGps() {
                            return gps;
                        }

                        public void setGps(List<Double> gps) {
                            this.gps = gps;
                        }
                    }



                    public static class PassengerBean {
                        private String name;
                        private String phone;

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public String getPhone() {
                            return phone;
                        }

                        public void setPhone(String phone) {
                            this.phone = phone;
                        }
                    }
                }
            }
        }

        public static class RequestsBean {
            /**
             * id : 15
             * request_id : 06160000113000005
             * transport_id : 000011300
             * travel : 1
             * date : 2016-06-23T00:00:00.000Z
             * timerange : ["2016-06-23T05:00:00.000Z","2016-06-23T07:00:00.000Z"]
             * start_location : {"gps":[22.6871528,120.3088077],"name":"高鐵左營站(客運)","address":"高雄市左營區高鐵路105-107號 ","category":"station"}
             * start_location_custom : null
             * end_location : {"gps":[21.949965,120.778987],"name":"吉利通舖民宿","address":"屏東縣恆春鎮墾丁路602號","category":"hotel"}
             * end_location_custom : null
             * passenger : 3
             * passenger_count : 1
             * status : queue
             * name : 左營高鐵站 -> 墾丁
             * price : 450
             * discount : 1
             * category : transport
             */

            private List<Request> nowandthen;
            private List<Request> wearedone;

            public List<Request> getNowandthen() {
                return nowandthen;
            }

            public void setNowandthen(List<Request> nowandthen) {
                this.nowandthen = nowandthen;
            }

            public List<Request> getWearedone() {
                return wearedone;
            }

            public void setWearedone(List<Request> wearedone) {
                this.wearedone = wearedone;
            }

            public static class Request {
                private String id;
                private String request_id;
                private String transport_id;
                private int travel;
                private String date;
                /**
                 * gps : [22.6871528,120.3088077]
                 * name : 高鐵左營站(客運)
                 * address : 高雄市左營區高鐵路105-107號
                 * category : station
                 */

                private LocationBean start_location;
                private LocationBean start_location_custom;
                /**
                 * gps : [21.949965,120.778987]
                 * name : 吉利通舖民宿
                 * address : 屏東縣恆春鎮墾丁路602號
                 * category : hotel
                 */

                private LocationBean end_location;
                private LocationBean end_location_custom;
                private int passenger;
                private int passenger_count;
                private String status;
                private String name;
                private int price;
                private double discount;
                private String category;
                private List<String> timerange;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getRequest_id() {
                    return request_id;
                }

                public void setRequest_id(String request_id) {
                    this.request_id = request_id;
                }

                public String getTransport_id() {
                    return transport_id;
                }

                public void setTransport_id(String transport_id) {
                    this.transport_id = transport_id;
                }

                public int getTravel() {
                    return travel;
                }

                public void setTravel(int travel) {
                    this.travel = travel;
                }

                public String getDate() {
                    return date;
                }

                public void setDate(String date) {
                    this.date = date;
                }

                public LocationBean getStart_location() {
                    return start_location;
                }

                public void setStart_location(LocationBean start_location) {
                    this.start_location = start_location;
                }

                public LocationBean getStart_location_custom() {
                    return start_location_custom;
                }

                public void setStart_location_custom(LocationBean start_location_custom) {
                    this.start_location_custom = start_location_custom;
                }

                public LocationBean getEnd_location() {
                    return end_location;
                }

                public void setEnd_location(LocationBean end_location) {
                    this.end_location = end_location;
                }

                public LocationBean getEnd_location_custom() {
                    return end_location_custom;
                }

                public void setEnd_location_custom(LocationBean end_location_custom) {
                    this.end_location_custom = end_location_custom;
                }

                public int getPassenger() {
                    return passenger;
                }

                public void setPassenger(int passenger) {
                    this.passenger = passenger;
                }

                public int getPassenger_count() {
                    return passenger_count;
                }

                public void setPassenger_count(int passenger_count) {
                    this.passenger_count = passenger_count;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public double getDiscount() {
                    return discount;
                }

                public void setDiscount(double discount) {
                    this.discount = discount;
                }

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public List<String> getTimerange() {
                    return timerange;
                }

                public void setTimerange(List<String> timerange) {
                    this.timerange = timerange;
                }

                public static class LocationBean {
                    private String name;
                    private String address;
                    private String category;
                    private List<Double> gps;

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getAddress() {
                        return address;
                    }

                    public void setAddress(String address) {
                        this.address = address;
                    }

                    public String getCategory() {
                        return category;
                    }

                    public void setCategory(String category) {
                        this.category = category;
                    }

                    public List<Double> getGps() {
                        return gps;
                    }

                    public void setGps(List<Double> gps) {
                        this.gps = gps;
                    }
                }

            }
        }
    }
}
