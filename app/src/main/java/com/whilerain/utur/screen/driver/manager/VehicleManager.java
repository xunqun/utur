package com.whilerain.utur.screen.driver.manager;

import android.content.Context;

import com.google.gson.Gson;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.driver.VehicalListRequest;
import com.whilerain.utur.net.driver.VehicalUpdasteRequest;
import com.whilerain.utur.screen.driver.manager.model.driver.car.VehicleListResponse;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/5/1.
 */
public class VehicleManager {
    private static VehicleManager instance;
    private List<VehicleManagerListener> listeners = new ArrayList<>();
    private VehicleListResponse vehicleListResponse;
    private Context context;


    public interface VehicleManagerListener {
        void onVehicleUpdateSuccess();

        void onVehicleUpdateFail();

        void onVehicleListChange();
    }

    private VehicleManager(Context context) {
        SimpleDataCache<VehicleListResponse> cache = new SimpleDataCache<>();
        vehicleListResponse = cache.get(context, VehicleListResponse.class);
    }

    public static VehicleManager getInstance(Context context) {

        if (instance == null) {
            instance = new VehicleManager(context);
        }
        instance.context = context;
        return instance;
    }

    public void addVehicleManagerListener(VehicleManagerListener listener){
        listeners.add(listener);
    }

    public void removeVehicleManagerListener(VehicleManagerListener l){
        listeners.remove(l);
    }

    public VehicleListResponse getVehicleListResponse() {
        return vehicleListResponse;
    }

    public void requestVehicleList() {
        VehicalListRequest.build(DriverUserManager.getInstance(context).getUserInfo().getUuid()).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                SimpleDataCache<VehicleListResponse> cache = new SimpleDataCache<>();
                Gson gson = new Gson();
                VehicleListResponse response = gson.fromJson(body, VehicleListResponse.class);
                if (response.isStatus()) {
                    cache.set(context, VehicleListResponse.class, body);
                    vehicleListResponse = cache.get(context, VehicleListResponse.class);
                    dispatchVehicleListChange();
                    return;
                }
            }

            @Override
            public void onFail() {

            }
        }).get();
    }

    public void requestVehicleUpdate(String model, String licenseplate, String license, String licensepic, String picture, int seat) {
        VehicalUpdasteRequest.build(DriverUserManager.getInstance(context).getUserInfo().getUuid(),
                model, licenseplate, license, licensepic, picture, seat).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    boolean status = jsonObject.getBoolean("status");
                    if (status) {
                        dispatchVehicleUpdateResult(true);
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dispatchVehicleUpdateResult(false);
            }

            @Override
            public void onFail() {

            }
        }).put();
    }

    private void dispatchVehicleUpdateResult(boolean success) {
        for (VehicleManagerListener l : listeners) {
            if (success) {
                l.onVehicleUpdateSuccess();
            } else {
                l.onVehicleUpdateFail();
            }
        }
    }

    private void dispatchVehicleListChange() {
        for (VehicleManagerListener l : listeners) {
            l.onVehicleListChange();
        }
    }
}
