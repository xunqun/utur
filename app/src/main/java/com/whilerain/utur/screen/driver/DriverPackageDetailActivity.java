

package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.screen.passenger.HistoryActivity;
import com.whilerain.utur.screen.passenger.manager.model.travel.FullDayTripDetailResponse;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.widget.DatePickerFragment;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverPackageDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.photo)
    ImageView vPhoto;

    @BindView(R.id.desc)
    TextView vDesc;

    @BindView(R.id.duration)
    TextView vDuration;

    @BindView(R.id.order_date)
    TextView vOrderDate;

    @BindView(R.id.order_count)
    TextView vOrderCount;

    @BindView(R.id.price)
    TextView vPrice;

    static FullDayTripDetailResponse response;
    static DriverApi.RequestListResponse.ResultBean resultBean;
    private String date;
    private int returnedPrice = 0;

    public static void launch(Activity activity, FullDayTripDetailResponse r, DriverApi.RequestListResponse.ResultBean bean) {
        response = r;
        resultBean = bean;
        activity.startActivity(new Intent(activity, DriverPackageDetailActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_package_detail);
        ButterKnife.bind(this);
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView(response);
    }

    private void initView(FullDayTripDetailResponse response) {

        getSupportActionBar().setTitle(StringUtils.trimSpecialString(response.getResult().getName()));
        vDesc.setText(response.getResult().getIntroduction());
        int hour = (int)(response.getResult().getDuration() / 60);
        int min = response.getResult().getDuration() % 60;
        vDuration.setText(String.format(getString(R.string.duration), response.getResult().getDuration() / 60f));
        returnedPrice = response.getResult().getPrice();
        vPrice.setText(String.valueOf(returnedPrice));
        try {
            String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd", resultBean.getTimerange().get(0));
            vOrderDate.setText(timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        vOrderCount.setText(String.valueOf(resultBean.getPassenger_count()));
        vPrice.setText(String.valueOf(resultBean.getPrice()));
        try {
            FullDayTripDetailResponse.ResultBean.ScenesBean scenesBean = response.getResult().getScenes().get(0);
            byte[] decodedString = Base64.decode(scenesBean.getPic(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            vPhoto.setImageBitmap(decodedByte);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.order_date_wrapper)
    public void onOrderDateClick() {
        DatePickerFragment dialog = new DatePickerFragment();
        dialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date = String.format("%4d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                vOrderDate.setText(date);
                vOrderDate.setError(null);
            }
        });
        dialog.show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.fab)
    public void onAccept(){
        PackageComfirmAcceptOrderActivity.launch(this, resultBean.getRequest_id());
    }


    private void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.order_scceed))
                .setCancelable(false)
                .setPositiveButton(R.string.launch_history, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HistoryActivity.launchClearTop(DriverPackageDetailActivity.this);
                    }
                })
                .create().show();
    }

    private void showErrorDialog(int msgRes) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(msgRes))
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupPrice(int price) {

            int num = Integer.valueOf(vOrderCount.getText().toString());
            vPrice.setText(String.valueOf(price * num));

    }
}
