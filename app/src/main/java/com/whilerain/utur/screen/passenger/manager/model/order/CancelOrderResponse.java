package com.whilerain.utur.screen.passenger.manager.model.order;

import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/6/18.
 */
public class CancelOrderResponse extends SimpleDataCache.CachePoJo {

    /**
     * status : true
     * result : {"id":"1","type":0,"category":"share","order_id":"0001061880001","requestdate":"2016-06-30T00:00:00.000Z","start_time":"2016-06-30T05:00:00.000Z","finish_time":"2016-06-30T08:20:00.000Z","duration":200,"vacant_seat":3,"driver":"1","car":2,"requests":["061880001"],"status":"aborted","is_commission_paid":false,"is_deleted":false,"createdAt":"2016-06-18T02:45:01.183Z","updatedAt":"2016-06-18T02:45:01.183Z"}
     */

    private boolean status;
    /**
     * id : 1
     * type : 0
     * category : share
     * order_id : 0001061880001
     * requestdate : 2016-06-30T00:00:00.000Z
     * start_time : 2016-06-30T05:00:00.000Z
     * finish_time : 2016-06-30T08:20:00.000Z
     * duration : 200
     * vacant_seat : 3
     * driver : 1
     * car : 2
     * requests : ["061880001"]
     * status : aborted
     * is_commission_paid : false
     * is_deleted : false
     * createdAt : 2016-06-18T02:45:01.183Z
     * updatedAt : 2016-06-18T02:45:01.183Z
     */

    private ResultBean result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private String id;
        private int type;
        private String category;
        private String order_id;
        private String requestdate;
        private String start_time;
        private String finish_time;
        private int duration;
        private int vacant_seat;
        private String driver;
        private int car;
        private String status;
        private boolean is_commission_paid;
        private boolean is_deleted;
        private String createdAt;
        private String updatedAt;
        private List<String> requests;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getRequestdate() {
            return requestdate;
        }

        public void setRequestdate(String requestdate) {
            this.requestdate = requestdate;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getFinish_time() {
            return finish_time;
        }

        public void setFinish_time(String finish_time) {
            this.finish_time = finish_time;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public int getVacant_seat() {
            return vacant_seat;
        }

        public void setVacant_seat(int vacant_seat) {
            this.vacant_seat = vacant_seat;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isIs_commission_paid() {
            return is_commission_paid;
        }

        public void setIs_commission_paid(boolean is_commission_paid) {
            this.is_commission_paid = is_commission_paid;
        }

        public boolean isIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(boolean is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<String> getRequests() {
            return requests;
        }

        public void setRequests(List<String> requests) {
            this.requests = requests;
        }
    }
}
