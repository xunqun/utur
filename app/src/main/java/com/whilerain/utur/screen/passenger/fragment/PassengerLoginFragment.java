package com.whilerain.utur.screen.passenger.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.ModeOptionActivity;
import com.whilerain.utur.screen.passenger.ForgotPasswordActivity;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.screen.register.PassengerRegisterActivity;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.utils.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PassengerLoginFragment extends Fragment {

    @BindView(R.id.country_code)
    EditText vCountryCode;

    @BindView(R.id.contact_num)
    EditText vContactNum;

    @BindView(R.id.pw)
    EditText vPassword;

    @BindView(R.id.login)
    Button vLogin;

    @BindView(R.id.forgot_pw)
    Button vForgotPw;

    private ProgressDialog progressDialog;

    private PassengerUserManager.PassengerUserListener listener = new PassengerUserManager.PassengerUserListener() {


        @Override
        public void onFail() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(R.string.error_login_fail);
            builder.setPositiveButton(R.string.ok, null);
            builder.create().show();
            vLogin.setEnabled(true);
            dismissDialog();
        }

        @Override
        public void onLoginStateChange() {

            dismissDialog();
            vLogin.setEnabled(true);

        }

        @Override
        public void onRegisterResult(boolean success, String msg) {
            dismissDialog();
        }

        @Override
        public void onSmsCodeVerifyResult(boolean success) {
            dismissDialog();
        }
    };

    private void dismissDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    public PassengerLoginFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_passenger_login, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    private void initViews() {
        vPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEND){
                    onLogin();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        PassengerUserManager.getInstance(getContext()).addPassengerUserListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        PassengerUserManager.getInstance(getContext()).removePassengerUserListener(listener);
        dismissDialog();
    }

    @OnClick(R.id.forgot_pw)
    public void onForgotPw(){
        ForgotPasswordActivity.launch(getActivity());
    }

    @OnClick(R.id.login)
    public void onLogin(){
        vLogin.setEnabled(false);
        if(dataValidation()){
            String countryCode = vCountryCode.getText().toString();
            String contactNum = vContactNum.getText().toString();

            String account = StringUtils.getAccountString(countryCode, contactNum);
            String password = vPassword.getText().toString();
            PassengerUserManager.getInstance(getContext()).requestLogin(getContext(), account, password);
            vLogin.setEnabled(false);
            progressDialog = ProgressDialog.show(getContext(), getString(R.string.please_wait), getString(R.string.loging_in));
        }

        UiUtils.hideImeKeyboard(getActivity());
    }

    @OnClick(R.id.register)
    public void onRegister(){
        PassengerRegisterActivity.launch(getActivity());
    }

    @OnClick(R.id.switch_mode)
    protected void onSwitchMode(){
        ModeOptionActivity.launchClearPref(getActivity());
    }

    private boolean dataValidation() {
        boolean ok = true;
        vContactNum.setError(null);
        vCountryCode.setError(null);
        vPassword.setError(null);
        vContactNum.setError(null);
        vCountryCode.setError(null);


        if (vContactNum.getText().toString().length() == 0) {
            ok = false;
            vContactNum.setError(getString(R.string.error_can_not_empty));
        }

        if (vCountryCode.getText().toString().length() == 0) {
            ok = false;
            vCountryCode.setError(getString(R.string.error_can_not_empty));
        }

        if (!TextUtils.isDigitsOnly(vCountryCode.getText().toString()) || vCountryCode.getText().toString().length() != 3) {
            ok = false;
            vCountryCode.setError(getString(R.string.error_data_invalid));
        }

        if (!TextUtils.isDigitsOnly(vContactNum.getText().toString()) || vContactNum.getText().toString().length() < 9 ||  vContactNum.getText().toString().length() > 10) {
            ok = false;
            vContactNum.setError(getString(R.string.error_data_invalid));
        }

        if (vPassword.getText().toString().length() == 0) {
            vPassword.setError(getString(R.string.error_can_not_empty));
            ok = false;
        }
        return ok;
    }

}
