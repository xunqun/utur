package com.whilerain.utur.screen.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverSmsCodeActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_VERIFY = 201;

    @BindView(R.id.code)
    EditText vCode;

    DriverUserManager.DriverUserListener listener = new DriverUserManager.DriverUserListener() {

        @Override
        public void onFail() {

        }

        @Override
        public void onLoginStateChange() {

        }

        @Override
        public void onRegisterResult(boolean success) {

        }

        @Override
        public void onSmsCodeVerifyResult(boolean success) {
            if(DriverSmsCodeActivity.this == null) return;
            if(success){
                setResult(RESULT_OK);
                Toast.makeText(DriverSmsCodeActivity.this, R.string.register_complete, Toast.LENGTH_SHORT).show();
                finish();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(DriverSmsCodeActivity.this);
                builder.setMessage(R.string.error_verify_fail)
                        .setPositiveButton(R.string.ok, null)
                        .create().show();
            }
        }
    };

    public static void launch(Activity activity){
        activity.startActivityForResult(new Intent(activity, DriverSmsCodeActivity.class), REQUEST_CODE_VERIFY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_code);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        vCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEND){
                    onSend();
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DriverUserManager.getInstance(this).addDriverUserListener(listener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DriverUserManager.getInstance(this).removeDriverUserListener(listener);
    }

    @OnClick(R.id.send)
    public void onSend() {
        if (dataValidation()) {
            String code = vCode.getText().toString();
            DriverUserManager.getInstance(this).requestCodeVerify(this, code);
        }
    }

    private boolean dataValidation() {
        boolean ok = true;
        vCode.setError(null);
        String code = vCode.getText().toString();
        if (!TextUtils.isDigitsOnly(code) || code.length() != 6){
            vCode.setError(getString(R.string.error_data_invalid));
            ok = false;
        }
        return ok;
    }
}
