package com.whilerain.utur.screen.driver;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.apis.taxi.DriverTaxiApi;
import com.whilerain.utur.utils.LocationHelper;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.utils.UiUtils;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverTaxiDetailActivity extends AppCompatActivity {

    @BindView(R.id.departure)
    Button vDeparture;

    @BindView(R.id.destination)
    Button vDestination;

    @BindView(R.id.date)
    TextView vDate;

    @BindView(R.id.time)
    TextView vTime;

    @BindView(R.id.price)
    TextView vPrice;

    @BindView(R.id.count)
    TextView vCount;

    private LocationHelper locationHelper;
    private DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean bean;
    private DriverApi.RequestListResponse.ResultBean qListBean;
    private Location mLastLocation;

    public static void launch(Context context, DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean bean) {
        Gson gson = new Gson();
        Intent it = new Intent(context, DriverTaxiDetailActivity.class);
        it.putExtra("data", gson.toJson(bean));
        context.startActivity(it);
    }

    public static void launch(Context context, DriverApi.RequestListResponse.ResultBean bean) {
        Gson gson = new Gson();
        Intent it = new Intent(context, DriverTaxiDetailActivity.class);
        it.putExtra("qlist_data", gson.toJson(bean));
        context.startActivity(it);
    }

    @OnClick(R.id.accept)
    void onAccept() {
        TaxiComfirmAcceptOrderActivity.launch(this, bean != null ? bean.getRequest_id() : qListBean.getRequest_id());
    }

    @OnClick(R.id.departure)
    void onDeparture() {
        if (mLastLocation != null) {
            double lat = bean != null ? bean.getStart_location_custom().getGps().get(0) : qListBean.getStart_location_custom().getGps().get(0);
            double lng = bean != null ? bean.getStart_location_custom().getGps().get(1) : qListBean.getStart_location_custom().getGps().get(1);
            UiUtils.showNavigation(this, mLastLocation.getLatitude(), mLastLocation.getLongitude(), lat, lng);
        }
    }

    @OnClick(R.id.destination)
    void onDestination() {
        if (mLastLocation != null) {
            double lat = bean != null ? bean.getEnd_location_custom().getGps().get(0) : qListBean.getEnd_location_custom().getGps().get(0);
            double lng = bean != null ? bean.getEnd_location_custom().getGps().get(1) : qListBean.getEnd_location_custom().getGps().get(1);
            UiUtils.showNavigation(this, mLastLocation.getLatitude(), mLastLocation.getLongitude(), lat, lng);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_any_time_trip_detail);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        if (getIntent().hasExtra("data")) {
            bean = gson.fromJson(getIntent().getStringExtra("data"), DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean.class);
        } else if (getIntent().hasExtra("qlist_data")) {
            qListBean = gson.fromJson(getIntent().getStringExtra("qlist_data"), DriverApi.RequestListResponse.ResultBean.class);
        } else {
            return;

        }
        initViews();
        locationHelper = new LocationHelper(this, new LocationHelper.LocationHelperListener() {
            @Override
            public void onConnected(Location location) {
                mLastLocation = location;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initViews() {
        vDeparture.setText(bean != null ? bean.getStart_location_custom().getAddress() : qListBean.getStart_location_custom().getAddress());
        vDestination.setText(bean != null ? bean.getEnd_location_custom().getAddress() : qListBean.getEnd_location_custom().getAddress());
        try {
            String localTimeString = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", bean != null ? bean.getTimerange().get(0) : qListBean.getTimerange().get(0));
            vDate.setText(localTimeString.substring(0, 10));
            vTime.setText(localTimeString.substring(11));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        vCount.setText(String.valueOf(bean != null ? bean.getPassenger_count() : qListBean.getPassenger_count()));
        vPrice.setText(String.valueOf(bean != null ? bean.getTotal_cost() : qListBean.getTotal_cost()));
    }


}
