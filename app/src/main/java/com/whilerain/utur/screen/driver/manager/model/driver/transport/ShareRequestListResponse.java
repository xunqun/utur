package com.whilerain.utur.screen.driver.manager.model.driver.transport;

import com.whilerain.utur.apis.DriverApi;

import java.util.List;

/**
 * Created by xunqun on 16/6/20.
 */
public class ShareRequestListResponse {

    /**
     * status : true
     * result : [{"id":"12","request_id":"062080004","transport_id":null,"travel":8,"date":"2016-06-30T00:00:00.000Z","seq":null,"timerange":["2016-06-30T05:00:00.000Z","2016-06-30T08:20:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":2,"passenger_count":1,"order":null,"status":"queue","is_deleted":false,"price":2000,"discount":null}]
     */

    private boolean status;
    /**
     * id : 12
     * request_id : 062080004
     * transport_id : null
     * travel : 8
     * date : 2016-06-30T00:00:00.000Z
     * seq : null
     * timerange : ["2016-06-30T05:00:00.000Z","2016-06-30T08:20:00.000Z"]
     * start_location : null
     * start_location_custom : null
     * end_location : null
     * end_location_custom : null
     * passenger : 2
     * passenger_count : 1
     * order : null
     * status : queue
     * is_deleted : false
     * price : 2000
     * discount : null
     */

    private List<DriverApi.RequestListResponse.ResultBean> result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DriverApi.RequestListResponse.ResultBean> getResult() {
        return result;
    }

    public void setResult(List<DriverApi.RequestListResponse.ResultBean> result) {
        this.result = result;
    }


}
