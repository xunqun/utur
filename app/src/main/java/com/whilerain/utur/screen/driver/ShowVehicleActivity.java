package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.bootstrap.CarDetailRequest;
import com.whilerain.utur.screen.driver.manager.model.driver.car.VehicleListResponse;
import com.whilerain.utur.screen.passenger.manager.model.car.CarResponse;
import com.whilerain.utur.utils.CarPrefHandler;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowVehicleActivity extends AppCompatActivity {

    @BindView(R.id.photo)
    ImageView vPhoto;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.car_num)
    TextView vCarNum;

    @BindView(R.id.car_seats)
    TextView vCarSeats;

    private static VehicleListResponse.Car mCar;

    public static void launch(Activity activity, VehicleListResponse.Car car) {
        mCar = car;
        activity.startActivity(new Intent(activity, ShowVehicleActivity.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_vehicle);
        ButterKnife.bind(this);
        initView();
        CarDetailRequest.build(mCar.getId()).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                Gson gson = new Gson();
                final CarResponse carResponse = gson.fromJson(body, CarResponse.class);
                if (carResponse.isStatus()) {
                    CarPrefHandler.getEditor(ShowVehicleActivity.this).putString(String.valueOf(mCar.getId()), body).apply();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                try {
                                    byte[] decodedString = Base64.decode(carResponse.getResult().getPicture(), Base64.DEFAULT);
                                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                    vPhoto.setImageBitmap(decodedByte);
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(ShowVehicleActivity.this, R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            }

            @Override
            public void onFail() {

            }
        }).get();
    }

    private void initView() {
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mCar.getModel());

        vCarNum.setText(mCar.getLicenseplate());
        vCarSeats.setText(String.valueOf(mCar.getSeat()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){

            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
