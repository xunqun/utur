package com.whilerain.utur.screen.passenger.manager;

import android.util.Log;

import com.google.gson.Gson;
import com.whilerain.utur.MyApplication;
import com.whilerain.utur.apis.BootstrapApi;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by xunqun on 2017/1/25.
 */

public class TransportListManager {
    private static TransportListManager instance;
    private List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean> fromStartTransportList = new ArrayList<>();
    private List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean> fromEndTransportList = new ArrayList<>();
    private Listener listener;

    public interface Listener {
        void onChanged();
    }

    private TransportListManager() {
        SimpleDataCache<BootstrapApi.RequestPopularTourResponse> cache = new SimpleDataCache<BootstrapApi.RequestPopularTourResponse>();
        BootstrapApi.RequestPopularTourResponse response = cache.get(MyApplication.getInstance(), BootstrapApi.RequestPopularTourResponse.class);
        initData(response);
    }

    public static TransportListManager getInstance() {
        if(instance == null){
            instance = new TransportListManager();
        }
        return instance;
    }

    public void setListener(Listener l) {
        listener = l;
    }

    public BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean findFromStartTransportsBeanById(int id){
        for(BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean b : fromStartTransportList){
            if(b.getStart_location().getId() == id){
                return b;
            }
        }
        return null;
    }

    public BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean findFromEndTransportsBeanById(int id){
        for(BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean b : fromEndTransportList){
            if(b.getEnd_location().getId() == id){
                return b;
            }
        }
        return null;
    }

    public void requestFromServer() {
        try {
            Call<BootstrapApi.RequestPopularTourResponse> call = BootstrapApi.RequestPopularTour();
            call.enqueue(new Callback<BootstrapApi.RequestPopularTourResponse>() {
                @Override
                public void onResponse(Call<BootstrapApi.RequestPopularTourResponse> call, Response<BootstrapApi.RequestPopularTourResponse> response) {
                    Gson gson = new Gson();
                    BootstrapApi.RequestPopularTourResponse requestPopularTourResponse = response.body();
                    if (requestPopularTourResponse.isStatus()) {
                        SimpleDataCache<BootstrapApi.RequestPopularTourResponse> cache = new SimpleDataCache<BootstrapApi.RequestPopularTourResponse>();
                        cache.set(MyApplication.getInstance(), BootstrapApi.RequestPopularTourResponse.class, gson.toJson(requestPopularTourResponse));
                        initData(requestPopularTourResponse);
                        if (listener != null) listener.onChanged();
                    }
                }

                @Override
                public void onFailure(Call<BootstrapApi.RequestPopularTourResponse> call, Throwable t) {
                    Log.d("xunqun", "onFailure: ");
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initData(BootstrapApi.RequestPopularTourResponse response) {
        if(response == null) return;
        fromStartTransportList = response.getResult().getFrom_start_location_transports();
        fromEndTransportList = response.getResult().getFrom_end_location_transports();
    }

    public List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean> getFromStartList(){
        return fromStartTransportList;
    }

    public List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean> getFromEndList(){
        return fromEndTransportList;
    }
}
