package com.whilerain.utur.screen.passenger.manager;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.order.PassengerHistoryOrderRequest;
import com.whilerain.utur.screen.passenger.manager.model.passenger.order.PassengerHistoryResponse;
import com.whilerain.utur.utils.SimpleDataCache;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/4/24.
 */
public class HistoryManager {
    private static HistoryManager instance;
    private Context context;
    private PassengerHistoryResponse.ResultBean resultBean;
    private List<HistoryListener> listeners = new ArrayList<>();

    public interface HistoryListener{
        void onDataSetChange();
        void onFail();
    }

    private HistoryManager(Context context){
        this.context = context;
        SimpleDataCache<PassengerHistoryResponse> cache = new SimpleDataCache<PassengerHistoryResponse>();
        PassengerHistoryResponse response = cache.get(context, PassengerHistoryResponse.class);
        if(response != null) {
            resultBean = response.getResult();
        }
    }

    public static HistoryManager getInstance(Context context){
        if(instance == null){
            instance = new HistoryManager(context);
        }
        instance.context = context;
        return instance;
    }

    @Nullable
    public PassengerHistoryResponse.ResultBean getResult(){
        return resultBean;
    }

    public void addHistoryListener(HistoryListener listener){
        listeners.add(listener);
    }

    public void removeHistoryListener(HistoryListener listener){
        listeners.remove(listener);
    }

    private void notifyDatasetChange(){
        for(HistoryListener l : listeners){
            l.onDataSetChange();
        }
    }

    private void notifyFail(){
        for(HistoryListener l : listeners){
            l.onFail();
        }
    }

    public void requestFromServer(){
        String uuid = PassengerUserManager.getInstance(context).getUserInfo().getUuid();
        PassengerHistoryOrderRequest.build(uuid).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {

                Gson gson = new Gson();
                PassengerHistoryResponse response = gson.fromJson(body, PassengerHistoryResponse.class);
                if(response.isStatus()){
                    SimpleDataCache<PassengerHistoryResponse> cache = new SimpleDataCache<PassengerHistoryResponse>();
                    cache.set(context, PassengerHistoryResponse.class, body);
                    resultBean = response.getResult();
                    notifyDatasetChange();
                }else{
                    notifyFail();
                }
            }

            @Override
            public void onFail() {
                notifyFail();
            }
        }).get();
    }
}
