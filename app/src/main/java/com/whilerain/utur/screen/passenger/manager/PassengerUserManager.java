package com.whilerain.utur.screen.passenger.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.whilerain.utur.apis.PushApi;
import com.whilerain.utur.gcm.RegistrationIntentService;
import com.whilerain.utur.screen.passenger.manager.model.passenger.login.PassengerLoginResponse;
import com.whilerain.utur.screen.passenger.manager.model.passenger.login.PassengerLoginResponse.Result;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.login.PassengerLoginRequest;
import com.whilerain.utur.net.register.CodeVerifyRequest;
import com.whilerain.utur.net.register.PassengerRegisterRequest;
import com.whilerain.utur.utils.SharePrefHandler;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/3/30.
 */
public class PassengerUserManager {
    private static PassengerUserManager instance;
    private Result result;
    private List<PassengerUserListener> listeners = new ArrayList<>();
    // Used for un-login state
    private String temp_uuid;
    private String temp_email;
    private String temp_name;



    public interface PassengerUserListener {

        void onFail();

        void onLoginStateChange();

        void onRegisterResult(boolean success, String msg);

        void onSmsCodeVerifyResult(boolean success);
    }

    private PassengerUserManager(Context context) {
        SimpleDataCache<PassengerLoginResponse> cache = new SimpleDataCache<PassengerLoginResponse>();
        PassengerLoginResponse response = cache.get(context, PassengerLoginResponse.class);
        if (response != null) {
            result = cache.get(context, PassengerLoginResponse.class).getResult();
        } else {
            result = null;
        }
    }

    public static PassengerUserManager getInstance(Context context) {
        if (instance == null) {
            instance = new PassengerUserManager(context);
        }
        return instance;
    }



    public Result getUserInfo() {
        return result;
    }

    public String getAccount(Context context){
        return SharePrefHandler.getSharedPrefences(context).getString(SharePrefHandler.ACCOUNT, "");
    }

    public boolean isLogin() {
        return result != null;
    }

    public void requestCodeVerify(final Context context, String code) {
        CodeVerifyRequest.build(temp_uuid, code).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        Gson gson = new Gson();
                        result = new Result();
                        result.setUuid(jsonObject.getJSONObject("result").getString("id"));
                        result.setApiKey(jsonObject.getJSONObject("result").getString("api_secret_key"));
                        result.setName(temp_name);
                        result.setEmail(temp_email);

                        SimpleDataCache<PassengerLoginResponse> cache = new SimpleDataCache<PassengerLoginResponse>();
                        cache.set(context, PassengerLoginResponse.class, gson.toJson(result));
                        result = cache.get(context, PassengerLoginResponse.class).getResult();

                        dispatchCodeVerifyResult(true);
                        return;
                    } else {
                        dispatchCodeVerifyResult(false);
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchCodeVerifyResult(false);
            }

            @Override
            public void onFail() {
                dispatchCodeVerifyResult(false);
            }
        }).post();
    }

    public void requestRegister(final Context context, String phone, final String email, final String name, String password) {
        SharePrefHandler.getEditor(context).putString(SharePrefHandler.ACCOUNT, phone).apply();
        PassengerRegisterRequest.build(phone, email, name, password).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        temp_uuid = jsonObject.getJSONObject("result").getString("uuid");
                        temp_name = name;
                        temp_email = email;

                        dispatchRegisterResult(true, "");
                        return;
                    } else {
                        String msg = "";
                        try {
                            msg = jsonObject.getJSONObject("result").getString("message");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        dispatchRegisterResult(false, msg);
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRegisterResult(false, "");
            }

            @Override
            public void onFail() {
                dispatchRegisterResult(false, "連線失敗");
            }
        }).post();
    }

    public void requestLogin(final Context context, String account, String password) {
        SharePrefHandler.getEditor(context).putString(SharePrefHandler.ACCOUNT, account).apply();
        PassengerLoginRequest.build(account, password).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        SimpleDataCache<PassengerLoginResponse> cache = new SimpleDataCache<PassengerLoginResponse>();
                        cache.set(context, PassengerLoginResponse.class, body);
                        result = cache.get(context, PassengerLoginResponse.class).getResult();
                        dispatchLoginStateChange();


                        RegistrationIntentService.launch(context, PassengerUserManager.getInstance(context).getUserInfo().getUuid());
                        return;
                    } else {

                        dispatchRequestFail();
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRequestFail();
            }

            @Override
            public void onFail() {
                dispatchRequestFail();
            }
        }).post();
    }

    public void logout(Context context) {
        String gcmToken = SharePrefHandler.getSharedPrefences(context).getString(SharePrefHandler.GCM_TOKEN, "");
        if(gcmToken.length() > 0){
            try {
                PushApi.requestDeleteToken(PassengerUserManager.getInstance(context).getUserInfo().getUuid(), gcmToken);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        SharePrefHandler.getEditor(context).clear().apply();
        SimpleDataCache<PassengerLoginResponse> cache = new SimpleDataCache<PassengerLoginResponse>();
        cache.set(context, PassengerLoginResponse.class, null);
        result = null;
        dispatchLoginStateChange();


    }

    public void addPassengerUserListener(PassengerUserListener l) {
        this.listeners.add(l);
    }

    public void removePassengerUserListener(PassengerUserListener l) {
        synchronized (listeners) {
            this.listeners.remove(l);
        }
    }

    public void dispatchCodeVerifyResult(final boolean b) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listeners.size(); i++) {
                    PassengerUserListener l = listeners.get(i);
                    l.onSmsCodeVerifyResult(b);
                }
            }
        });
    }

    public void dispatchRegisterResult(final boolean b, final String msg) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listeners.size(); i++) {
                    PassengerUserListener l = listeners.get(i);
                    l.onRegisterResult(b, msg);
                }
            }
        });
    }

    public void dispatchRequestFail() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listeners.size(); i++) {
                    PassengerUserListener l = listeners.get(i);
                    l.onFail();
                }
            }
        });

    }

    public void dispatchLoginStateChange() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                synchronized (listeners) {
                    for (int i = 0; i < listeners.size(); i++) {
                        PassengerUserListener l = listeners.get(i);
                        l.onLoginStateChange();
                    }
                }
            }
        });
    }
}
