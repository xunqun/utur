package com.whilerain.utur.screen.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PassengerSmsCodeActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_VERIFY = 201;

    @BindView(R.id.code)
    EditText vCode;

    PassengerUserManager.PassengerUserListener listener = new PassengerUserManager.PassengerUserListener() {

        @Override
        public void onFail() {

        }

        @Override
        public void onLoginStateChange() {

        }

        @Override
        public void onRegisterResult(boolean success, String msg) {

        }

        @Override
        public void onSmsCodeVerifyResult(boolean success) {
            if(PassengerSmsCodeActivity.this == null) return;
            if(success){
                setResult(RESULT_OK);
                finish();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(PassengerSmsCodeActivity.this);
                builder.setMessage(R.string.error_verify_fail)
                        .setPositiveButton(R.string.ok, null)
                        .create().show();
            }
        }
    };

    public static void launch(Activity activity){
        activity.startActivityForResult(new Intent(activity, PassengerSmsCodeActivity.class), REQUEST_CODE_VERIFY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_code);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        vCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEND){
                    onSend();
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        PassengerUserManager.getInstance(this).addPassengerUserListener(listener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PassengerUserManager.getInstance(this).removePassengerUserListener(listener);
    }

    @OnClick(R.id.send)
    public void onSend() {
        if (dataValidation()) {
            String code = vCode.getText().toString();
            PassengerUserManager.getInstance(this).requestCodeVerify(this, code);
        }
    }

    private boolean dataValidation() {
        boolean ok = true;
        vCode.setError(null);
        String code = vCode.getText().toString();
        if (!TextUtils.isDigitsOnly(code) || code.length() != 6){
            vCode.setError(getString(R.string.error_data_invalid));
            ok = false;
        }
        return ok;
    }
}
