package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.driver.fragment.HistoryFinishedFragment;
import com.whilerain.utur.screen.driver.fragment.HistoryProgressingFragment;
import com.whilerain.utur.screen.driver.manager.DriverHistoryManager;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar vToolBar;

    @BindView(R.id.user)
    TextView vUser;

    @BindView(R.id.strip)
    PagerTabStrip vStrip;

    @BindView(R.id.pager)
    ViewPager vPager;

    private enum PAGE{
        p1(HistoryProgressingFragment.class, R.string.received_history),
        p2(HistoryFinishedFragment.class, R.string.finish_history);

        public Class<? extends Fragment> C;
        public int titleRes;
        PAGE(Class<? extends Fragment> t, int title){
            this.C = t;
            this.titleRes = title;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

        DriverHistoryManager.getInstance(this).requestFromServer();
    }

    private void initViews() {
        setSupportActionBar(vToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupPager();
        vUser.setText(DriverUserManager.getInstance(this).getUserInfo().getName());
    }

    private void setupPager() {
        vPager.setAdapter(new HistoryPagerAdapter(getSupportFragmentManager()));

    }

    public static void launch(Activity activity){
        activity.startActivity(new Intent(activity, HistoryActivity.class));
    }

    public static void launchClearTop(Activity activity) {
        Intent it = new Intent(activity, HistoryActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(it);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            DriverActivity.launch(this, 0);
        }
        return super.onOptionsItemSelected(item);
    }



    private class HistoryPagerAdapter extends FragmentPagerAdapter{

        public HistoryPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Class<? extends Fragment> T = PAGE.values()[position].C;

            try {
                return T.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(PAGE.values()[position].titleRes);
        }
    }
}
