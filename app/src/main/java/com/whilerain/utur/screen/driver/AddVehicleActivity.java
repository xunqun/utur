package com.whilerain.utur.screen.driver;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.driver.manager.VehicleManager;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddVehicleActivity extends AppCompatActivity implements VehicleManager.VehicleManagerListener {
    private static final int REQUEST_PERMISSION_CAMERA = 234;
    private static final int REQUEST_TAKE_ABLUM = 235;
    private static final int REQUEST_TAKE_PICTURE = 236;
    @BindView(R.id.model)
    EditText vModel;

    @BindView(R.id.vehicle_num)
    EditText vVehicleNum;

    @BindView(R.id.seat)
    EditText vSeat;

    @BindView(R.id.pic)
    ImageView vPic;

    @BindView(R.id.license_pic)
    ImageView vLicensePic;

    @BindView(R.id.license_num)
    EditText vLicenseNum;

    @BindView(R.id.take_photo)
    Button vTakePhoto;

    @BindView(R.id.take_photo_license)
    Button vTakePhotoLicense;

    private String mPicBase64 = "";
    private String mLicensePicBase64 = "";


    enum CamAction {
        CarPic, LicensePic;
    }

    CamAction action = CamAction.CarPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {

        registerForContextMenu(vTakePhoto);
        registerForContextMenu(vTakePhotoLicense);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Bitmap bitmap = null;
            Bitmap thumbnail = null;
            if (requestCode == REQUEST_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
            }

            if (requestCode == REQUEST_TAKE_ABLUM && resultCode == Activity.RESULT_OK) {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                bitmap = BitmapFactory.decodeStream(inputStream);
            }

            if (bitmap == null) return;

            //scale bitmap
            if (bitmap.getWidth() > bitmap.getHeight()) {
                thumbnail = bitmap.createScaledBitmap(bitmap, 800, (int) (800f / bitmap.getWidth() * bitmap.getHeight()), false);
            } else {
                thumbnail = bitmap.createScaledBitmap(bitmap, 600, (int) (600f / bitmap.getHeight() * bitmap.getWidth()), false);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String base64 = Base64.encodeToString(b, Base64.DEFAULT);
            if (action == CamAction.CarPic) {
                vPic.setImageBitmap(bitmap);
                mPicBase64 = base64;
            } else {
                vLicensePic.setImageBitmap(bitmap);
                mLicensePicBase64 = base64;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        VehicleManager.getInstance(this).addVehicleManagerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        VehicleManager.getInstance(this).removeVehicleManagerListener(this);
    }

    public static void launch(Activity a) {
        a.startActivity(new Intent(a, AddVehicleActivity.class));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_photo_option, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.camera:

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
                    }
                } else {

                    takePictureIntent();
                }
                return true;
            case R.id.album:
                Intent pickIntent = new Intent();
                pickIntent.setType("image/*");
                pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(pickIntent, REQUEST_TAKE_ABLUM);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_add_vehicle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send:
                boolean ok = checkData();
                if (ok) {

                    VehicleManager.getInstance(this).requestVehicleUpdate(vModel.getText().toString(),
                            vVehicleNum.getText().toString(),
                            vLicenseNum.getText().toString(),
                            mLicensePicBase64, mPicBase64,
                            Integer.valueOf(vSeat.getText().toString()));
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkData() {
        vModel.setError(null);
        vVehicleNum.setError(null);
        vSeat.setError(null);
        vTakePhoto.setError(null);
        vTakePhotoLicense.setError(null);

        boolean ok = true;
        if (TextUtils.isEmpty(vModel.getText().toString())) {
            ok = false;
            vModel.setError(getString(R.string.error_can_not_empty));
        }

        if (TextUtils.isEmpty(vVehicleNum.getText().toString())) {
            ok = false;
            vVehicleNum.setText(getString(R.string.error_can_not_empty));
        }

        if (TextUtils.isEmpty(vSeat.getText().toString())) {
            ok = false;
            vSeat.setError(getString(R.string.error_can_not_empty));
        }

        if (!TextUtils.isDigitsOnly(vSeat.getText().toString())) {
            ok = false;
            vSeat.setError(getString(R.string.error_data_invalid));
        }

        if (mLicensePicBase64.length() == 0) {
            ok = false;
            vTakePhotoLicense.setError(getString(R.string.error_can_not_empty));
        }

        if(mPicBase64.length() == 0){
            ok = false;
            vTakePhoto.setError(getString(R.string.error_can_not_empty));
        }
        return ok;
    }

    @OnClick(R.id.take_photo_license)
    protected void onTakePhotoLicense(View v) {
        action = CamAction.LicensePic;
        openContextMenu(v);
    }

    @OnClick(R.id.take_photo)
    protected void onTakePhoto(View v) {
        action = CamAction.CarPic;
        openContextMenu(v);
    }

    private void takePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE);
        }
    }

    @Override
    public void onVehicleUpdateSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMessage(getString(R.string.add_vehicle_success), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }
        });

    }

    @Override
    public void onVehicleUpdateFail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMessage(getString(R.string.error_data_query_fail), null);
            }
        });

    }

    @Override
    public void onVehicleListChange() {

    }

    protected void showMessage(String text, DialogInterface.OnClickListener l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(text)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, l)
                .create().show();
    }
}
