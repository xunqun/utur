package com.whilerain.utur.screen.driver.fragment;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.BootstrapApi;
import com.whilerain.utur.screen.driver.DriverActivity;
import com.whilerain.utur.screen.driver.DriverStationShuttleTripListActivity;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.driver.model.TransportWraper;
import com.whilerain.utur.screen.passenger.manager.TransportListManager;
import com.whilerain.utur.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xunqun on 2017/2/5.
 */

public class TransportFragment extends Fragment implements TransportListManager.Listener {


    @BindView(R.id.list)
    ExpandableListView vList;

    @BindView(R.id.root)
    ViewGroup vRoot;

    private List<TransportWraper> fromStationLocations = new ArrayList<>();
    private List<TransportWraper> fromSpotLocations = new ArrayList<>();
    ListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransportListManager.getInstance().setListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_populartour, null);
        ButterKnife.bind(this, view);
        initViews();

        if (NetworkUtils.isOnline(getContext())) {
            //do request
            TransportListManager.getInstance().requestFromServer();
        } else {
            Snackbar snackbar = Snackbar.make(vRoot, R.string.error_no_network, Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.RED);
            snackbar.show();
        }

        return view;
    }

    private void initViews() {
        initData();
        adapter = new ListAdapter();
        vList.setAdapter(adapter);
        vList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if(DriverUserManager.getInstance(getContext()).isLogin()) {

                    DriverStationShuttleTripListActivity.launch(getActivity(), adapter.getChild(groupPosition, childPosition).getBean().getTransports_id());

                }else{

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.require_loged_in)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ((DriverActivity)getActivity()).switchToPageNum(3);
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, null);
                    builder.create().show();
                }
                return true;
            }
        });
        vList.expandGroup(0, true);
        vList.expandGroup(1, true);
    }

    private void initData() {
        List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean> list = TransportListManager.getInstance().getFromStartList();

        fromStationLocations.clear();
        fromSpotLocations.clear();

        for (BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean b : list) {
            if (b.getCategory().equals("station")) {
                for (BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean t : b.getTransports()) {
                    TransportWraper w = new TransportWraper();
                    w.setBean(t);
                    w.setFromLocation(b.getStart_location());
                    fromStationLocations.add(w);
                }
            } else {
                for (BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean t : b.getTransports()) {

                    TransportWraper w = new TransportWraper();
                    w.setBean(t);
                    w.setFromLocation(b.getStart_location());
                    fromSpotLocations.add(w);

                }
            }
        }
    }

    @Override
    public void onChanged() {
        initData();
        adapter.notifyDataSetChanged();
    }

    class ListAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            return 2;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return getGroup(groupPosition).size();
        }

        @Override
        public List<TransportWraper> getGroup(int groupPosition) {
            if (groupPosition == 0) {
                return fromStationLocations;
            } else {
                return fromSpotLocations;
            }
        }

        @Override
        public TransportWraper getChild(int groupPosition, int childPosition) {
            return getGroup(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_popular_tour_group, null);
            if (groupPosition == 0) {
                ((TextView) ButterKnife.findById(view, R.id.name)).setText(R.string.from_station);
            } else {
                ((TextView) ButterKnife.findById(view, R.id.name)).setText(R.string.from_spot);
            }
            return view;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_popular_tour_item, null);
            }
            TextView name = ButterKnife.findById(convertView, R.id.name);

            TransportWraper b = getChild(groupPosition, childPosition);

            if (groupPosition == 0) {
                name.setText(b.getFromLocation().getName() + " ► " + b.getBean().getEnd_location().getName());
                name.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_directions_bus_black_24dp), null, null, null);
            } else {
                name.setText(b.getFromLocation().getName() + " ► " + b.getBean().getEnd_location().getName());
                name.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_beach_access_black_24dp), null, null, null);
            }

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }


}
