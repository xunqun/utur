
package com.whilerain.utur.screen.driver.manager.model.driver.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.whilerain.utur.utils.SimpleDataCache;


public class DriverLoginResponse extends SimpleDataCache.CachePoJo {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * 
     * @return
     *     The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public static class Result {

        @SerializedName("uuid")
        @Expose
        private String uuid;
        @SerializedName("api_key")
        @Expose
        private String apiKey;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("status")
        private String status;

        /**
         *
         * @return
         *     The uuid
         */
        public String getUuid() {
            return uuid;
        }

        /**
         *
         * @param uuid
         *     The uuid
         */
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        /**
         *
         * @return
         *     The apiKey
         */
        public String getApiKey() {
            return apiKey;
        }

        /**
         *
         * @param apiKey
         *     The api_key
         */
        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The email
         */
        public String getEmail() {
            return email;
        }

        /**
         *
         * @param email
         *     The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
