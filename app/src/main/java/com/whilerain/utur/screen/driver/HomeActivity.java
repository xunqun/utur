package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.model.ApiReference;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.travel.FullDayTripDetailRequest;
import com.whilerain.utur.net.travel.HalfDayTripDetailRequest;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.passenger.manager.model.travel.FullDayTripDetailResponse;
import com.whilerain.utur.screen.passenger.manager.model.travel.HalfDayTripDetailResponse;
import com.whilerain.utur.utils.LocationHelper;
import com.whilerain.utur.utils.NetworkUtils;
import com.whilerain.utur.utils.StringUtils;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.progressBar)
    ProgressBar vProgress;

    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwipe;
    LocationHelper locationHelper;
    private List<DriverApi.RequestListResponse.ResultBean> mList;
    private ListAdapter adapter;
    private Timer refreshTimer = new Timer();
    private Location mLastLocation;


    public static void launchClearTop(Activity activity) {
        Intent intent = new Intent(activity, com.whilerain.utur.screen.driver.HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
        setContentView(R.layout.activity_driver_home);
        ButterKnife.bind(this);
        try {
            if (!NetworkUtils.isOnline(this)) {
                Toast.makeText(this, R.string.error_connection_fail, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
        locationHelper = new LocationHelper(this, new LocationHelper.LocationHelperListener() {
            @Override
            public void onConnected(Location location) {
                mLastLocation = location;
                try {
                    vProgress.setVisibility(View.VISIBLE);
                    sendRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            refreshTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                vProgress.setVisibility(View.VISIBLE);
                            }
                        });
                        sendRequest();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, 0, 3 * 60 * 1000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (refreshTimer != null)
            refreshTimer.cancel();
    }

    private void sendRequest() throws JSONException {
        if (mLastLocation == null){
            vSwipe.setRefreshing(false);
            vProgress.setVisibility(View.INVISIBLE);

            return;
        }
        DriverApi.RequestList(5, mLastLocation.getLatitude(), mLastLocation.getLongitude()).enqueue(new Callback<DriverApi.RequestListResponse>() {
            @Override
            public void onResponse(Call<DriverApi.RequestListResponse> call, Response<DriverApi.RequestListResponse> response) {
                try {
                    mList = response.body().getResult();
                    adapter.notifyDataSetChanged();
                    vProgress.setVisibility(View.INVISIBLE);
                    vSwipe.setRefreshing(false);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DriverApi.RequestListResponse> call, Throwable t) {
                Log.d("xunqun", "onFailure: ");
                try {
                    vProgress.setVisibility(View.INVISIBLE);
                    vSwipe.setRefreshing(false);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void initView() {
        adapter = new ListAdapter();
        vList.setAdapter(adapter);
        vSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    sendRequest();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick(R.id.free_route)
    void onFreeRoute() {
        DriverActivity.launch(this, 2);

    }

    @OnClick(R.id.station)
    void onStation() {

        DriverActivity.launch(this, 0);

    }

    @OnClick(R.id.halfday)
    void onHalfday() {

        DriverActivity.launch(this, 1);

    }

    @OnClick(R.id.profile)
    void onProfile() {
        DriverActivity.launch(this, 3);

    }

    class ListAdapter extends BaseAdapter {
        private List<DriverApi.RequestListResponse.ResultBean> list;

        ListAdapter() {
            init();
        }

        @Override
        public void notifyDataSetChanged() {
            init();
            super.notifyDataSetChanged();

        }

        void init() {
            list = mList;
            if (list == null) {
                list = new ArrayList<>();
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public DriverApi.RequestListResponse.ResultBean getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(HomeActivity.this).inflate(R.layout.layout_driver_quick_list, null);
                vh = new ViewHolder(convertView);
                convertView.setTag(vh);

            }
            vh = (ViewHolder) convertView.getTag();
            vh.loadData(getItem(position));
            return convertView;
        }
    }

    class ViewHolder {
        @BindView(R.id.icon)
        ImageView vIcon;

        @BindView(R.id.time)
        TextView vTime;

        @BindView(R.id.desc)
        TextView vDesc;

        @BindView(R.id.count)
        Button vCount;

        DriverApi.RequestListResponse.ResultBean bean;

        ViewHolder(View v) {
            ButterKnife.bind(this, v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!DriverUserManager.getInstance(HomeActivity.this).isLogin()) {
                        Toast.makeText(HomeActivity.this, R.string.require_loged_in, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(bean.getCategory().equalsIgnoreCase(ApiReference.TransType.TAXI.toString())){
                        DriverTaxiDetailActivity.launch(HomeActivity.this, bean);
                    }else if (bean.getCategory().equalsIgnoreCase(ApiReference.TransType.TRANSPORT.toString())) {
                        DriverTransportDetailActivity.launch(HomeActivity.this, bean);
                    } else if (bean.getCategory().equalsIgnoreCase(ApiReference.TransType.SHARE.toString())) {
                        requestShareDetailFromServer(bean);
                    } else {
                        requestPackageDetailFromServer(bean);
                    }
                }
            });
        }

        public void loadData(DriverApi.RequestListResponse.ResultBean b) {
            bean = b;
            String desc = "";
            if(b.getCategory().equalsIgnoreCase(ApiReference.TransType.TAXI.toString())) {
                vIcon.setBackgroundResource(R.color.color_package);
                vIcon.setImageResource(R.drawable.bitmap_taxi_dark);
                desc = b.getStart_location_custom().getAddress();
            }if (b.getCategory().equalsIgnoreCase(ApiReference.TransType.TRANSPORT.toString())) {
                vIcon.setBackgroundResource(R.color.color_transport);
                vIcon.setImageResource(R.drawable.bitmap_shuttle_dark);
                try {
                    desc = b.getStart_location() == null ? b.getStart_location_custom().getAddress() : b.getStart_location().getName();
                    desc += " ► " + (b.getEnd_location() == null ? b.getEnd_location_custom().getAddress() : b.getEnd_location().getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (b.getCategory().equalsIgnoreCase(ApiReference.TransType.PACKAGE.toString())) {
                vIcon.setBackgroundResource(R.color.color_package);
                vIcon.setImageResource(R.drawable.bitmap_package_dark);
                desc = b.getName();

            } else if (b.getCategory().equalsIgnoreCase(ApiReference.TransType.SHARE.toString())) {
                vIcon.setBackgroundResource(R.color.color_halfday);
                vIcon.setImageResource(R.drawable.bitmap_share_dark);
                desc = b.getName();
            }
            vDesc.setText(StringUtils.trimSpecialString(desc));
            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", b.getTimerange().get(0));
                vTime.setText(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            vCount.setText(String.valueOf(b.getPassenger_count()));
        }

        private void requestPackageDetailFromServer(final DriverApi.RequestListResponse.ResultBean item) {
            FullDayTripDetailRequest.build(item.getTravel()).setApiListener(new ApiRequest.ApiListener() {
                @Override
                public void onSeccess(ApiRequest apiRequest, String body) {
                    try {
                        Gson gson = new Gson();
                        final FullDayTripDetailResponse response = gson.fromJson(body, FullDayTripDetailResponse.class);
                        if (response.isStatus()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DriverPackageDetailActivity.launch(HomeActivity.this, response, item);
                                }
                            });
                        } else {
                            Toast.makeText(HomeActivity.this, R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
                        }
                        return;
                    } catch (Exception e) {

                    }
                    Toast.makeText(HomeActivity.this, R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFail() {

                }
            }).get();
        }

        private void requestShareDetailFromServer(final DriverApi.RequestListResponse.ResultBean item) {
            HalfDayTripDetailRequest.build(item.getTravel()).setApiListener(new ApiRequest.ApiListener() {
                @Override
                public void onSeccess(ApiRequest apiRequest, String body) {
                    try {
                        Gson gson = new Gson();
                        final HalfDayTripDetailResponse response = gson.fromJson(body, HalfDayTripDetailResponse.class);
                        if (response.isStatus()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DriverShareDetailActivity.launch(HomeActivity.this, response, item);
                                }
                            });
                        } else {
                            Toast.makeText(HomeActivity.this, R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
                        }
                        return;
                    } catch (Exception e) {

                    }
                    Toast.makeText(HomeActivity.this, R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFail() {

                }
            }).get();
        }
    }
}
