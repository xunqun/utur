
package com.whilerain.utur.screen.driver.manager.model.driver.register;

public class DriverRegisterResponse {


    /**
     * status : true
     * result : {"uuid":"b2f30c87-549b-4e16-8b50-cdeeec96cf34"}
     */

    private boolean status;
    /**
     * uuid : b2f30c87-549b-4e16-8b50-cdeeec96cf34
     */

    private ResultBean result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private String uuid;

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }
}
