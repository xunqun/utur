package com.whilerain.utur.screen.passenger.manager.model.travel;

import java.util.List;

/**
 * Created by xunqun on 16/6/18.
 */
public class FullDayTripDetailResponse {

    /**
     * status : true
     * result : {"introduction":"墾丁一日包車，帶您暢遊墾丁！","duration":360,"name":"包車整天遊墾丁","price":3500,"timetable":["09:00"],"scenes":[{"name":"墾丁","pic":"abcd"}]}
     */

    private boolean status;
    /**
     * introduction : 墾丁一日包車，帶您暢遊墾丁！
     * duration : 360
     * name : 包車整天遊墾丁
     * price : 3500
     * timetable : ["09:00"]
     * scenes : [{"name":"墾丁","pic":"abcd"}]
     */

    private ResultBean result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private String introduction;
        private int duration;
        private String name;
        private int price;
        private List<String> timetable;
        /**
         * name : 墾丁
         * pic : abcd
         */

        private List<ScenesBean> scenes;

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public List<String> getTimetable() {
            return timetable;
        }

        public void setTimetable(List<String> timetable) {
            this.timetable = timetable;
        }

        public List<ScenesBean> getScenes() {
            return scenes;
        }

        public void setScenes(List<ScenesBean> scenes) {
            this.scenes = scenes;
        }

        public static class ScenesBean {
            private String name;
            private String pic;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }
        }
    }
}
