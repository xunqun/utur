package com.whilerain.utur.screen.driver.fragment;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.taxi.DriverTaxiApi;
import com.whilerain.utur.screen.driver.DriverTaxiDetailActivity;
import com.whilerain.utur.utils.LocationHelper;
import com.whilerain.utur.utils.StringUtils;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by shawn on 2017/4/25.
 */

public class DriverTaxiFragment extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 10;
    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwipe;

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.progressBar)
    ProgressBar vProgress;

    @BindView(R.id.empty)
    TextView vEmpty;

    private Location mLastLocation;
    private ListAdapter adapter;
    private LocationHelper locationHelper;

    @OnItemClick(R.id.list)
    void onItemClick(int position, View view) {
        ViewHolder vh = (ViewHolder) view.getTag();
        DriverTaxiDetailActivity.launch(getContext(), vh.bean);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stationshuttle_list, container, false);
        ButterKnife.bind(this, view);
        locationHelper = new LocationHelper(getActivity(), new LocationHelper.LocationHelperListener() {
            @Override
            public void onConnected(Location location) {
                mLastLocation = location;
                if (mLastLocation != null)
                    requestFromServer();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Override
    public void onStart() {
        locationHelper.onStart();
        super.onStart();
    }

    @Override
    public void onStop() {
        locationHelper.onStop();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mLastLocation != null) {
            requestFromServer();
        }
    }

    private void initView() {
        adapter = new ListAdapter();
        vList.setAdapter(adapter);
        vSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(mLastLocation != null) {
                    requestFromServer();
                }else{
                    vSwipe.setRefreshing(false);
                }
            }
        });
    }

    private void requestFromServer() {
        vProgress.setVisibility(View.VISIBLE);
        try {
            Call<DriverTaxiApi.TaxiRequestListResponse> call = DriverTaxiApi.requestList(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            call.enqueue(new Callback<DriverTaxiApi.TaxiRequestListResponse>() {
                @Override
                public void onResponse(Call<DriverTaxiApi.TaxiRequestListResponse> call, Response<DriverTaxiApi.TaxiRequestListResponse> response) {
                    vProgress.setVisibility(GONE);
                    vSwipe.setRefreshing(false);
                    DriverTaxiApi.TaxiRequestListResponse r = response.body();
                    if (r.isStatus()) {
                        adapter.setData(r.getResult());
                    }
                }

                @Override
                public void onFailure(Call<DriverTaxiApi.TaxiRequestListResponse> call, Throwable t) {
                    vProgress.setVisibility(GONE);
                    vSwipe.setRefreshing(false);
                    Toast.makeText(getContext(), R.string.error_connection_fail, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        locationHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private class ListAdapter extends BaseAdapter {
        List<DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean> list;

        ListAdapter() {
            list = new ArrayList<>();
            vEmpty.setVisibility(View.VISIBLE);
        }

        void setData(List<DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean> l) {
            list = l;
            if (list == null) {
                list = new ArrayList<>();
            }
            notifyDataSetChanged();
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            if(getCount() > 0){
                vEmpty.setVisibility(GONE);
            }else{
                vEmpty.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getCount() {

            return list.size();
        }

        @Override
        public DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder vh;
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.layout_driver_taxi_list_item, null);
                vh = new ViewHolder(view);
                view.setTag(vh);
            } else {
                vh = (ViewHolder) view.getTag();
            }

            vh.loadData(getItem(i));

            return view;
        }
    }

    class ViewHolder {
        @BindView(R.id.time)
        TextView vTime;

        @BindView(R.id.people_count)
        TextView vPeopleCount;

        @BindView(R.id.address)
        TextView vAddress;

        @BindView(R.id.dest)
        TextView vDest;

        private DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean bean;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void loadData(DriverTaxiApi.TaxiRequestListResponse.TaxiResultBean b) {
            this.bean = b;
            try {
                String localTimeString = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", b.getTimerange().get(0));
                vTime.setText(localTimeString);
            } catch (Exception e) {
                vTime.setText(b.getTimerange().get(0));
            }
            vAddress.setText(b.getStart_location_custom().getAddress());
            vDest.setText(b.getEnd_location_custom().getAddress());
            vPeopleCount.setText(b.getPassenger_count() + "人");

        }
    }

}
