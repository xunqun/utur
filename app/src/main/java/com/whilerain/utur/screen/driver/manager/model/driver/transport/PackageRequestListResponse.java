package com.whilerain.utur.screen.driver.manager.model.driver.transport;

import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/6/21.
 */
public class PackageRequestListResponse extends SimpleDataCache.CachePoJo{

    /**
     * status : true
     * result : [{"id":"14","request_id":"062070005","transport_id":null,"travel":7,"date":"2016-06-29T00:00:00.000Z","seq":null,"timerange":["2016-06-29T01:00:00.000Z","2016-06-29T07:00:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":2,"passenger_count":2,"order":null,"status":"queue","is_deleted":false,"price":3500,"discount":null},{"id":"15","request_id":"062170006","transport_id":null,"travel":7,"date":"2016-06-30T00:00:00.000Z","seq":null,"timerange":["2016-06-30T01:00:00.000Z","2016-06-30T07:00:00.000Z"],"start_location":null,"start_location_custom":null,"end_location":null,"end_location_custom":null,"passenger":2,"passenger_count":2,"order":null,"status":"queue","is_deleted":false,"price":3500,"discount":null}]
     */

    private boolean status;
    /**
     * id : 14
     * request_id : 062070005
     * transport_id : null
     * travel : 7
     * date : 2016-06-29T00:00:00.000Z
     * seq : null
     * timerange : ["2016-06-29T01:00:00.000Z","2016-06-29T07:00:00.000Z"]
     * start_location : null
     * start_location_custom : null
     * end_location : null
     * end_location_custom : null
     * passenger : 2
     * passenger_count : 2
     * order : null
     * status : queue
     * is_deleted : false
     * price : 3500
     * discount : null
     */

    private List<DriverApi.RequestListResponse.ResultBean> result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DriverApi.RequestListResponse.ResultBean> getResult() {
        return result;
    }

    public void setResult(List<DriverApi.RequestListResponse.ResultBean> result) {
        this.result = result;
    }


}
