package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.ModeOptionActivity;
import com.whilerain.utur.screen.driver.fragment.DriverShareFragment;
import com.whilerain.utur.screen.driver.fragment.DriverLoginFragment;
import com.whilerain.utur.screen.driver.fragment.DriverTaxiFragment;
import com.whilerain.utur.screen.driver.fragment.MyProfileFragment;
import com.whilerain.utur.screen.driver.fragment.TransportFragment;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DriverActivity extends AppCompatActivity implements DriverUserManager.DriverUserListener {

    private static int openIndex = 0;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    private enum PAGE {
        p1(R.string.title_station_shuttle, R.drawable.ic_attraction, TransportFragment.class),
        p2(R.string.title_popular_route, R.drawable.ic_attraction, DriverShareFragment.class),
        p3(R.string.title_free_routes, R.drawable.ic_attraction, DriverTaxiFragment.class),
        p4(R.string.title_my_profile, R.drawable.ic_attraction, MyProfileFragment.class);

        public int resName;
        public int resIcon;
        public Class<? extends Fragment> C;

        PAGE(int res_name, int res_icon, Class<? extends Fragment> C) {
            resName = res_name;
            res_icon = res_icon;
            this.C = C;
        }
    }


    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(PAGE.p1.resName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                getSupportActionBar().setTitle(PAGE.values()[position].resName);
                mViewPager.setCurrentItem(position, true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabIcons();
        mViewPager.setCurrentItem(openIndex);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        DriverUserManager.getInstance(this).addDriverUserListener(this);
        mSectionsPagerAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    protected void onPause() {
        DriverUserManager.getInstance(this).removeDriverUserListener(this);
        super.onPause();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onContextItemSelected(item);

    }

    public void switchToPageNum(int num) {
        mViewPager.setCurrentItem(num, true);
    }

    private void setupTabIcons() {
        for (int i = 0; i < PAGE.values().length; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setIcon(tabLayout.getTabAt(i).getIcon());
        }
    }

    public static void launch(Activity activity, int index) {
        openIndex = index;
        Intent intent = new Intent(activity, DriverActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        initActionItem(menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        initActionItem(menu);
        return true;
    }

    private void initActionItem(Menu menu) {
        if (DriverUserManager.getInstance(this).isLogin()) {
            getMenuInflater().inflate(R.menu.menu_driver_activity, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_driver_activity_notlogin, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            DriverUserManager.getInstance(this).logout(this);
            ModeOptionActivity.launchClearPref(this);
            return true;
        } else if (id == R.id.switch_to_passenger) {
            ModeOptionActivity.launchClearPref(this);
            return true;
        } else if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Class<? extends Fragment> C = PAGE.values()[position].C;
            try {
                if (position == 3 && !DriverUserManager.getInstance(DriverActivity.this).isLogin()) {
                    // If user not login, shows login UI
                    return new DriverLoginFragment();
                } else {
                    return C.newInstance();
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return PAGE.values().length;
        }

        @Override
        public int getItemPosition(Object object) {
            if (object instanceof DriverLoginFragment || object instanceof MyProfileFragment) {
                return POSITION_NONE;
            }
            return super.getItemPosition(object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(PAGE.values()[position].resName);
        }


    }


    @Override
    public void onFail() {

    }

    @Override
    public void onLoginStateChange() {
        mSectionsPagerAdapter.notifyDataSetChanged();
        invalidateOptionsMenu();
    }

    @Override
    public void onRegisterResult(boolean success) {

    }

    @Override
    public void onSmsCodeVerifyResult(boolean success) {

    }
}
