package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.schedule.ShareScheduleRequest;
import com.whilerain.utur.screen.passenger.manager.model.transport.schedule.HalfDayTripScheduleResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class ShareScheduleChooserActivity extends AppCompatActivity {
    public static int REQUEST_SCHEDULE = 301;

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.route)
    TextView vRoute;

    @BindView(R.id.date)
    TextView vDate;

    private int travelId;
    private String date;
    private String name;
    private int retruned_price;

    private List<String> list = new ArrayList<>();
    private ScheduleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_chooser);
        ButterKnife.bind(this);
        if(getIntent() != null){
            travelId = getIntent().getIntExtra("id", -1);
            date = getIntent().getStringExtra("date");
            name = getIntent().getStringExtra("name");
        }

        ShareScheduleRequest.build(travelId, date).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        Gson gson = new Gson();
                        final HalfDayTripScheduleResponse response = gson.fromJson(body, HalfDayTripScheduleResponse.class);
                        if (response.isStatus() && response.getResult().getTimetable() != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    retruned_price = response.getResult().getPrice();
                                    list = response.getResult().getTimetable();
                                    adapter.notifyDataSetChanged();
                                }
                            });

                        }
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailMessage(R.string.error_data_query_fail);
                    }
                });
            }

            @Override
            public void onFail() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showFailMessage(R.string.error_connection_fail);
                    }
                });
            }
        }).get();

    }

    @Override
    protected void onStart() {
        super.onStart();
        initViews();
    }

    private void showFailMessage(int res){
        showFailMessage(getString(res));
    }

    private void showFailMessage(String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }

    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter = new ScheduleAdapter();
        vList.setAdapter(adapter);
        vDate.setText(date);
        try {
            vRoute.setText(name);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void launchForResult(Activity activity, int travel_id, String date, String name){
        Intent it = new Intent(activity, ShareScheduleChooserActivity.class);
        it.putExtra("id", travel_id);
        it.putExtra("date", date);
        it.putExtra("name", name);
        activity.startActivityForResult(it, REQUEST_SCHEDULE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position){
        Gson gson = new Gson();
        String schedule = adapter.getItem(position);
        if(isValidSchedule(schedule)) {
            Intent it = new Intent();
            it.putExtra("schedule", schedule);
            it.putExtra("price", retruned_price);
            setResult(RESULT_OK, it);
            finish();
        }else{
            Toast.makeText(this, R.string.out_of_date, Toast.LENGTH_SHORT).show();
        }
    }

    private class ScheduleAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public String getItem(int position) {

            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            String schedule = list.get(position);
            if(convertView == null){
                convertView = LayoutInflater.from(ShareScheduleChooserActivity.this).inflate(R.layout.layout_transport_schedule_item, null);
            }

            TextView price = ButterKnife.findById(convertView, R.id.price);
            TextView time = ButterKnife.findById(convertView, R.id.time);
            time.setText(schedule);
//            price.setText(String.valueOf(retruned_price));
            if(isValidSchedule(schedule)){
                time.setEnabled(true);
            }else{
                time.setEnabled(false);
            }

            return convertView;
        }
    }

    private boolean isValidSchedule(String schedule){
        String[] dateSplit = date.split("-");
        String[] timeSplit = schedule.split(":");
        Calendar calendar = Calendar.getInstance();
        Calendar tartgetCalendar = Calendar.getInstance();
        tartgetCalendar.set(Calendar.YEAR, Integer.parseInt(dateSplit[0]));
        tartgetCalendar.set(Calendar.MONTH, Integer.parseInt(dateSplit[1]) - 1);
        tartgetCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateSplit[2]));
        tartgetCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeSplit[0]));
        tartgetCalendar.set(Calendar.MINUTE, Integer.parseInt(timeSplit[1]));

        if(tartgetCalendar.getTimeInMillis() + 30 * 60 * 1000 < calendar.getTimeInMillis()){
            return false;
        }else{
            return true;
        }
    }
}
