
package com.whilerain.utur.screen.passenger.manager.model.transport;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.whilerain.utur.utils.SimpleDataCache;


public class TransportResponse extends SimpleDataCache.CachePoJo {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();

    /**
     * 
     * @return
     *     The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("start_location")
        @Expose
        private Location startLocation;
        @SerializedName("end_location")
        @Expose
        private Location endLocation;
        @SerializedName("start_zone")
        @Expose
        private Zone startZone;
        @SerializedName("end_zone")
        @Expose
        private Zone endZone;

        /**
         *
         * @return
         *     The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The startLocation
         */
        public Location getStartLocation() {
            return startLocation;
        }

        /**
         *
         * @param startLocation
         *     The start_location
         */
        public void setStartLocation(Location startLocation) {
            this.startLocation = startLocation;
        }

        /**
         *
         * @return
         *     The endLocation
         */
        public Location getEndLocation() {
            return endLocation;
        }

        /**
         *
         * @param endLocation
         *     The end_location
         */
        public void setEndLocation(Location endLocation) {
            this.endLocation = endLocation;
        }

        /**
         *
         * @return
         *     The startZone
         */
        public Zone getStartZone() {
            return startZone;
        }

        /**
         *
         * @param startZone
         *     The start_zone
         */
        public void setStartZone(Zone startZone) {
            this.startZone = startZone;
        }

        /**
         *
         * @return
         *     The endZone
         */
        public Zone getEndZone() {
            return endZone;
        }

        /**
         *
         * @param endZone
         *     The end_zone
         */
        public void setEndZone(Zone endZone) {
            this.endZone = endZone;
        }

    }


    public static class Location {

        @SerializedName("gps")
        @Expose
        private List<Double> gps = new ArrayList<Double>();
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("zone_id")
        @Expose
        private Integer zoneId;
        @SerializedName("address")
        @Expose
        private String address;

        /**
         *
         * @return
         *     The gps
         */
        public List<Double> getGps() {
            return gps;
        }

        /**
         *
         * @param gps
         *     The gps
         */
        public void setGps(List<Double> gps) {
            this.gps = gps;
        }

        /**
         *
         * @return
         *     The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The zoneId
         */
        public Integer getZoneId() {
            return zoneId;
        }

        /**
         *
         * @param zoneId
         *     The zone_id
         */
        public void setZoneId(Integer zoneId) {
            this.zoneId = zoneId;
        }

        /**
         *
         * @return
         *     The address
         */
        public String getAddress() {
            return address;
        }

        /**
         *
         * @param address
         *     The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

    }

    public static class Zone {

        @SerializedName("focus")
        @Expose
        private List<Double> focus = new ArrayList<Double>();
        @SerializedName("area")
        @Expose
        private List<List<List<Double>>> area = new ArrayList<List<List<Double>>>();
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        /**
         *
         * @return
         *     The focus
         */
        public List<Double> getFocus() {
            return focus;
        }

        /**
         *
         * @param focus
         *     The focus
         */
        public void setFocus(List<Double> focus) {
            this.focus = focus;
        }

        /**
         *
         * @return
         *     The area
         */
        public List<List<List<Double>>> getArea() {
            return area;
        }

        /**
         *
         * @param area
         *     The area
         */
        public void setArea(List<List<List<Double>>> area) {
            this.area = area;
        }

        /**
         *
         * @return
         *     The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

    }

}
