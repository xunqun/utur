package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.BootstrapApi;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.googleplace.NearbyPlacesRequest;
import com.whilerain.utur.net.zone.ZoneLocationRequest;
import com.whilerain.utur.screen.passenger.manager.model.zone.location.ZoneLocationResponse;
import com.whilerain.utur.screen.passenger.manager.model.zone.location.ZoneLocationResponse.Location;
import com.whilerain.utur.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationChooserActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final int REQUEST_DEPARTURE_LOCATION = 400;
    public static final int REQUEST_DESTINATION_LOCATION = 401;
    private static final int PERMISSIONS_REQUEST_FINE_LOCATION = 3;
    private static final float ZOOM_LEVEL = 18;
    public static final int DISTANCE_TO_REFRESH = 1000;
    private int zoneId = 0;

    public enum TYPE {
        location, place;
    }

    @BindView(R.id.list)
    ExpandableListView vList;

    @BindView(R.id.list_frame)
    LinearLayout vListFrame;

    @BindView(R.id.search_button)
    ImageButton vMagnifier;

    @BindView(R.id.selected_location_frame)
    RelativeLayout vSelectFrame;

    @BindView(R.id.fab)
    FloatingActionButton vFab;

    @BindView(R.id.search)
    EditText vSearch;


    private GoogleMap mMap;
    private BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean locationBean;
    private ZoneLocationResponse response;
    private LocationListControler locationListControler;
    private HashMap<ZoneLocationResponse.Location, Marker> locationMarkerHashMap = new HashMap<>();
    private HashMap<Marker, ZoneLocationResponse.Location> markerLocationHashMap = new HashMap<>();
    private ZoneLocationResponse.Location selectedLocation;
    private NearbyPlacesRequest.Response.Place selectedPlace;
    private LatLng focusPosition;
    private boolean requestByUser = false;
    private TYPE type;

    private ApiRequest.ApiListener locationListener = new ApiRequest.ApiListener() {
        @Override
        public void onSeccess(ApiRequest apiRequest, String body) {
            try {
                Gson gson = new Gson();
                response = gson.fromJson(body, ZoneLocationResponse.class);
                if (response.getStatus()) {
                    if (mMap != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showNumButton();
                                setupMarker();

                                if (!requestByUser) {
                                    if (response.getResult().getLocations().size() > 0) {
                                        LatLng p = new LatLng(response.getResult().getLocations().get(0).getGps().get(0), response.getResult().getLocations().get(0).getGps().get(1));
                                        focusPosition = p;
                                        animCameraTo(p);
                                    }
                                }

                                if (locationListControler != null) {
                                    locationListControler.notifyLocationUpdate();
                                }
                            }
                        });

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFail() {

        }
    };


    private void showNumButton() {
        vMagnifier.setVisibility(View.VISIBLE);
    }

    public BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean getLocationBean() {
        return locationBean;
    }

    private void setupMarker() {

        mMap.clear();
        locationMarkerHashMap.clear();
        markerLocationHashMap.clear();
        for (int i = 0; i < response.getResult().getLocations().size(); i++) {
            ZoneLocationResponse.Location l = response.getResult().getLocations().get(i);
            if (!locationMarkerHashMap.containsKey(l)) {
                Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(l.getGps().get(0), l.getGps().get(1))).snippet(l.getAddress()).title(l.getName()));
                locationMarkerHashMap.put(l, marker);
                markerLocationHashMap.put(marker, l);
            }
        }

    }

    public static void launch(Activity activity, BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean locationBean, int requestCode) {
        Gson gson = new Gson();
        String data = gson.toJson(locationBean);
        Intent intent = new Intent(activity, LocationChooserActivity.class);
        intent.putExtra("location", data);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_chooser);
        ButterKnife.bind(this);
        initViews();

        if (getIntent().hasExtra("location")) {
            Gson gson = new Gson();
            String data = getIntent().getStringExtra("location");
            locationBean = gson.fromJson(data, BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean.class);
            zoneId = locationBean.getZone_id();
            focusPosition = new LatLng(locationBean.getGps().get(0), locationBean.getGps().get(1));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestLocation(locationBean.getGps().get(0), locationBean.getGps().get(1));
    }

    private void initViews() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        locationListControler = new LocationListControler(this, vMagnifier, vList, vListFrame);
        vSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });
        vSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("xunqun", "onTextChanged: ");

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("xunqun", "afterTextChanged: ");
                locationListControler.setSearchWord(s.toString());
            }
        });
    }

    private void requestLocation(Double lat, Double lng) {
        if (NetworkUtils.isOnline(this)) {
            ZoneLocationRequest.build(zoneId, lat, lng).setApiListener(locationListener).get();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.error_no_network)
                    .setPositiveButton(R.string.ok, null)
                    .create().show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {

                float[] result = new float[1];

                if (position.target != null && focusPosition != null && position.target.latitude != 0) {
                    android.location.Location.distanceBetween(position.target.latitude, position.target.longitude, focusPosition.latitude, focusPosition.longitude, result);
                    Log.d("xunqun", "distance: " + result[0] + " focus: " + focusPosition.toString());
                    if (result[0] > DISTANCE_TO_REFRESH) {
                        focusPosition = position.target;
                        requestLocation(focusPosition.latitude, focusPosition.longitude);
                    }
                }
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ZoneLocationResponse.Location l = markerLocationHashMap.get(marker);
                selectLocation(l);
                return true;
            }
        });
        if(focusPosition != null){
            requestLocation(focusPosition.latitude, focusPosition.longitude);
        }

    }

    public List<Location> getLocationList() {
        try {
            return response.getResult().getLocations();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    private void initMap() {
        if (response != null) {
            vMagnifier.setVisibility(View.VISIBLE);
            setupMarker();
            animCameraTo(new LatLng(response.getResult().getFocus().get(0), response.getResult().getFocus().get(1)));
            if (locationListControler != null) {
                locationListControler.notifyLocationUpdate();
            }
        }
    }

    private void animCameraTo(LatLng p) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(p, ZOOM_LEVEL));
    }


    @OnClick(R.id.search_button)
    protected void onNumClick() {
        locationListControler.showList();
        vSelectFrame.setVisibility(View.GONE);
    }

    @OnClick(R.id.back)
    protected void onBack() {
        locationListControler.hideList();
    }

    @OnClick(R.id.fab)
    protected void onFab() {
        Gson gson = new Gson();
        Intent it = new Intent();
        if (type == TYPE.location) {
            it.putExtra("location", gson.toJson(selectedLocation));
        } else {
            it.putExtra("googleplace", gson.toJson(selectedPlace));
        }
        setResult(RESULT_OK, it);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (vListFrame.getVisibility() == View.VISIBLE) {
            locationListControler.hideList();
        } else {
            super.onBackPressed();
        }
    }

    public void selectLocation(Location item) {

        // Restore all marker color
        for (Marker m : locationMarkerHashMap.values()) {
            m.setIcon(BitmapDescriptorFactory.defaultMarker());
        }
        selectedLocation = item;
        Marker marker = locationMarkerHashMap.get(item);
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
        vSelectFrame.setVisibility(View.VISIBLE);
        ((TextView) ButterKnife.findById(this, R.id.selected_location_title)).setText(item.getName());
        ((TextView) ButterKnife.findById(this, R.id.selected_location_addr)).setText(item.getAddress());

        focusPosition = new LatLng(item.getGps().get(0), item.getGps().get(1));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(focusPosition, ZOOM_LEVEL));
        type = TYPE.location;

    }

    public void selectPlace(NearbyPlacesRequest.Response.Place item) {
        // Restore all marker color
        for (Marker m : locationMarkerHashMap.values()) {
            m.setIcon(BitmapDescriptorFactory.defaultMarker());
        }
        selectedPlace = item;
        mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).position(new LatLng(item.getGeometry().getLocation().getLat(), item.getGeometry().getLocation().getLng())).snippet(item.getVicinity()).title(item.getName()));

        vSelectFrame.setVisibility(View.VISIBLE);
        ((TextView) ButterKnife.findById(this, R.id.selected_location_title)).setText(item.getName());
        ((TextView) ButterKnife.findById(this, R.id.selected_location_addr)).setText(item.getVicinity());

        focusPosition = new LatLng(item.getGeometry().getLocation().getLat(), item.getGeometry().getLocation().getLng());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(focusPosition, ZOOM_LEVEL));
        type = TYPE.place;
    }
}
