

package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.whilerain.utur.R;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.passenger_request.PackageOrderRequest;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.screen.passenger.manager.model.travel.FullDayTripDetailResponse;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.widget.DatePickerFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PackageDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.photo)
    SliderLayout vPhoto;

    @BindView(R.id.desc)
    TextView vDesc;

    @BindView(R.id.duration)
    TextView vDuration;

    @BindView(R.id.order_date)
    TextView vOrderDate;


    @BindView(R.id.order_count)
    TextView vOrderCount;

    @BindView(R.id.price)
    TextView vPrice;

    @BindView(R.id.country_code)
    EditText vCountryCode;

    @BindView(R.id.contact_num)
    EditText vContactNum;

    static FullDayTripDetailResponse response;
    static int travel_id;
    private String date;
    private int returnedPrice = 0;

    public static void launch(Activity activity, FullDayTripDetailResponse r, int id) {
        response = r;
        travel_id = id;
        activity.startActivity(new Intent(activity, PackageDetailActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_detail);
        ButterKnife.bind(this);
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView(response);
    }

    @Override
    protected void onStop() {
        vPhoto.stopAutoCycle();
        super.onStop();
    }

    private void initView(final FullDayTripDetailResponse response) {

        getSupportActionBar().setTitle(response.getResult().getName());
        vDesc.setText(response.getResult().getIntroduction());
        vDuration.setText(String.format(getString(R.string.duration), response.getResult().getDuration() / 60f));;
        returnedPrice = response.getResult().getPrice();
        vPrice.setText(String.valueOf(returnedPrice));

        String[] phone = StringUtils.getCountryCodeAndPhoneViaAccount(PassengerUserManager.getInstance(this).getAccount(this));
        vContactNum.setText(phone[1]);
        vCountryCode.setText(phone[0]);

        new AsyncTask<Void, Void, List<File>>(){
            List<File> pics = new ArrayList<>();
            List<String> titles = new ArrayList<String>();
            @Override
            protected List<File> doInBackground(Void... params) {

                Bitmap bitmap;
                try {

                    for (FullDayTripDetailResponse.ResultBean.ScenesBean scenesBean : response.getResult().getScenes()) {

                        byte[] decodedString = Base64.decode(scenesBean.getPic(), Base64.DEFAULT);
                        bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                        //create a file to write bitmap data
                        File f = new File(getCacheDir(), scenesBean.getName().replace("\u0096", ""));
                        try {
                            f.createNewFile();

                            //Convert bitmap to byte array
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                            byte[] bitmapdata = bos.toByteArray();

                            //write the bytes in file
                            FileOutputStream fos = new FileOutputStream(f);
                            fos.write(bitmapdata);
                            fos.flush();
                            fos.close();
                            pics.add(f);
                            titles.add(scenesBean.getName().replace("\u0096", ""));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                return pics;
            }

            @Override
            protected void onPostExecute(List<File> files) {
                super.onPostExecute(files);
                if(PackageDetailActivity.this != null) {

                    for(int i = 0; i < files.size(); i++) {
                        TextSliderView textSliderView = new TextSliderView(PackageDetailActivity.this);
                        textSliderView
                                .description(titles.get(i))
                                .image(files.get(i));

                        vPhoto.addSlider(textSliderView);
                    }
                }
            }
        }.execute();
    }

    @OnClick(R.id.add_one)
    public void addCount() {
        int count = Integer.valueOf(vOrderCount.getText().toString());
        count += 1;
        vOrderCount.setText(String.valueOf(count));

        setupPrice(returnedPrice);
    }

    @OnClick(R.id.minus_one)
    public void minusCount() {
        int count = Integer.valueOf(vOrderCount.getText().toString());
        count -= 1;
        if (count > 0) {
            vOrderCount.setText(String.valueOf(count));
        }
        setupPrice(returnedPrice);
    }

    @OnClick(R.id.order_date_wrapper)
    public void onOrderDateClick() {
        DatePickerFragment dialog = new DatePickerFragment();
        dialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date = String.format("%4d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                vOrderDate.setText(date);
                vOrderDate.setError(null);
            }
        });
        dialog.show(getSupportFragmentManager(), "");
    }



    @OnClick(R.id.send)
    public void onSend(){


        vOrderDate.setError(null);
        vContactNum.setError(null);
        vCountryCode.setError(null);

        if(date.length() == 0){
            vOrderDate.setError(getString(R.string.error_can_not_empty));
            return;
        }
        if(!TextUtils.isDigitsOnly(vPrice.getText().toString())){
            Toast.makeText(PackageDetailActivity.this, R.string.error_can_not_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        if(vContactNum.getText().toString().length() == 0){
            vContactNum.setError(getString(R.string.error_can_not_empty));
            return;
        }

        if(vCountryCode.getText().toString().length() == 0){
            vCountryCode.setError(getString(R.string.error_can_not_empty));
            return;
        }

        String message = String.format(getString(R.string.order_detail),response.getResult().getName(),vOrderCount.getText().toString(), vPrice.getText().toString());
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(R.string.order)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestOrder();
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .create().show();


    }

    private void requestOrder() {
        String contactPhone =  vContactNum.getText().toString();
        if(contactPhone.charAt(0) == '0') contactPhone = contactPhone.substring(1);
        String contactNum = vCountryCode.getText().toString() + contactPhone;
        PackageOrderRequest.build(travel_id,
                PassengerUserManager.getInstance(this).getUserInfo().getUuid(),
                date,
                Integer.valueOf(vOrderCount.getText().toString()), contactNum
        ).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if(jsonObject.getBoolean("status")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showSuccessDialog();
                            }
                        });
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showErrorDialog(R.string.error_connection_fail);
                    }
                });
            }

            @Override
            public void onFail() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showErrorDialog(R.string.error_connection_fail);
                    }
                });

            }
        }).post();
    }

    private void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.order_scceed))
                .setCancelable(false)
                .setPositiveButton(R.string.launch_history, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HistoryActivity.launchClearTop(PackageDetailActivity.this);
                    }
                })
                .create().show();
    }

    private void showErrorDialog(int msgRes) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(msgRes))
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupPrice(int price) {

            vPrice.setText(String.valueOf(price));

    }
}
