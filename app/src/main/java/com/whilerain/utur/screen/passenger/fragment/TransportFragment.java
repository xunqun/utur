package com.whilerain.utur.screen.passenger.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.BootstrapApi;
import com.whilerain.utur.screen.ToChooserActivity;
import com.whilerain.utur.screen.passenger.FromChooserActivity;
import com.whilerain.utur.screen.passenger.PassengerActivity;
import com.whilerain.utur.screen.passenger.TransportDetailActivity;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.screen.passenger.manager.TransportListManager;
import com.whilerain.utur.utils.NetworkUtils;
import com.whilerain.utur.widget.DatePickerFragment;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.whilerain.utur.R.id.date;

/**
 * Created by xunqun on 2017/1/22.
 */

public class TransportFragment extends Fragment implements TransportListManager.Listener {

    @BindView(R.id.root)
    ViewGroup vRoot;

    @BindView(R.id.date)
    Button vDate;

    @BindView(R.id.from)
    TextView vFrom;

    @BindView(R.id.to)
    TextView vTo;

    @BindView(R.id.title1)
    TextView vTitle1;

    @BindView(R.id.title2)
    TextView vTitle2;

    @BindView(R.id.icon1)
    ImageView vIcon1;

    @BindView(R.id.icon2)
    ImageView vIcon2;

    @BindView(R.id.progressBar)
    ProgressBar vProgressBar;

    @BindView(R.id.description)
    TextView vDescription;

    private BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean fromLocation;
    private BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean toLocation;
    private boolean isFromStart = true;
    private String dateString;
    private List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean> fromStartLocations;
    private List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean> fromEndLocations;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransportListManager.getInstance().setListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_populartour, null);
        ButterKnife.bind(this, view);
        initViews();

        if (NetworkUtils.isOnline(getContext())) {
            //do request
            TransportListManager.getInstance().requestFromServer();
            vProgressBar.setVisibility(View.VISIBLE);
        } else {
            Snackbar snackbar = Snackbar.make(vRoot, R.string.error_no_network, Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.RED);
            snackbar.show();
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FromChooserActivity.REQUEST) {
            if (FromChooserActivity.sChoosedLocation == null) return;
            if (isFromStart) {
                fromLocation = ((BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean) FromChooserActivity.sChoosedLocation).getStart_location();
                toLocation = ((BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean) FromChooserActivity.sChoosedLocation).getTransports().get(0);
            } else {
                fromLocation = ((BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean) FromChooserActivity.sChoosedLocation).getEnd_location();
                toLocation = ((BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean) FromChooserActivity.sChoosedLocation).getTransports().get(0);
            }
            initLocationsViews();
        } else if (requestCode == ToChooserActivity.REQUEST) {
            toLocation = ToChooserActivity.sChooseedTransport;
            initLocationsViews();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.from)
    void onFromClick() {
        if (isFromStart && fromStartLocations.size() > 0) {
            FromChooserActivity.launch(this, fromStartLocations, null);
        } else if (!isFromStart && fromEndLocations.size() > 0) {
            FromChooserActivity.launch(this, null, fromEndLocations);
        }
    }

    @OnClick(R.id.to)
    void onToClick() {
        if (isFromStart) {
            for (BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean b : fromStartLocations) {
                if (b.getStart_location().getId() == fromLocation.getId()) {
                    ToChooserActivity.launch(this, b.getTransports());
                    break;
                }
            }
        } else {
            for (BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean b : fromEndLocations) {
                if (b.getEnd_location().getId() == fromLocation.getId()) {
                    ToChooserActivity.launch(this, b.getTransports());
                    break;
                }
            }
        }
    }

    @OnClick(R.id.submit)
    void onSubmit() {
        if (!PassengerUserManager.getInstance(getContext()).isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(R.string.require_loged_in)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((PassengerActivity)getActivity()).switchToPageNum(3);
                        }
                    })
                    .create().show();
            return;
        }

        if (isFromStart) {
            TransportDetailActivity.launch(getActivity(), fromLocation, toLocation.getEnd_location(), toLocation.getTransports_id(), dateString);
        } else {
            TransportDetailActivity.launch(getActivity(), fromLocation, toLocation.getStart_location(), toLocation.getTransports_id(), dateString);
        }
    }

    private void initViews() {

        fromStartLocations = TransportListManager.getInstance().getFromStartList();
        fromEndLocations = TransportListManager.getInstance().getFromEndList();
        try {
            if (isFromStart) {
                fromLocation = fromStartLocations.get(0).getStart_location();
                toLocation = fromStartLocations.get(0).getTransports().get(0);

            } else {
                fromLocation = fromEndLocations.get(0).getEnd_location();
                toLocation = fromEndLocations.get(0).getTransports().get(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        initLocationsViews();

        // date
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int monthOfYear = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        dateString = String.format("%4d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
        vDate.setText(dateString);
    }

    private void initLocationsViews() {

        try {
            if (toLocation.getCategory().equalsIgnoreCase("station")) {

                vTitle1.setText(R.string.vertical_destination);
                vTitle2.setText(R.string.vertical_station);
                vIcon1.setImageResource(R.drawable.ic_beach_access_black_24dp);
                vIcon2.setImageResource(R.drawable.ic_directions_bus_black_24dp);

            } else {
                vTitle1.setText(R.string.vertical_station);
                vIcon1.setImageResource(R.drawable.ic_directions_bus_black_24dp);
                vTitle2.setText(R.string.vertical_destination);
                vIcon2.setImageResource(R.drawable.ic_beach_access_black_24dp);

            }
            vFrom.setText(fromLocation.getName());
            if (isFromStart) {
                vTo.setText(toLocation.getEnd_location().getName());
                vDescription.setText(String.format(getString(R.string.from_to), fromLocation.getName(), toLocation.getEnd_location().getName()));
            } else {
                vTo.setText(toLocation.getStart_location().getName());
                vDescription.setText(String.format(getString(R.string.from_to), fromLocation.getName(), toLocation.getStart_location().getName()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChanged() {
        initViews();
        vProgressBar.setVisibility(View.INVISIBLE);
    }

    @OnClick(date)
    public void onDate() {
        DatePickerFragment dialog = new DatePickerFragment();
        dialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateString = String.format("%4d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                vDate.setText(dateString);
            }
        });
        dialog.show(getFragmentManager(), "");
    }

    @OnClick(R.id.switch_dest)
    public void onSwitchDest() {
        isFromStart = !isFromStart;

        if (isFromStart) {
            BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean b = TransportListManager.getInstance().findFromStartTransportsBeanById(toLocation.getStart_location().getId());

            for (BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean t : b.getTransports()) {
                if ((t.getStart_location() != null && t.getStart_location().getId() == fromLocation.getId()) ||
                        t.getEnd_location() != null && t.getEnd_location().getId() == fromLocation.getId()) {
                    toLocation = t;
                    break;
                }
            }
            fromLocation = b.getStart_location();

        } else {
            BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean b = TransportListManager.getInstance().findFromEndTransportsBeanById(toLocation.getEnd_location().getId());

            for (BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean t : b.getTransports()) {
                if ((t.getStart_location() != null && t.getStart_location().getId() == fromLocation.getId()) ||
                        t.getEnd_location() != null && t.getEnd_location().getId() == fromLocation.getId()) {
                    toLocation = t;
                    break;
                }
            }
            fromLocation = b.getEnd_location();
        }
        initLocationsViews();
    }
}
