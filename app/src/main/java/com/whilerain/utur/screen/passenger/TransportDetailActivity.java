package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.BootstrapApi;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.googleplace.NearbyPlacesRequest;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.screen.passenger.manager.TransportManager;
import com.whilerain.utur.screen.passenger.manager.model.transport.schedule.Schedule;
import com.whilerain.utur.screen.passenger.manager.model.zone.location.ZoneLocationResponse.Location;
import com.whilerain.utur.widget.DatePickerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransportDetailActivity extends AppCompatActivity {

    @BindView(R.id.departure)
    TextView vDeparture;

    @BindView(R.id.departure_addr)
    TextView vDepartureAddr;

    @BindView(R.id.destination)
    TextView vDestination;

    @BindView(R.id.destination_addr)
    TextView vDestinationAddr;

    @BindView(R.id.order_date)
    TextView vOrderDate;

    @BindView(R.id.order_time)
    TextView vOrderTime;

    @BindView(R.id.order_count)
    TextView vOrderCount;

    @BindView(R.id.price)
    TextView vPrice;

    @BindView(R.id.contact_num)
    EditText vContactNum;

    @BindView(R.id.country_code)
    EditText vCountryCode;

    //    private static Result data;
    private static BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean fromLocation;
    private static BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean toLocation;
    private static int transportId;
    private static String date;

    private Schedule schedule;
    private Location departureLocation;
    private Location destinationLocation;
    private NearbyPlacesRequest.Response.Place departurePlace;
    private NearbyPlacesRequest.Response.Place destinationPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Gson gson = new Gson();
        if (requestCode == TransportScheduleChooserActivity.REQUEST_SCHEDULE && resultCode == RESULT_OK) {
            schedule = gson.fromJson(data.getStringExtra("schedule"), Schedule.class);
            vOrderTime.setText(schedule.getTime());
            setupPrice();
        }else if(requestCode == LocationChooserActivity.REQUEST_DEPARTURE_LOCATION && resultCode == RESULT_OK){
            departureLocation = null;
            if(data.hasExtra("location")) {
                departureLocation = gson.fromJson(data.getStringExtra("location"), Location.class);
                vDeparture.setText(departureLocation.getName());
                vDepartureAddr.setText(departureLocation.getAddress());
            }else if(data.hasExtra("googleplace")){

                departurePlace = gson.fromJson(data.getStringExtra("googleplace"), NearbyPlacesRequest.Response.Place.class);
                vDeparture.setText(departurePlace.getName());
                vDepartureAddr.setText(departurePlace.getVicinity());
            }
        }else if(requestCode == LocationChooserActivity.REQUEST_DESTINATION_LOCATION && resultCode == RESULT_OK){
            destinationLocation = null;
            if(data.hasExtra("location")) {
                destinationLocation = gson.fromJson(data.getStringExtra("location"), Location.class);
                vDestination.setText(destinationLocation.getName());
                vDestinationAddr.setText(destinationLocation.getAddress());
            }else if(data.hasExtra("googleplace")){

                destinationPlace = gson.fromJson(data.getStringExtra("googleplace"), NearbyPlacesRequest.Response.Place.class);
                vDestination.setText(destinationPlace.getName());
                vDestinationAddr.setText(destinationPlace.getVicinity());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setupPrice() {
        if (schedule != null) {
            int num = Integer.valueOf(vOrderCount.getText().toString());
            vPrice.setText(String.valueOf(schedule.getPrice() * num));
        } else {
            vPrice.setText("---");
        }
    }

    private void initView() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vDeparture.setText(fromLocation.getName());
        vDepartureAddr.setText(fromLocation.getAddress());
        vDestination.setText(toLocation.getName());
        vDestinationAddr.setText(toLocation.getAddress());
        vOrderDate.setText(date);
        if(PassengerUserManager.getInstance(this).isLogin()) {
            vCountryCode.setText(PassengerUserManager.getInstance(this).getAccount(this).substring(0, 3));
            vContactNum.setText(PassengerUserManager.getInstance(this).getAccount(this).substring(3));
        }
    }

    public static void launch(Activity activity, BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean from , BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean to, int id, String dateString){
        fromLocation  = from;
        toLocation = to;
        transportId = id;
        date = dateString;
        Intent it = new Intent(activity, TransportDetailActivity.class);
        activity.startActivity(it);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.add_one)
    public void addCount() {
        int count = Integer.valueOf(vOrderCount.getText().toString());
        count += 1;
        vOrderCount.setText(String.valueOf(count));
        setupPrice();
    }

    @OnClick(R.id.minus_one)
    public void minusCount() {
        int count = Integer.valueOf(vOrderCount.getText().toString());
        count -= 1;
        if (count > 0) {
            vOrderCount.setText(String.valueOf(count));
            setupPrice();
        }
    }

    @OnClick(R.id.order_time_wrapper)
    public void onOrderTimeClick() {
        vOrderDate.setError(null);
        if (date != null && date.length() > 0) {
            TransportScheduleChooserActivity.launchForResult(this, transportId, date);
        } else {
            vOrderDate.setError(getString(R.string.error_can_not_empty));
        }
    }

    @OnClick(R.id.order_date_wrapper)
    public void onOrderDateClick() {
        DatePickerFragment dialog = new DatePickerFragment();
        dialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date = String.format("%4d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                vOrderDate.setText(date);
                vOrderDate.setError(null);
            }
        });
        dialog.show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.send)
    public void onSend() {
        if(!PassengerUserManager.getInstance(this).isLogin()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.require_loged_in)
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show();
            return;
        }

        vOrderDate.setError(null);
        vContactNum.setError(null);
        vCountryCode.setError(null);

        if(schedule == null){
            vOrderTime.setError(getString(R.string.error_can_not_empty));
            return;
        }

        if(vContactNum.getText().toString().length() == 0){
            vContactNum.setError(getString(R.string.error_can_not_empty));
            return;
        }

        if(vCountryCode.getText().toString().length() == 0){
            vCountryCode.setError(getString(R.string.error_can_not_empty));
            return;
        }



        String name = vDeparture.getText().toString() + " ► " + vDestination.getText().toString();
        String message = String.format(getString(R.string.order_detail), name, vOrderCount.getText().toString(), vPrice.getText().toString());
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(R.string.order)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestOrder();
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .create().show();


    }

    private void requestOrder() {
        int fromId = fromLocation.getId();
        int toId = toLocation.getId();

        if(departureLocation != null){
            fromId = departureLocation.getId();
        }

        if(destinationLocation != null){
            toId = destinationLocation.getId();
        }

        String contactPhone =  vContactNum.getText().toString();
        if(contactPhone.charAt(0) == '0') contactPhone = contactPhone.substring(1);

        String contactNum = vCountryCode.getText().toString() + contactPhone;
        String departurePlaceString = departurePlace == null ? null : getPlaceString(departurePlace);
        String destinationPlaceString = destinationPlace == null ? null : getPlaceString(destinationPlace);

        TransportManager.getInstance(this).requestOrder(schedule.getId(), transportId,
                PassengerUserManager.getInstance(this).getUserInfo().getUuid(),
                date,
                Integer.valueOf(vOrderCount.getText().toString()),
                fromId,
                toId,
                departurePlaceString,
                destinationPlaceString,
                contactNum,
                new ApiRequest.ApiListener() {
                    @Override
                    public void onSeccess(ApiRequest apiRequest, String body) {
                        try {
                            JSONObject jsonObject = new JSONObject(body);
                            if (jsonObject.getBoolean("status")) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(TransportDetailActivity.this);
                                        builder.setMessage(getString(R.string.order_scceed))
                                                .setCancelable(false)
                                                .setPositiveButton(R.string.launch_history, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        HistoryActivity.launchClearTop(TransportDetailActivity.this);
                                                    }
                                                })
                                                .create().show();

                                    }
                                });
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showErrorDialog(R.string.error_data_query_fail);
                            }
                        });
                    }

                    @Override
                    public void onFail() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showErrorDialog(R.string.error_connection_fail);
                            }
                        });
                    }
                }
        );
    }

    private String getPlaceString(NearbyPlacesRequest.Response.Place place) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("address", place.getVicinity());
            JSONArray gps = new JSONArray();
            gps.put(place.getGeometry().getLocation().getLat());
            gps.put(place.getGeometry().getLocation().getLng());
            jsonObject.put("gps", gps);
            jsonObject.put("name", place.getName());
            return jsonObject.toString();

        }catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    @OnClick(R.id.destination_wrapper)
    protected void onDestinationLocation(){
        LocationChooserActivity.launch(this, toLocation, LocationChooserActivity.REQUEST_DESTINATION_LOCATION);
    }

    @OnClick(R.id.departure_wrapper)
    protected void onDepartureLocation(){
        LocationChooserActivity.launch(this, fromLocation, LocationChooserActivity.REQUEST_DEPARTURE_LOCATION);
    }

    private void showErrorDialog(int msgRes) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(msgRes))
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }
}
