package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.model.ApiReference;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.bootstrap.CarDetailRequest;
import com.whilerain.utur.net.order.CancelOrderRequest;
import com.whilerain.utur.net.order.CancelRequestRequest;
import com.whilerain.utur.screen.passenger.manager.model.car.CarResponse;
import com.whilerain.utur.screen.passenger.manager.model.passenger.order.PassengerHistoryResponse;
import com.whilerain.utur.utils.CarPrefHandler;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EngagedDriverInfoOrderActivity extends AppCompatActivity {

    private static Object object;

    @BindView(R.id.name)
    TextView vName;

    @BindView(R.id.phone)
    TextView vPhone;

    @BindView(R.id.departure_name)
    TextView vDepartureName;

    @BindView(R.id.departure_addr)
    TextView vDepartureAddr;

    @BindView(R.id.destination_name)
    TextView vDestinationName;

    @BindView(R.id.destination_addr)
    TextView vDestinationAddr;

    @BindView(R.id.car_table)
    TableLayout vCarTable;

    @BindView(R.id.car_title)
    TextView vCarTitle;

    @BindView(R.id.car_model)
    TextView vCarModel;

    @BindView(R.id.car_num)
    TextView vCarNum;

    @BindView(R.id.photo)
    ImageView vPhoto;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.call)
    ImageView vCall;

    @BindView(R.id.order_info)
    TextView vOrderInfo;

    @BindView(R.id.name_cell)
    TextView vNameCell;

    @BindView(R.id.phone_cell)
    TextView vPhoneCell;

    @BindView(R.id.date)
    TextView vOrderDate;

    @BindView(R.id.time)
    TextView vOrderTime;

    @BindView(R.id.count)
    TextView vCount;

    @BindView(R.id.price)
    TextView vPrice;

    String mId;
    private String mState;
    private CarResponse carResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engaged_driver_transport_info);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.cancel) {
            AlertDialog.Builder builder = new AlertDialog.Builder(EngagedDriverInfoOrderActivity.this);
            builder.setMessage(R.string.do_you_want_to_cancel_order)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (object instanceof PassengerHistoryResponse.ResultBean.OrdersBean.Order) {
                                cancelOrder();
                            } else {
                                cancelRequest();
                            }
                        }
                    }).setNegativeButton(android.R.string.cancel, null);
            builder.create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void cancelRequest() {
        CancelRequestRequest.build(mId).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        showCancelDialog(R.string.cancel_success);
                        finish();
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showErrorMessage(R.string.error_data_query_fail);
            }

            @Override
            public void onFail() {
                showErrorMessage(R.string.error_connection_fail);
            }
        }).put();
    }

    private void cancelOrder() {
        CancelOrderRequest.build(mId).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        showCancelDialog(R.string.cancel_explain);
                        finish();
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showErrorMessage(R.string.error_data_query_fail);
            }

            @Override
            public void onFail() {
                showErrorMessage(R.string.error_connection_fail);
            }
        }).put();
    }

    private void showErrorMessage(final int res) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(EngagedDriverInfoOrderActivity.this);
                builder.setMessage(res).setPositiveButton(android.R.string.ok, null).create().show();
            }
        });
    }

    private void showCancelDialog(final int res) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(EngagedDriverInfoOrderActivity.this);
                builder.setMessage(res).setPositiveButton(android.R.string.ok, null).create().show();
            }
        });
    }

    private void initViews() {
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (object instanceof PassengerHistoryResponse.ResultBean.OrdersBean.Order) {
            // this is a request has accepted by a driver

            final PassengerHistoryResponse.ResultBean.OrdersBean.Order order = (PassengerHistoryResponse.ResultBean.OrdersBean.Order) object;
            mId = order.getOrder_id();
            mState = order.getStatus();

            vOrderInfo.setText(vOrderInfo.getText().toString() + " (" + mId + ")");
            vOrderDate.setText(order.getStart_time());
            if (order.getCategory().equals(ApiReference.TransType.TRANSPORT.toString())) {

                PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location();
                startLocation = startLocation == null ? order.getRequests().get(0).getStart_location_custom() : startLocation;
                PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location();
                endLocation = endLocation == null ? order.getRequests().get(0).getEnd_location_custom() : endLocation;


                vDepartureName.setText(startLocation.getName());
                vDepartureAddr.setText(startLocation.getAddress());

                vDestinationName.setText(endLocation.getName());
                vDestinationAddr.setText(endLocation.getAddress());


            } else if (order.getCategory().equals(ApiReference.TransType.SHARE.toString())) {
                vDestinationName.setVisibility(View.GONE);
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(StringUtils.trimSpecialString(order.getRequests().get(0).getName()));
                vDepartureAddr.setVisibility(View.GONE);
            } else if (order.getCategory().equals(ApiReference.TransType.TAXI.toString())) {

                PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location_custom();
                PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location_custom();

                vDestinationName.setText(endLocation.getAddress());
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(startLocation.getAddress());
                vDepartureAddr.setVisibility(View.GONE);
            } else {
                vDestinationName.setVisibility(View.GONE);
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(StringUtils.trimSpecialString(order.getRequests().get(0).getName()));
                vDepartureAddr.setVisibility(View.GONE);
            }

            if (order.getDriver() != null) {
                vName.setText(order.getDriver().getName());
                vPhone.setText(order.getDriver().getPhone());
                vCall.setVisibility(View.VISIBLE);
            } else {
                vNameCell.setVisibility(View.INVISIBLE);
                vName.setText(R.string.requesting);
                vPhoneCell.setVisibility(View.INVISIBLE);
                vCall.setVisibility(View.GONE);
            }

            if (order.getCar() != null) {
                String responseBody = CarPrefHandler.getSharedPrefences(this).getString(String.valueOf(order.getCar().getId()), "");
                if (responseBody.length() > 0) {
                    setupCar(order.getCar().getId(), responseBody);
                } else {

                    CarDetailRequest.build(order.getCar().getId()).setApiListener(new ApiRequest.ApiListener() {
                        @Override
                        public void onSeccess(ApiRequest apiRequest, String body) {
                            setupCar(order.getCar().getId(), body);

                        }

                        @Override
                        public void onFail() {
                            showToast(R.string.error_connection_fail);
                        }
                    }).get();
                }

            } else {
                vCarTitle.setVisibility(View.GONE);
                vCarTable.setVisibility(View.GONE);
            }
        } else {
            // this is a request still has not accepted by a driver

            PassengerHistoryResponse.ResultBean.RequestsBean.Request request = (PassengerHistoryResponse.ResultBean.RequestsBean.Request) object;
            mId = request.getRequest_id();
            mState = request.getStatus();

            vOrderInfo.setText(vOrderInfo.getText().toString() + "(" + mId + ")");

            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", request.getTimerange().get(0));
                vOrderDate.setText(timeStr.substring(0, 10));
                vOrderTime.setText(timeStr.substring(11));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            vCount.setText(String.valueOf(request.getPassenger_count()));
            if (request.getCategory().equals(ApiReference.TransType.TRANSPORT.toString())) {
                if (request.getStart_location() != null) {
                    if (request.getStart_location().getName() != null) {
                        vDepartureName.setText(request.getStart_location().getName());
                    }
                    vDepartureAddr.setText(request.getStart_location().getAddress());


                } else {
                    if (request.getStart_location_custom().getName() != null) {
                        vDepartureName.setText(request.getStart_location_custom().getName());
                    }
                    vDepartureAddr.setText(request.getStart_location_custom().getAddress());


                }


                if (request.getEnd_location() != null) {
                    if (request.getEnd_location().getName() != null) {
                        vDestinationName.setText(request.getEnd_location().getName());
                    }
                    vDestinationAddr.setText(request.getEnd_location().getAddress());

                } else {
                    if (request.getEnd_location_custom().getName() != null) {
                        vDestinationName.setText(request.getEnd_location_custom().getName());
                    }
                    vDestinationAddr.setText(request.getEnd_location_custom().getAddress());
                }


            } else if (request.getCategory().equals(ApiReference.TransType.SHARE.toString())) {
                vDestinationName.setText(request.getName());
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setVisibility(View.GONE);
                vDepartureAddr.setVisibility(View.GONE);
            } else if(request.getCategory().equals(ApiReference.TransType.TAXI.toString())){

                PassengerHistoryResponse.ResultBean.RequestsBean.Request.LocationBean startLocation = request.getStart_location_custom();
                PassengerHistoryResponse.ResultBean.RequestsBean.Request.LocationBean endLocation = request.getEnd_location_custom();

                vDestinationName.setText(endLocation.getAddress());
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(startLocation.getAddress());
                vDepartureAddr.setVisibility(View.GONE);

            } else if (request.getCategory().equals(ApiReference.TransType.PACKAGE.toString())) {
                vDepartureName.setText(request.getName());
            }

            vName.setText(R.string.requesting);
            vNameCell.setVisibility(View.INVISIBLE);
            vPhoneCell.setVisibility(View.INVISIBLE);
            vCarTitle.setVisibility(View.GONE);
            vCarTable.setVisibility(View.GONE);
            if (request.getDiscount() != 0) {
                vPrice.setText(String.format("%.0f 元", request.getPrice() * request.getPassenger_count() * request.getDiscount()));
            } else {
                vPrice.setText(String.format("%d 元", request.getPrice() * request.getPassenger_count()));
            }
        }

    }

    private void setupCar(int id, String body) {
        Gson gson = new Gson();
        carResponse = gson.fromJson(body, CarResponse.class);
        if (carResponse.isStatus()) {
            CarPrefHandler.getEditor(this).putString(String.valueOf(id), body).apply();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        vCarModel.setText(carResponse.getResult().getModel());
                        vCarNum.setText(carResponse.getResult().getLicenseplate());
                        try {
                            byte[] decodedString = Base64.decode(carResponse.getResult().getPicture(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            vPhoto.setImageBitmap(decodedByte);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(R.string.error_data_query_fail);
                    }
                }
            });

        }

    }

    private void showToast(final int res) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(EngagedDriverInfoOrderActivity.this, res, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void launch(Activity activity, Object r) {
        activity.startActivity(new Intent(activity, EngagedDriverInfoOrderActivity.class));
        object = r;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mState.equals("queue") || mState.equals("proceeding")) {
            menu.findItem(R.id.cancel).setVisible(true);
        } else {
            menu.findItem(R.id.cancel).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_engaged_driver_info, menu);
        return true;
    }

    @OnClick(R.id.call)
    void onCall() {
        String num = vPhone.getText().toString();
        if (num.length() > 0)
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", num, null)));
    }

    @OnClick(R.id.departure_name)
    void onDepartureName() {
        if (object instanceof PassengerHistoryResponse.ResultBean.OrdersBean.Order) {


            final PassengerHistoryResponse.ResultBean.OrdersBean.Order order = (PassengerHistoryResponse.ResultBean.OrdersBean.Order) object;
            PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location();
            startLocation = startLocation == null ? order.getRequests().get(0).getStart_location_custom() : startLocation;

            UiUtils.showLocation(this, String.valueOf(startLocation.getGps().get(0)), String.valueOf(startLocation.getGps().get(1)), startLocation.getAddress());
        } else {
            PassengerHistoryResponse.ResultBean.RequestsBean.Request request = (PassengerHistoryResponse.ResultBean.RequestsBean.Request) object;
            PassengerHistoryResponse.ResultBean.RequestsBean.Request.LocationBean startLocation = request.getStart_location();
            startLocation = startLocation == null ? request.getStart_location_custom() : startLocation;
            UiUtils.showLocation(this, String.valueOf(startLocation.getGps().get(0)), String.valueOf(startLocation.getGps().get(1)), startLocation.getAddress());
        }
    }

    @OnClick(R.id.destination_name)
    void onDestination() {
        if (object instanceof PassengerHistoryResponse.ResultBean.OrdersBean.Order) {
            final PassengerHistoryResponse.ResultBean.OrdersBean.Order order = (PassengerHistoryResponse.ResultBean.OrdersBean.Order) object;

            PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location();
            endLocation = endLocation == null ? order.getRequests().get(0).getEnd_location_custom() : endLocation;

            UiUtils.showLocation(this, String.valueOf(endLocation.getGps().get(0)), String.valueOf(endLocation.getGps().get(1)), endLocation.getAddress());
        } else {


            PassengerHistoryResponse.ResultBean.RequestsBean.Request request = (PassengerHistoryResponse.ResultBean.RequestsBean.Request) object;
            PassengerHistoryResponse.ResultBean.RequestsBean.Request.LocationBean endlocation = request.getEnd_location();
            endlocation = endlocation == null ? request.getEnd_location_custom() : endlocation;
            UiUtils.showLocation(this, String.valueOf(endlocation.getGps().get(0)), String.valueOf(endlocation.getGps().get(1)), endlocation.getAddress());
        }
    }

}
