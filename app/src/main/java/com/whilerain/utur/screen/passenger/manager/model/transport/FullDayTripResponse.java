package com.whilerain.utur.screen.passenger.manager.model.transport;

import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/6/15.
 */
public class FullDayTripResponse extends SimpleDataCache.CachePoJo{

    /**
     * status : true
     * result : [{"id":7,"name":"包車整天遊墾丁","category":"package","back":0,"type":"all_day","start_location":{"gps":[21.9456877,120.7972097],"id":470,"name":"墾丁","zone_id":2,"address":"屏東縣恆春鎮墾丁路237號"},"end_location":null,"start_zone":{"focus":[21.945165,120.7979981],"area":[[[120.81124305725098,21.962245837143676],[120.84746360778809,21.960972199469655],[120.87132453918457,21.898709253832585],[120.84952354431152,21.8910638444382],[120.76746940612793,21.949349728374518],[120.81124305725098,21.962245837143676]]],"id":2,"name":"墾丁"},"end_zone":{"focus":[21.945165,120.7979981],"area":[[[120.81124305725098,21.962245837143676],[120.84746360778809,21.960972199469655],[120.87132453918457,21.898709253832585],[120.84952354431152,21.8910638444382],[120.76746940612793,21.949349728374518],[120.81124305725098,21.962245837143676]]],"id":2,"name":"墾丁"}}]
     */

    private boolean status;
    /**
     * id : 7
     * name : 包車整天遊墾丁
     * category : package
     * back : 0
     * type : all_day
     * start_location : {"gps":[21.9456877,120.7972097],"id":470,"name":"墾丁","zone_id":2,"address":"屏東縣恆春鎮墾丁路237號"}
     * end_location : null
     * start_zone : {"focus":[21.945165,120.7979981],"area":[[[120.81124305725098,21.962245837143676],[120.84746360778809,21.960972199469655],[120.87132453918457,21.898709253832585],[120.84952354431152,21.8910638444382],[120.76746940612793,21.949349728374518],[120.81124305725098,21.962245837143676]]],"id":2,"name":"墾丁"}
     * end_zone : {"focus":[21.945165,120.7979981],"area":[[[120.81124305725098,21.962245837143676],[120.84746360778809,21.960972199469655],[120.87132453918457,21.898709253832585],[120.84952354431152,21.8910638444382],[120.76746940612793,21.949349728374518],[120.81124305725098,21.962245837143676]]],"id":2,"name":"墾丁"}
     */

    private List<ResultBean> result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        private int id;
        private String name;
        private String category;
        private int back;
        private String type;
        /**
         * gps : [21.9456877,120.7972097]
         * id : 470
         * name : 墾丁
         * zone_id : 2
         * address : 屏東縣恆春鎮墾丁路237號
         */

        private LocationBean start_location;
        private LocationBean end_location;
        /**
         * focus : [21.945165,120.7979981]
         * area : [[[120.81124305725098,21.962245837143676],[120.84746360778809,21.960972199469655],[120.87132453918457,21.898709253832585],[120.84952354431152,21.8910638444382],[120.76746940612793,21.949349728374518],[120.81124305725098,21.962245837143676]]]
         * id : 2
         * name : 墾丁
         */

        private ZoneBean start_zone;
        /**
         * focus : [21.945165,120.7979981]
         * area : [[[120.81124305725098,21.962245837143676],[120.84746360778809,21.960972199469655],[120.87132453918457,21.898709253832585],[120.84952354431152,21.8910638444382],[120.76746940612793,21.949349728374518],[120.81124305725098,21.962245837143676]]]
         * id : 2
         * name : 墾丁
         */

        private ZoneBean end_zone;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public int getBack() {
            return back;
        }

        public void setBack(int back) {
            this.back = back;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public LocationBean getStart_location() {
            return start_location;
        }

        public void setStart_location(LocationBean start_location) {
            this.start_location = start_location;
        }

        public LocationBean getEnd_location() {
            return end_location;
        }

        public void setEnd_location(LocationBean end_location) {
            this.end_location = end_location;
        }

        public ZoneBean getStart_zone() {
            return start_zone;
        }

        public void setStart_zone(ZoneBean start_zone) {
            this.start_zone = start_zone;
        }

        public ZoneBean getEnd_zone() {
            return end_zone;
        }

        public void setEnd_zone(ZoneBean end_zone) {
            this.end_zone = end_zone;
        }

        public static class LocationBean {
            private int id;
            private String name;
            private int zone_id;
            private String address;
            private List<Double> gps;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getZone_id() {
                return zone_id;
            }

            public void setZone_id(int zone_id) {
                this.zone_id = zone_id;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public List<Double> getGps() {
                return gps;
            }

            public void setGps(List<Double> gps) {
                this.gps = gps;
            }
        }

        public static class ZoneBean {
            private int id;
            private String name;
            private List<Double> focus;
            private List<List<List<Double>>> area;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<Double> getFocus() {
                return focus;
            }

            public void setFocus(List<Double> focus) {
                this.focus = focus;
            }

            public List<List<List<Double>>> getArea() {
                return area;
            }

            public void setArea(List<List<List<Double>>> area) {
                this.area = area;
            }
        }
    }
}
