package com.whilerain.utur.screen.driver.manager.model.driver.car;

import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/5/1.
 */
public class VehicleListResponse extends SimpleDataCache.CachePoJo {

    /**
     * status : true
     * result : [{"id":1,"model":"Wish","seat":4,"picture":null,"licenseplate":"1234-XD"},{"id":2,"model":"Tiida","seat":4,"picture":null,"licenseplate":"4321-DX"},{"id":3,"model":"BMW","seat":4,"picture":"car picture base64","licenseplate":"1234-DD"}]
     */

    private boolean status;
    /**
     * id : 1
     * model : Wish
     * seat : 4
     * picture : null
     * licenseplate : 1234-XD
     */

    private List<Car> result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Car> getResult() {
        return result;
    }

    public void setResult(List<Car> result) {
        this.result = result;
    }

    public static class Car {
        private int id;
        private String model;
        private int seat;
        private String licenseplate;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public int getSeat() {
            return seat;
        }

        public void setSeat(int seat) {
            this.seat = seat;
        }


        public String getLicenseplate() {
            return licenseplate;
        }

        public void setLicenseplate(String licenseplate) {
            this.licenseplate = licenseplate;
        }
    }
}
