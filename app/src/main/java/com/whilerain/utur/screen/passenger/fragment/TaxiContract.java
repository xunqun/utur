package com.whilerain.utur.screen.passenger.fragment;

import com.google.android.gms.location.places.Place;
import com.whilerain.utur.apis.taxi.PassengerTaxiApi;
import com.whilerain.utur.screen.passenger.model.TimeSlot;

/**
 * Created by shawn on 2017/4/19.
 */

public interface TaxiContract {
    interface Presenter {
        void beginStep2(Place departure);
        void beginStep3(Place destination);
        void beginStep4(TimeSlot timeSlot);
        void backToStep(int i);
        void sendRequest(int pipoCount, int total, String contact_phone);
    }

    interface View {
        void setPresenter(Presenter presenter);
        void showInitStaus();
        void showStep2In();
        void showStep3In();
        void showStep4In(PassengerTaxiApi.TaxiQueryPriceResponse response);
        void showStep2Out();
        void showStep3Out();
        void showStep4Out();
        void showErrorMessage(String msg);
        void setProgressingVisible(boolean show);
        void showCompleteSuccessfull();
    }
}
