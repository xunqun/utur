package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.model.ApiReference;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.bootstrap.CarDetailRequest;
import com.whilerain.utur.net.order.CancelOrderRequest;
import com.whilerain.utur.net.order.CancelRequestRequest;
import com.whilerain.utur.net.order.DriverHistoryOrderRequest;
import com.whilerain.utur.net.order.FinishOrderRequest;
import com.whilerain.utur.screen.passenger.manager.model.car.CarResponse;
import com.whilerain.utur.screen.passenger.manager.model.passenger.order.PassengerHistoryResponse;
import com.whilerain.utur.utils.CarPrefHandler;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EngagedPassengerInfoOrderActivity extends AppCompatActivity {

    private static Object object;

    @BindView(R.id.name)
    TextView vName;

    @BindView(R.id.phone)
    TextView vPhone;

    @BindView(R.id.time)
    TextView vTime;

    @BindView(R.id.departure_name)
    TextView vDepartureName;

    @BindView(R.id.departure_addr)
    TextView vDepartureAddr;

    @BindView(R.id.destination_name)
    TextView vDestinationName;

    @BindView(R.id.destination_addr)
    TextView vDestinationAddr;

    @BindView(R.id.car_model)
    TextView vCarModel;

    @BindView(R.id.car_num)
    TextView vCarNum;

    @BindView(R.id.photo)
    ImageView vPhoto;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.call)
    ImageView vCall;

    @BindView(R.id.seat)
    TextView vSeat;

    String mId;
    private String mState;
    private CarResponse carResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engaged_passenger_transport_info);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.finish) {
            AlertDialog.Builder builder = new AlertDialog.Builder(EngagedPassengerInfoOrderActivity.this);
            builder.setMessage(R.string.do_you_want_to_finish_order)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (object instanceof DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) {
                                finishOrder();
                            } else {
                                finishOrder();
                            }
                        }
                    }).setNegativeButton(android.R.string.cancel, null);
            builder.create().show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void finishOrder() {
        FinishOrderRequest.build(mId).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        showFinishDialog(R.string.finish_order_success);
                        finish();
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showErrorMessage(R.string.error_data_query_fail);
            }

            @Override
            public void onFail() {
                showErrorMessage(R.string.error_connection_fail);
            }
        }).put();
    }

    private void cancelRequest() {
        CancelRequestRequest.build(mId).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        showFinishDialog(R.string.cancel_success);
                        finish();
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showErrorMessage(R.string.error_data_query_fail);
            }

            @Override
            public void onFail() {
                showErrorMessage(R.string.error_connection_fail);
            }
        }).put();
    }

    private void showErrorMessage(final int res) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(EngagedPassengerInfoOrderActivity.this);
                builder.setMessage(res).setPositiveButton(android.R.string.ok, null).create().show();
            }
        });
    }

    private void showFinishDialog(final int res) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(EngagedPassengerInfoOrderActivity.this);
                builder.setMessage(res).setPositiveButton(android.R.string.ok, null).create().show();
            }
        });
    }

    private void initViews() {
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (object instanceof DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) {
            final DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order order = (DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) object;
            mId = order.getOrder_id();
            mState = order.getStatus();
            vSeat.setText(String.valueOf(order.getRequests().get(0).getPassenger_count()));
            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", order.getStart_time());
                vTime.setText(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (order.getCategory().equals(ApiReference.TransType.TRANSPORT.toString()) ) {

                DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location();
                startLocation = startLocation == null ? order.getRequests().get(0).getStart_location_custom() : startLocation;
                DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location();
                endLocation = endLocation == null ? order.getRequests().get(0).getEnd_location_custom() : endLocation;

                vDestinationName.setText(endLocation.getName());
                vDestinationAddr.setText(endLocation.getAddress());
                vDepartureName.setText(startLocation.getName());
                vDepartureAddr.setText(startLocation.getAddress());
            } else if (order.getCategory().equals(ApiReference.TransType.SHARE.toString())) {
                vDestinationName.setVisibility(View.GONE);
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(StringUtils.trimSpecialString(order.getRequests().get(0).getName()));
                vDepartureAddr.setVisibility(View.GONE);
            }else if(order.getCategory().equals(ApiReference.TransType.TAXI.toString())){
                DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location_custom();
                DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location_custom();

                vDestinationName.setText(endLocation.getAddress());
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(startLocation.getAddress());
                vDepartureAddr.setVisibility(View.GONE);
            } else { // package
                vDestinationName.setVisibility(View.GONE);
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(StringUtils.trimSpecialString(order.getRequests().get(0).getName()));
                vDepartureAddr.setVisibility(View.GONE);
            }

            if (order.getRequests().get(0) != null && order.getRequests().get(0).getPassenger() != null) {
                vName.setText(order.getRequests().get(0).getPassenger().getName());
                vPhone.setText(order.getRequests().get(0).getContact_phone());
                vCall.setVisibility(View.VISIBLE);
            } else {

            }

            if (order.getCar() != null) {
                String responseBody = CarPrefHandler.getSharedPrefences(this).getString(String.valueOf(order.getCar().getId()), "");
                if (responseBody.length() > 0) {
                    setupCar(order.getCar().getId(), responseBody);
                } else {

                    CarDetailRequest.build(order.getCar().getId()).setApiListener(new ApiRequest.ApiListener() {
                        @Override
                        public void onSeccess(ApiRequest apiRequest, String body) {
                            setupCar(order.getCar().getId(), body);

                        }

                        @Override
                        public void onFail() {
                            showToast(R.string.error_connection_fail);
                        }
                    }).get();
                }

            } else {
                vCarModel.setVisibility(View.GONE);
                vCarNum.setVisibility(View.GONE);
                vPhoto.setVisibility(View.GONE);
            }
        } else {
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request request = (DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request) object;
            mId = request.getRequest_id();
            mState = request.getStatus();
            if (request.getCategory().equals(ApiReference.TransType.TRANSPORT.toString())) {
                vDestinationName.setText(request.getStart_location().getName());
                vDestinationAddr.setText(request.getStart_location().getAddress());
                vDepartureName.setText(request.getEnd_location().getName());
                vDepartureAddr.setText(request.getEnd_location().getAddress());
            }else if(request.getCategory().equals(ApiReference.TransType.TAXI.toString())){
                DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request.LocationBean startLocation = request.getStart_location_custom();
                DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request.LocationBean endLocation = request.getEnd_location_custom();

                vDestinationName.setText(endLocation.getAddress());
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setText(startLocation.getAddress());
                vDepartureAddr.setVisibility(View.GONE);
            } else if (request.getCategory().equals(ApiReference.TransType.SHARE.toString())) {
                vDestinationName.setText(request.getName());
                vDestinationAddr.setVisibility(View.GONE);
                vDepartureName.setVisibility(View.GONE);
                vDepartureAddr.setVisibility(View.GONE);
            }
            vName.setText(R.string.requesting);

            vCarModel.setVisibility(View.GONE);
            vCarNum.setVisibility(View.GONE);
            vPhoto.setVisibility(View.GONE);
        }
    }

    private void setupCar(int id, String body) {
        Gson gson = new Gson();
        carResponse = gson.fromJson(body, CarResponse.class);
        if (carResponse.isStatus()) {
            CarPrefHandler.getEditor(this).putString(String.valueOf(id), body).apply();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        vCarModel.setText(carResponse.getResult().getModel());
                        vCarNum.setText(carResponse.getResult().getLicenseplate());
                        try {
                            byte[] decodedString = Base64.decode(carResponse.getResult().getPicture(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            vPhoto.setImageBitmap(decodedByte);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(R.string.error_data_query_fail);
                    }
                }
            });
        }
    }

    private void showToast(final int res) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(EngagedPassengerInfoOrderActivity.this, res, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void launch(Activity activity, Object r) {
        activity.startActivity(new Intent(activity, EngagedPassengerInfoOrderActivity.class));
        object = r;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_engaged_passenger_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.call)
    void onCall(){
        String num = vPhone.getText().toString();
        if(num.length() > 0)
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", num, null)));
    }

    @OnClick(R.id.departure_name)
    void onDepartureName(){
        if (object instanceof DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) {
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order order = (DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) object;
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location();
            startLocation = startLocation == null ? order.getRequests().get(0).getStart_location_custom() : startLocation;

            if(startLocation != null && startLocation.getGps() != null) {
                UiUtils.showLocation(this, String.valueOf(startLocation.getGps().get(0)), String.valueOf(startLocation.getGps().get(1)), startLocation.getAddress());
            }
        }else{
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request request = (DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request) object;
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request.LocationBean startLocation = request.getStart_location();
            startLocation = startLocation == null ? request.getStart_location_custom() : startLocation;

            if(startLocation != null && startLocation.getGps() != null) {
                UiUtils.showLocation(this, String.valueOf(startLocation.getGps().get(0)), String.valueOf(startLocation.getGps().get(1)), startLocation.getAddress());
            }
        }
    }

    @OnClick(R.id.destination_name)
    void onDestination(){
        if (object instanceof DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) {
            final DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order order = (DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order) object;

            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location();
            endLocation = endLocation == null ? order.getRequests().get(0).getEnd_location_custom() : endLocation;

            if(endLocation != null && endLocation.getGps() != null) {
                UiUtils.showLocation(this, String.valueOf(endLocation.getGps().get(0)), String.valueOf(endLocation.getGps().get(1)), endLocation.getAddress());
            }
        }else{


            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request request = (DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request) object;
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request.LocationBean endlocation = request.getEnd_location();
            endlocation = endlocation == null ? request.getStart_location_custom() : endlocation;

            if(endlocation != null && endlocation.getGps() != null) {
                UiUtils.showLocation(this, String.valueOf(endlocation.getGps().get(0)), String.valueOf(endlocation.getGps().get(1)), endlocation.getAddress());
            }
        }
    }
}
