package com.whilerain.utur.screen.passenger.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.bootstrap.FullDayTripRequest;
import com.whilerain.utur.screen.passenger.manager.model.transport.FullDayTripResponse;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/6/15.
 */
public class PackageManager {
    private static PackageManager instance;
    private List<FullDayTripResponse.ResultBean> results = new ArrayList<>();
    private FullDayTripRequestListener listener;

    public interface FullDayTripRequestListener{
        void onSuccess();
        void onFail();
    }

    private PackageManager(Context context){
        SimpleDataCache<FullDayTripResponse> cache = new SimpleDataCache<FullDayTripResponse>();
        FullDayTripResponse response = cache.get(context, FullDayTripResponse.class);
        if (response != null && response.getResult() != null) {
            results = cache.get(context, FullDayTripResponse.class).getResult();
        }
    }

    public static PackageManager getInstance(Context context){
        if(instance == null){
            instance = new PackageManager(context);
        }
        return instance;
    }

    public List<FullDayTripResponse.ResultBean> getResults() {
        return results;
    }

    public void setFullDayTripRequestListener(FullDayTripRequestListener l) {
        this.listener = l;
    }

    public void dispatchRequestFail() {
        if (listener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.onFail();
                }
            });
        }
    }

    public void dispatchRequestSuccess() {
        if (listener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.onSuccess();
                }
            });
        }
    }

    public void requestFromServer(final Context context) {
        FullDayTripRequest.build().setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        SimpleDataCache<FullDayTripResponse> cache = new SimpleDataCache<FullDayTripResponse>();
                        cache.set(context, FullDayTripResponse.class, body);
                        results = cache.get(context, FullDayTripResponse.class).getResult();
                        dispatchRequestSuccess();
                        return;
                    } else {

                        dispatchRequestFail();
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRequestFail();
            }

            @Override
            public void onFail() {
                dispatchRequestFail();
            }
        }).get();
    }

}
