package com.whilerain.utur.screen.driver.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.driver.DriverActivity;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.passenger.manager.TransportManager;
import com.whilerain.utur.screen.passenger.manager.model.transport.TransportResponse;
import com.whilerain.utur.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DriverTransportFragment extends Fragment {

    @BindView(R.id.root)
    FrameLayout root;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeView;

    private DataAdapter dataAdapter;

    TransportManager.TransportRoutesListener listener = new TransportManager.TransportRoutesListener() {
        @Override
        public void onSuccess() {
            if(swipeView.isRefreshing()) {
                swipeView.setRefreshing(false);
            }
            dataAdapter.refreshData();
        }

        @Override
        public void onFail() {
            if(swipeView.isRefreshing()) {
                swipeView.setRefreshing(false);
            }
            Toast.makeText(getContext(), R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DriverTransportFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransportManager.getInstance(getContext()).setTransportRoutesListener(listener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popularroutes_list, null);
        ButterKnife.bind(this, view);
        initViews();

        if(NetworkUtils.isOnline(getContext())) {
            TransportManager.getInstance(getContext()).requestFromServer(getContext());
            swipeView.setRefreshing(true);
        }else{
            Snackbar snackbar = Snackbar.make(root, R.string.error_no_network, Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.RED);
            snackbar.show();
        }
        return view;
    }

    private void initViews() {
        // Set the adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        dataAdapter = new DataAdapter();
        recyclerView.setAdapter(dataAdapter);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                TransportManager.getInstance(getContext()).requestFromServer(getContext());
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class DataAdapter extends RecyclerView.Adapter<ViewHolder> {

        List<TransportResponse.Result> list = new ArrayList<>();

        public DataAdapter() {
            list = TransportManager.getInstance(getContext()).getResults();
        }

        public void refreshData() {
            list = TransportManager.getInstance(getContext()).getResults();
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_station_shuttle_item, null);
            ViewHolder vh = new ViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setData(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.content)
        TextView textView;

        TransportResponse.Result data;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(TransportResponse.Result data) {
            this.data = data;
            textView.setText(data.getName());
        }

        @OnClick(R.id.root)
        public void onRoot(){
            if(DriverUserManager.getInstance(getContext()).isLogin()) {

//                DriverStationShuttleTripListActivity.launch(getActivity(), data.getId());

            }else{

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(R.string.require_loged_in)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((DriverActivity)getActivity()).switchToPageNum(3);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null);
                builder.create().show();
            }
        }
    }
}
