package com.whilerain.utur.screen.passenger.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.bootstrap.HalfDayTripRequest;
import com.whilerain.utur.screen.passenger.manager.model.transport.HalfDayTripResponse;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/6/15.
 */
public class ShareManager {
    private static ShareManager instance;
    private List<HalfDayTripResponse.ResultBean> results = new ArrayList<>();
    private HalfDayTripRequestListener listener;

    public interface HalfDayTripRequestListener {
        void onSuccess();
        void onFail();
    }

    private ShareManager(Context context){
        SimpleDataCache<HalfDayTripResponse> cache = new SimpleDataCache<HalfDayTripResponse>();
        HalfDayTripResponse response = cache.get(context, HalfDayTripResponse.class);
        if (response != null && response.getResult() != null) {
            results = cache.get(context, HalfDayTripResponse.class).getResult();
        }
    }

    public static ShareManager getInstance(Context context){
        if(instance == null){
            instance = new ShareManager(context);
        }
        return instance;
    }

    public List<HalfDayTripResponse.ResultBean> getResults() {
        return results;
    }

    public void setHalfDayTripRequestListener(HalfDayTripRequestListener l) {
        this.listener = l;
    }

    public void dispatchRequestFail() {
        if (listener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.onFail();
                }
            });
        }
    }

    public void dispatchRequestSuccess() {
        if (listener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.onSuccess();
                }
            });
        }
    }

    public void requestFromServer(final Context context) {
        HalfDayTripRequest.build().setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        SimpleDataCache<HalfDayTripResponse> cache = new SimpleDataCache<HalfDayTripResponse>();
                        cache.set(context, HalfDayTripResponse.class, body);
                        results = cache.get(context, HalfDayTripResponse.class).getResult();
                        dispatchRequestSuccess();
                        return;
                    } else {

                        dispatchRequestFail();
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRequestFail();
            }

            @Override
            public void onFail() {
                dispatchRequestFail();
            }
        }).get();
    }

}
