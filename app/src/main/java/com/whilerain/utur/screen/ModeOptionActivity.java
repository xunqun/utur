package com.whilerain.utur.screen;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.whilerain.utur.R;
import com.whilerain.utur.gcm.RegistrationIntentService;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.passenger.HomeActivity;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.utils.SharePrefHandler;
import com.whilerain.utur.utils.UiUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ModeOptionActivity extends AppCompatActivity {

    public static final int MODE_NONE = -1;
    public static final int MODE_PASSENGER = 0;
    public static final int MODE_DRIVER = 1;
    private int MY_PERMISSIONS_REQUEST = 123;

    @OnClick(R.id.tutorial)
    void onTutorial(){
        TutorialActivity.launch(this);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_option);
        ButterKnife.bind(this);

        if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, MODE_NONE) == MODE_PASSENGER){

            //register gcm token to server
            if(PassengerUserManager.getInstance(this).isLogin()){
                RegistrationIntentService.launch(this, PassengerUserManager.getInstance(this).getUserInfo().getUuid());
            }

            HomeActivity.launchClearTop(this);
        }else if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, MODE_NONE) == MODE_DRIVER){

            //register gcm token to server
            if(DriverUserManager.getInstance(this).isLogin()){
                RegistrationIntentService.launch(this, DriverUserManager.getInstance(this).getUserInfo().getUuid());
            }

            com.whilerain.utur.screen.driver.HomeActivity.launchClearTop(this);
        }

        requestPermission();
    }

    private void requestPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.permission_requirement)
                        .setMessage(R.string.permission_storage_description)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                UiUtils.startInstalledAppDetailsActivity(ModeOptionActivity.this);
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .create().show();


            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }



    public static void launchClearPref(Activity activity){
        Intent it = new Intent(activity, ModeOptionActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(it);
        SharePrefHandler.getEditor(activity).clear().apply();
    }

    public static void launch(Activity activity){
        Intent it = new Intent(activity, ModeOptionActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(it);
    }

    @OnClick(R.id.passenger)
    public void onPassenger(){
        SharePrefHandler.getEditor(this).putInt(SharePrefHandler.APP_MODE, MODE_PASSENGER).apply();
        HomeActivity.launchClearTop(this);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
    }

    @OnClick(R.id.driver)
    public void onDriver(){
        SharePrefHandler.getEditor(this).putInt(SharePrefHandler.APP_MODE, MODE_DRIVER).apply();
        com.whilerain.utur.screen.driver.HomeActivity.launchClearTop(this);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
    }
}
