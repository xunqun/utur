package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.ModeOptionActivity;
import com.whilerain.utur.screen.passenger.fragment.TaxiFragment;
import com.whilerain.utur.screen.passenger.fragment.ShareFragment;
import com.whilerain.utur.screen.passenger.fragment.MyProfileFragment;
import com.whilerain.utur.screen.passenger.fragment.PassengerLoginFragment;
import com.whilerain.utur.screen.passenger.fragment.TransportFragment;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PassengerActivity extends AppCompatActivity implements PassengerUserManager.PassengerUserListener {

    private static int openIndex = 0;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    private enum PAGE {
        p1(R.string.title_station_shuttle, R.drawable.ic_attraction, TransportFragment.class),
        p2(R.string.title_popular_route, R.drawable.ic_attraction, ShareFragment.class),
        p3(R.string.title_free_routes, R.drawable.ic_attraction, TaxiFragment.class),
        p4(R.string.title_my_profile, R.drawable.ic_attraction, MyProfileFragment.class);

        public int resName;
        public int resIcon;
        public Class<? extends Fragment> C;

        PAGE(int res_name, int res_icon, Class<? extends Fragment> C) {
            resName = res_name;
            res_icon = res_icon;
            this.C = C;
        }
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(PAGE.p1.resName);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                getSupportActionBar().setTitle(PAGE.values()[position].resName);
                mViewPager.setCurrentItem(position, true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabIcons();

        mViewPager.setCurrentItem(openIndex);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void switchToPageNum(int num){
        mViewPager.setCurrentItem(num, true);
    }

    @Override
    protected void onResume() {
        PassengerUserManager.getInstance(this).addPassengerUserListener(this);
        mSectionsPagerAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    protected void onPause() {
        PassengerUserManager.getInstance(this).removePassengerUserListener(this);
        super.onPause();
    }

    private void setupTabIcons() {
        for (int i = 0; i < PAGE.values().length; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setIcon(R.drawable.ic_beach_access_black_24dp);
        }
    }

    public static void launch(Activity activity) {
        Intent intent = new Intent(activity, PassengerActivity.class);
        activity.startActivity(intent);
    }

    public static void launchClearTask(Activity activity) {
        launchClearTask(activity, 0);
    }

    public static void launchClearTask(Activity activity, int index) {
        openIndex = index;
        Intent it = new Intent(activity, PassengerActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(it);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        initActionItem(menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        initActionItem(menu);
        return true;
    }

    private void initActionItem(Menu menu) {
        if (PassengerUserManager.getInstance(this).isLogin()) {
            getMenuInflater().inflate(R.menu.menu_passenger_activity, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_passenger_activity_notlogin, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            PassengerUserManager.getInstance(this).logout(this);
            ModeOptionActivity.launchClearPref(this);
            return true;
        } else if (id == R.id.switch_to_driver) {
            ModeOptionActivity.launchClearPref(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        HashMap<Integer, Fragment> fragmentHashMap = new HashMap<>();
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Class<? extends Fragment> C = PAGE.values()[position].C;
            try {
                if (position == 3 && !PassengerUserManager.getInstance(PassengerActivity.this).isLogin()) {
                    // If user not login, shows login UI
                    return new PassengerLoginFragment();
                } else {

                    if(fragmentHashMap.containsKey(position)){
                        return fragmentHashMap.get(position);
                    }else {
                        Fragment f = C.newInstance();
                        fragmentHashMap.put(position, f);
                        return f;
                    }
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return PAGE.values().length;
        }

        @Override
        public int getItemPosition(Object object) {
            if (object instanceof PassengerLoginFragment || object instanceof MyProfileFragment) {
                return POSITION_NONE;
            }
            return super.getItemPosition(object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(PAGE.values()[position].resName);
        }


    }


    @Override
    public void onFail() {

    }

    @Override
    public void onLoginStateChange() {
        mSectionsPagerAdapter.notifyDataSetChanged();
        invalidateOptionsMenu();
    }

    @Override
    public void onRegisterResult(boolean success, String msg) {

    }

    @Override
    public void onSmsCodeVerifyResult(boolean success) {

    }
}
