package com.whilerain.utur.screen.passenger.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;


import com.whilerain.utur.screen.passenger.manager.model.transport.TransportResponse.Result;
import com.whilerain.utur.screen.passenger.manager.model.transport.TransportResponse;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.bootstrap.TransportRequest;
import com.whilerain.utur.net.passenger_request.TransportOrderRequest;
import com.whilerain.utur.net.schedule.TransportScheduleRequest;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/3/29.
 */
public class TransportManager {
    private static TransportManager ourInstance;
    private TransportRoutesListener listener;
    private List<Result> results = new ArrayList<>();

    public interface TransportRoutesListener {
        void onSuccess();
        void onFail();
    }

    public static TransportManager getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new TransportManager(context);
        }
        return ourInstance;
    }

    private TransportManager(Context context) {
        SimpleDataCache<TransportResponse> cache = new SimpleDataCache<TransportResponse>();
        try {
            TransportResponse response = cache.get(context, TransportResponse.class);
            if (response != null && response.getResult() != null) {
                results = cache.get(context, TransportResponse.class).getResult();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Result> getResults() {
        return results;
    }

    public void setTransportRoutesListener(TransportRoutesListener l) {
        this.listener = l;
    }

    public void dispatchRequestFail() {
        if (listener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.onFail();
                }
            });
        }
    }

    public void dispatchRequestSuccess() {
        if (listener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    listener.onSuccess();
                }
            });
        }
    }
    public void requestOrder(String transId, int travId, String uuid, String date, int passengerCount, int from, int to, String start_location_custom, String end_location_custom, String contactNum, ApiRequest.ApiListener listener){
        TransportOrderRequest.build(transId, travId, uuid, date, passengerCount, from, to, start_location_custom, end_location_custom, contactNum).setApiListener(listener).post();
    }

    public void requestSchedule(int travelId, String date, ApiRequest.ApiListener listener){
        TransportScheduleRequest.build(travelId, date).setApiListener(listener).get();
    }

    public void requestFromServer(final Context context) {
        TransportRequest.build().setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        SimpleDataCache<TransportResponse> cache = new SimpleDataCache<TransportResponse>();
                        cache.set(context, TransportResponse.class, body);
                        results = cache.get(context, TransportResponse.class).getResult();
                        dispatchRequestSuccess();
                        return;
                    } else {

                        dispatchRequestFail();
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRequestFail();
            }

            @Override
            public void onFail() {
                dispatchRequestFail();
            }
        }).get();
    }

    public Result findRouteById(int id){
        if(results == null) return null;

        for (int i = 0; i < results.size(); i++) {
            if(results.get(i).getId() == id){
                return results.get(i);
            }
        }
        return null;
    }


}
