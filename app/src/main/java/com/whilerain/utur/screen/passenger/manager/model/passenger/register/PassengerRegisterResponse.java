
package com.whilerain.utur.screen.passenger.manager.model.passenger.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerRegisterResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * 
     * @return
     *     The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public static class Result {

        @SerializedName("code")
        @Expose
        private Integer code;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("uuid")
        @Expose
        private String uuid;

        /**
         *
         * @return
         *     The code
         */
        public Integer getCode() {
            return code;
        }

        /**
         *
         * @param code
         *     The code
         */
        public void setCode(Integer code) {
            this.code = code;
        }

        /**
         *
         * @return
         *     The message
         */
        public String getMessage() {
            return message;
        }

        /**
         *
         * @param message
         *     The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         *
         * @return
         *     The uuid
         */
        public String getUuid() {
            return uuid;
        }

        /**
         *
         * @param uuid
         *     The uuid
         */
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

    }
}
