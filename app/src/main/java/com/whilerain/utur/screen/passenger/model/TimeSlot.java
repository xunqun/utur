package com.whilerain.utur.screen.passenger.model;

/**
 * Created by shawn on 2017/4/24.
 */

public class TimeSlot {
    int year;
    int month;
    int day;
    int hour;
    int minut;

    public TimeSlot(int y, int m, int d, int h, int min) {
        this.year = y;
        this.month = m;
        this.day = d;
        this.hour = h;
        this.minut = min;
    }

    public String toString() {
        return year + "-" +String.format("%02d", month) + "-" + String.format("%02d",day) + " " + String.format("%02d", hour) + ":" + String.format("%02d", minut) + ":00";
    }
}