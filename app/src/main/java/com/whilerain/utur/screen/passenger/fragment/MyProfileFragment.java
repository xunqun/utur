package com.whilerain.utur.screen.passenger.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.passenger.HistoryActivity;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {

    @BindView(R.id.name)
    TextView vName;

    @BindView(R.id.account)
    TextView vAccount;

    @BindView(R.id.email)
    TextView vEmail;

    @BindView(R.id.history)
    Button vHistory;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_passenger_my_profile, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    private void initViews() {
        vEmail.setText(PassengerUserManager.getInstance(getContext()).getUserInfo().getEmail());
        vName.setText(String.format(getString(R.string.hello), PassengerUserManager.getInstance(getContext()).getUserInfo().getName()));
        vAccount.setText(PassengerUserManager.getInstance(getContext()).getAccount(getContext()));
    }


    @OnClick(R.id.history)
    protected void onHistory(){
        HistoryActivity.launch(getActivity());
    }
}
