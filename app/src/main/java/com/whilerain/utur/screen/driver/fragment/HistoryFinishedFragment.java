package com.whilerain.utur.screen.driver.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.model.ApiReference;
import com.whilerain.utur.net.order.DriverHistoryOrderRequest;
import com.whilerain.utur.screen.driver.manager.DriverHistoryManager;
import com.whilerain.utur.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xunqun on 16/4/24.
 */
public class HistoryFinishedFragment extends Fragment implements DriverHistoryManager.HistoryListener {

    @BindView(R.id.list)
    ListView vList;
    private FinishedAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_finished, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        DriverHistoryManager.getInstance(getContext()).addHistoryListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        DriverHistoryManager.getInstance(getContext()).addHistoryListener(this);
    }


    @Override
    public void onDataSetChange() {
        if (adapter != null) {
            vList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            },2000);
        }
    }

    @Override
    public void onFail() {

    }

    private void initViews() {
        adapter = new FinishedAdapter();
        vList.setAdapter(adapter);
    }

    class FinishedAdapter extends BaseAdapter {
        List<DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order> oList = new ArrayList<>();
        List<DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request> rList = new ArrayList<>();

        FinishedAdapter() {
            initData();
        }

        public void initData() {
            oList.clear();
            rList.clear();
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean result = DriverHistoryManager.getInstance(getContext()).getResult();
            if (result != null) {
                if (result.getOrders().getNowandthen() != null) {
                    for(DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order order : result.getOrders().getWearedone()){
                        if(order.getRequests().size() > 0){
                            oList.add(order);
                        }
                    }
                }

                if (result.getRequests().getNowandthen() != null) {
                    rList.addAll(result.getRequests().getWearedone());
                }
            }
        }

        @Override
        public void notifyDataSetChanged() {
            initData();
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return oList.size() + rList.size();
        }

        @Override
        public Object getItem(int position) {
            if (position < oList.size()) {
                return oList.get(position);
            } else {
                return rList.get(position - oList.size());
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            OrderViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_history_order, null);
                vh = new OrderViewHolder(convertView);
                convertView.setTag(vh);
            }

            vh = (OrderViewHolder) convertView.getTag();
            if (position < oList.size()) {
                vh.setOrder(oList.get(position));
            } else {
                vh.setRequest(rList.get(position - oList.size()));
            }

            return convertView;

        }
    }

    protected class OrderViewHolder {
        @BindView(R.id.time)
        TextView vTime;

        @BindView(R.id.seat)
        TextView vSeat;

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.state)
        TextView vState;

        private DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order order;
        private DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request request;


        public OrderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void setOrder(DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order order) {

            this.order = order;
            if(order.getRequests().size() < 1) {
                vTime.setText("");
                vSeat.setText("");
                vName.setText("");
                vState.setText("");
                return;
            }
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location();
            startLocation = startLocation == null ? order.getRequests().get(0).getStart_location_custom() : startLocation;
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location();
            endLocation = endLocation == null ? order.getRequests().get(0).getEnd_location_custom() : endLocation;

            vTime.setText(order.getStart_time());
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean requestsBean = order.getRequests().get(0);
            vSeat.setText(String.format(getString(R.string.n_seat), String.valueOf(requestsBean.getPassenger_count())));
            if(order.getCategory().equals(ApiReference.TransType.PACKAGE.toString())){
                String name = requestsBean.getName();
                vName.setText(StringUtils.trimSpecialString(name));
            }else if(order.getCategory().equals(ApiReference.TransType.TRANSPORT.toString())) {
                String name = startLocation.getName() + " ► " + endLocation.getName();
                vName.setText(name);
            }else if(order.getCategory().equals(ApiReference.TransType.SHARE.toString())){
                String name = StringUtils.trimSpecialString(requestsBean.getName());
                vName.setText(name);
            }else if(order.getCategory().equals(ApiReference.TransType.TAXI.toString())){
                vName.setText(R.string.title_free_routes);
            }

            vState.setText(StringUtils.getStatusString(getContext(), order.getStatus()));
            vState.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_green_circle, 0, 0, 0);
        }

        public void setRequest(DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request request) {

            this.request = request;

            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request.LocationBean startLocation = request.getStart_location();
            startLocation = startLocation == null ? request.getStart_location_custom() : startLocation;
            DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean.RequestsBean.Request.LocationBean endLocation = request.getEnd_location();
            endLocation = endLocation == null ? request.getEnd_location_custom() : endLocation;

            vTime.setText(request.getDate());
            vSeat.setText(String.format(getString(R.string.n_seat), String.valueOf(request.getPassenger_count())));
            if(request.getCategory().equals("transport")) {
                String name = startLocation.getName() + " ► " + endLocation.getName();
                vName.setText(name);
            }else if(request.getCategory().equals(ApiReference.TransType.TAXI)){
                vName.setText(R.string.title_free_routes);
            }else{
                String name = request.getName().replace("\u0096", "");
                vName.setText(name);
            }

            boolean accept = request.getStatus().equals("accepted");
            vState.setText(StringUtils.getStatusString(getContext(), request.getStatus()));
            vState.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_green_circle, 0, 0, 0);
        }
    }
}
