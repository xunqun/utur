package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.driver.manager.VehicleManager;
import com.whilerain.utur.screen.driver.manager.model.driver.car.VehicleListResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleListActivity extends AppCompatActivity implements VehicleManager.VehicleManagerListener {

    @BindView(R.id.vehicle_list)
    ListView vList;

    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwipe;

    protected VehicleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_list);
        ButterKnife.bind(this);

        initViews();

    }

    @Override
    protected void onResume() {
        super.onResume();
        VehicleManager.getInstance(this).addVehicleManagerListener(this);
        VehicleManager.getInstance(this).requestVehicleList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        VehicleManager.getInstance(this).removeVehicleManagerListener(this);
    }

    public static void launch(Activity activity) {
        activity.startActivity(new Intent(activity, VehicleListActivity.class));
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddVehicleActivity.launch(VehicleListActivity.this);
            }
        });

        adapter = new VehicleAdapter();
        vList.setAdapter(adapter);
        vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VehicleListResponse.Car car= adapter.getItem(position);
                ShowVehicleActivity.launch(VehicleListActivity.this, car);

            }
        });

        vSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                VehicleManager.getInstance(VehicleListActivity.this).requestVehicleList();
            }
        });
    }


    @Override
    public void onVehicleUpdateSuccess() {

    }

    @Override
    public void onVehicleUpdateFail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vSwipe.setRefreshing(false);
            }
        });
    }

    @Override
    public void onVehicleListChange() {
        // should run in main thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vSwipe.setRefreshing(false);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private class VehicleAdapter extends BaseAdapter {

        List<VehicleListResponse.Car> list = new ArrayList<>();

        public VehicleAdapter() {
            initList();
        }

        private void initList() {
            VehicleListResponse response = VehicleManager.getInstance(VehicleListActivity.this).getVehicleListResponse();
            if (response == null) {
                list = new ArrayList<>();
            } else {
                list = response.getResult();
            }
        }

        @Override
        public void notifyDataSetChanged() {
            initList();
            super.notifyDataSetChanged();

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public VehicleListResponse.Car getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            VehicleListResponse.Car car = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(VehicleListActivity.this).inflate(R.layout.layout_vehicle_item, null);
            }

            TextView vTitle = ButterKnife.findById(convertView, R.id.title);
            TextView vNum = ButterKnife.findById(convertView, R.id.num);
            TextView vSeat = ButterKnife.findById(convertView, R.id.seat);
            vTitle.setText(car.getModel());
            vNum.setText(car.getLicenseplate());
            vSeat.setText(String.format("%s人座", car.getSeat()));
            return convertView;
        }
    }
}
