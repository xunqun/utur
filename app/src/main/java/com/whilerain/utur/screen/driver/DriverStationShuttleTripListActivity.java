package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.driver.DriverTransportListRequest;
import com.whilerain.utur.screen.driver.manager.model.driver.transport.TransportRequestListResponse;
import com.whilerain.utur.utils.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class DriverStationShuttleTripListActivity extends AppCompatActivity {

    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwipe;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.size)
    TextView vText;

    @BindView(R.id.progressBar)
    ProgressBar vProgressBar;

    TransportRequestListResponse response;
    private DataAdapter adapter;
    private static int mId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_station_shuttle_trip_list);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter = new DataAdapter();
        vList.setAdapter(adapter);
        vSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestFromServer();
                vProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnItemClick(R.id.list)
    protected void onItemClick(int position){
        DriverTransportDetailActivity.launch(this, adapter.getItem(position));
    }


    public static void launch(Activity activity, int id) {
        mId = id;
        activity.startActivity(new Intent(activity, DriverStationShuttleTripListActivity.class));
    }


    @Override
    protected void onResume() {
        super.onResume();

        vProgressBar.setVisibility(View.VISIBLE);
        requestFromServer();
    }

    private void requestFromServer() {
        DriverTransportListRequest.build(mId).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                Gson gson = new Gson();
                response = gson.fromJson(body, TransportRequestListResponse.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vProgressBar.setVisibility(View.INVISIBLE);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFail() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).get();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class DataAdapter extends BaseAdapter {
        List<DriverApi.RequestListResponse.ResultBean> list;

        DataAdapter() {
            initData();
        }

        private void initData() {
            if(response != null) {
                list = response.getResult();
            }
            if (list == null) {
                list = new ArrayList<>();
            }
        }

        @Override
        public void notifyDataSetChanged() {
            initData();
            super.notifyDataSetChanged();

        }

        @Override
        public int getCount() {
            if (list.size() == 0) {
                vText.setText(R.string.no_request);
            } else {
                vText.setText(String.format(getString(R.string.some_requests), list.size()));
            }
            return list.size();
        }

        @Override
        public DriverApi.RequestListResponse.ResultBean getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if(convertView == null){
                convertView = LayoutInflater.from(DriverStationShuttleTripListActivity.this).inflate(R.layout.driver_request_item, null);
                vh = new ViewHolder(convertView);
                convertView.setTag(vh);
            }
            vh = (ViewHolder) convertView.getTag();
            vh.loadData(getItem(position));
            return convertView;
        }
    }

    protected class ViewHolder {
        @BindView(R.id.time)
        TextView vTime;

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.count)
        TextView vCount;

        DriverApi.RequestListResponse.ResultBean resultBean;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }

        public void loadData(DriverApi.RequestListResponse.ResultBean resultBean){
            this.resultBean = resultBean;
            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd", resultBean.getTimerange().get(0));
                vTime.setText(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String start = "";
            String end = "";
            if(resultBean.getStart_location() != null) {
                start = resultBean.getStart_location().getName();
            }else{
                start = resultBean.getStart_location_custom().getAddress();
            }

            if(resultBean.getEnd_location() != null) {
                end = resultBean.getEnd_location().getName();
            }else{
                end = resultBean.getEnd_location_custom().getAddress();
            }

            vName.setText(start + " ► " + end);
            vCount.setText(String.format(getString(R.string.people_count), resultBean.getPassenger_count()));
        }
    }
}
