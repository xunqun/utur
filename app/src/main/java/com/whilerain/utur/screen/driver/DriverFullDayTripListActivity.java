package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.driver.DriverPackageListRequest;
import com.whilerain.utur.net.travel.FullDayTripDetailRequest;
import com.whilerain.utur.screen.driver.manager.model.driver.transport.PackageRequestListResponse;
import com.whilerain.utur.screen.passenger.manager.model.travel.FullDayTripDetailResponse;
import com.whilerain.utur.utils.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class DriverFullDayTripListActivity extends AppCompatActivity {
    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwipe;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.size)
    TextView vText;

    @BindView(R.id.progressBar)
    ProgressBar vProgressBar;

    PackageRequestListResponse response;
    private static int sId = 0;
    private static String sName = "";
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_station_shuttle_trip_list);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter = new DataAdapter();
        vList.setAdapter(adapter);
        vSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestFromServer();
                vProgressBar.setVisibility(View.VISIBLE);
            }
        });

    }

    private void requestFromServer() {
        DriverPackageListRequest.build(sId).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                Gson gson = new Gson();
                response = gson.fromJson(body, PackageRequestListResponse.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vProgressBar.setVisibility(View.INVISIBLE);
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFail() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).get();
    }


    public static void launch(Activity activity, int id, String name) {
        sId = id;
        sName = name;
        activity.startActivity(new Intent(activity, DriverFullDayTripListActivity.class));
    }

    @OnItemClick(R.id.list)
    protected void onItemClick(int position) {
        requestFromServer(adapter.getItem(position));
    }

    private void requestFromServer(final DriverApi.RequestListResponse.ResultBean bean) {
        FullDayTripDetailRequest.build(bean.getTravel()).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    Gson gson = new Gson();
                    final FullDayTripDetailResponse response = gson.fromJson(body, FullDayTripDetailResponse.class);
                    if (response.isStatus()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                DriverPackageDetailActivity.launch(DriverFullDayTripListActivity.this, response, bean);
                            }
                        });
                    } else {
                        showErrorMessage(getString(R.string.error_data_query_fail));
                    }
                    return;
                } catch (Exception e) {

                }
                showErrorMessage(getString(R.string.error_data_query_fail));
            }

            @Override
            public void onFail() {
                showErrorMessage(getString(R.string.error_connection_fail));
            }
        }).get();
    }

    private void showErrorMessage(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DriverFullDayTripListActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestFromServer();
        vProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class DataAdapter extends BaseAdapter {
        List<DriverApi.RequestListResponse.ResultBean> list;

        DataAdapter() {
            initData();
        }

        private void initData() {
            if (response != null) {
                list = response.getResult();
            }
            if (list == null) {
                list = new ArrayList<>();
            }
        }

        @Override
        public void notifyDataSetChanged() {
            initData();
            super.notifyDataSetChanged();

        }

        @Override
        public int getCount() {
            if (list.size() == 0) {
                vText.setText(R.string.no_request);
            } else {
                vText.setText(String.format(getString(R.string.some_requests), list.size()));
            }
            return list.size();
        }

        @Override
        public DriverApi.RequestListResponse.ResultBean getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(DriverFullDayTripListActivity.this).inflate(R.layout.driver_request_item, null);
                vh = new ViewHolder(convertView);
                convertView.setTag(vh);
            }
            vh = (ViewHolder) convertView.getTag();
            vh.loadData(getItem(position));
            return convertView;
        }
    }

    protected class ViewHolder {
        @BindView(R.id.time)
        TextView vTime;

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.count)
        TextView vCount;

        DriverApi.RequestListResponse.ResultBean resultBean;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void loadData(DriverApi.RequestListResponse.ResultBean resultBean) {
            this.resultBean = resultBean;
            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd", resultBean.getDate());
                vTime.setText(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            vName.setText(StringUtils.trimSpecialString(sName));
            vCount.setText(String.format(getString(R.string.people_count), resultBean.getPassenger_count()));
        }
    }
}
