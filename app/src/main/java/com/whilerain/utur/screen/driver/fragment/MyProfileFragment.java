package com.whilerain.utur.screen.driver.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.whilerain.utur.MyApplication;
import com.whilerain.utur.R;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.driver.DriverStatusRequest;
import com.whilerain.utur.net.driver.LicensePostRequest;
import com.whilerain.utur.screen.driver.HistoryActivity;
import com.whilerain.utur.screen.driver.VehicleListActivity;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {

    private static final int REQUEST_TAKE_PICTURE = 133;
    private static final int REQUEST_TAKE_ABLUM = 134;
    private static final int REQUEST_PERMISSION_CAMERA = 31;
    @BindView(R.id.name)
    TextView vName;

    @BindView(R.id.account)
    TextView vAccount;

    @BindView(R.id.email)
    TextView vEmail;

    @BindView(R.id.history)
    Button vHistory;

    @BindView(R.id.status)
    TextView vStatus;

    @BindView(R.id.license)
    ImageView vLicense;

    @BindView(R.id.upload_license)
    Button vUploadBtn;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_driver_my_profile, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Bitmap bitmap = null;
            Bitmap thumbnail = null;
            if (requestCode == REQUEST_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
            }

            if (requestCode == REQUEST_TAKE_ABLUM && resultCode == Activity.RESULT_OK) {
                InputStream inputStream = getContext().getContentResolver().openInputStream(data.getData());
                bitmap = BitmapFactory.decodeStream(inputStream);
            }

            if (bitmap == null) return;

            vLicense.setImageBitmap(bitmap);
            //scale bitmap
            if (bitmap.getWidth() > bitmap.getHeight()) {
                thumbnail = bitmap.createScaledBitmap(bitmap, 800, (int) (800f / bitmap.getWidth() * bitmap.getHeight()), false);
            } else {
                thumbnail = bitmap.createScaledBitmap(bitmap, 600, (int) (600f / bitmap.getHeight() * bitmap.getWidth()), false);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            uploadLicensePictureToServer(Base64.encodeToString(b, Base64.DEFAULT));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        requestStatusFromServer();
    }

    private void uploadLicensePictureToServer(String file) {
        LicensePostRequest.build(DriverUserManager.getInstance(getContext()).getUserInfo().getUuid(), file)
                .setApiListener(new ApiRequest.ApiListener() {
                    @Override
                    public void onSeccess(ApiRequest apiRequest, String body) {
                        try {
                            JSONObject jsonObject = new JSONObject(body);
                            if (jsonObject.getBoolean("status")) {
                                requestStatusFromServer();
                                showToast(R.string.upload_success);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showToast(R.string.upload_fail);
                    }

                    @Override
                    public void onFail() {
                        showToast(R.string.upload_fail);
                    }
                }).put();
    }

    private void showToast(final int res) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyApplication.getInstance(), res, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File saveBitmap(Bitmap bitmap) throws IOException {
        File file = new File(getContext().getCacheDir(), "temp.png");

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        return file;

    }

    private void initViews() {

        String mail = DriverUserManager.getInstance(getContext()).getUserInfo().getEmail();

        vEmail.setText(mail == null ? getString(R.string.not_provide) : DriverUserManager.getInstance(getContext()).getUserInfo().getEmail());

        vName.setText(String.format(getString(R.string.hello), DriverUserManager.getInstance(getContext()).getUserInfo().getName()));
        vAccount.setText(DriverUserManager.getInstance(getContext()).getAccount(getContext()));
        boolean verified = DriverUserManager.getInstance(getContext()).getUserInfo().getStatus().equals("verified");
        updateState(DriverUserManager.getInstance(getContext()).getUserInfo().getStatus());
        registerForContextMenu(vUploadBtn);
        // TODO: 16/5/1   Picasso.with(getContext()).load(DriverUserManager.getInstance(getContext()).getUserInfo().)
    }

    @OnClick(R.id.add_vehicle)
    protected void onAddVehicle(View v) {
        VehicleListActivity.launch(getActivity());
    }

    @OnClick(R.id.upload_license)
    protected void onUploadLicense(View v) {
        getActivity().openContextMenu(vUploadBtn);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_photo_option, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.camera:

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA);

                } else {

                    takePictureIntent();
                }
                return true;
            case R.id.album:
                Intent pickIntent = new Intent();
                pickIntent.setType("image/*");
                pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(pickIntent, REQUEST_TAKE_ABLUM);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takePictureIntent();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void takePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE);
        }
    }

    private void requestStatusFromServer() {
        DriverStatusRequest.build(DriverUserManager.getInstance(getContext()).getUserInfo().getUuid()).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, final String body) {
                if (getActivity() == null || getActivity().isFinishing()) return;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(body);
                            if (jsonObject.getBoolean("status")) {
                                String status = jsonObject.getJSONObject("result").getString("status");
                                updateState(status);
                                DriverUserManager.getInstance(getContext()).getUserInfo().setStatus(status);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


            }

            @Override
            public void onFail() {

            }
        }).get();
    }

    private void updateState(String status) {
        if (status.equals("idle")) {
            vStatus.setText(R.string.state_idle);
        } else if (status.equals("working")) {
            vStatus.setText(R.string.state_working);
        } else if (status.equals("offline")) {
            vStatus.setText(R.string.state_offline);
        } else if (status.equals("waitforupload")) {
            vStatus.setText(R.string.state_waitforupload);
        } else if (status.equals("verifying")) {
            vStatus.setText(R.string.state_verifying);
        } else if (status.equals("verified")) {
            vStatus.setText(R.string.state_verified);
        } else if (status.equals("fail")) {
            vStatus.setText(R.string.state_fail);
        }
    }

    @OnClick(R.id.history)
    void onHistory() {
        HistoryActivity.launch(getActivity());
    }
}
