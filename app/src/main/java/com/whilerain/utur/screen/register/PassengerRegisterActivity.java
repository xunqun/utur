package com.whilerain.utur.screen.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.utils.NetworkUtils;
import com.whilerain.utur.utils.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PassengerRegisterActivity extends AppCompatActivity {
    public static final int REQUEST_PASSENGER_REGISTER = 101;


    @BindView(R.id.contact_num)
    EditText vContactNum;

    @BindView(R.id.country_code)
    EditText vCountryCode;

    @BindView(R.id.pw)
    EditText vPassword;

    @BindView(R.id.name)
    EditText vName;

    @BindView(R.id.email)
    EditText vEmail;

    private ProgressDialog progressDialog;

    PassengerUserManager.PassengerUserListener listener = new PassengerUserManager.PassengerUserListener() {


        @Override
        public void onFail() {
            dismissDialog();
        }

        @Override
        public void onLoginStateChange() {
            dismissDialog();
        }

        @Override
        public void onRegisterResult(boolean success, String msg) {
            dismissDialog();
            if(PassengerRegisterActivity.this == null)return;

            if(success){
                PassengerSmsCodeActivity.launch(PassengerRegisterActivity.this);
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(PassengerRegisterActivity.this);
                msg = msg.length() > 0 ? msg : getString(R.string.error_register_fail);
                builder.setMessage(msg)
                        .setPositiveButton(R.string.ok, null)
                        .create().show();
            }
        }

        @Override
        public void onSmsCodeVerifyResult(boolean success) {
            dismissDialog();
        }
    };
    private void dismissDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger_register);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PassengerSmsCodeActivity.REQUEST_CODE_VERIFY && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onResume() {
        super.onResume();
        PassengerUserManager.getInstance(this).addPassengerUserListener(listener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PassengerUserManager.getInstance(this).removePassengerUserListener(listener);
        dismissDialog();
    }

    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    sendRegister();
                }
                return false;
            }
        });
    }

    public static void launch(Activity activity){
        activity.startActivityForResult(new Intent(activity, PassengerRegisterActivity.class), REQUEST_PASSENGER_REGISTER);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
            case R.id.action_send:
                sendRegister();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.send)
    protected void sendRegister() {
        if(dataValidation()){
            String countryCode = vCountryCode.getText().toString();
            String contactNum = vContactNum.getText().toString();
            if(contactNum.charAt(0) == '0'){
                contactNum = contactNum.substring(1);
            }
            String account = countryCode + contactNum;
            PassengerUserManager.getInstance(this).requestRegister(this,
                    account,
                    vEmail.getText().toString(),
                    vName.getText().toString(),
                    vPassword.getText().toString());
            progressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.connecting));
            UiUtils.hideImeKeyboard(this);
        }
    }

    private boolean dataValidation() {
        boolean ok = true;

        vContactNum.setError(null);
        vCountryCode.setError(null);
        vEmail.setError(null);
        vName.setError(null);
        vPassword.setError(null);

        // check empty
        if (vContactNum.getText().toString().length() == 0) {
            ok = false;
            vContactNum.setError(getString(R.string.error_can_not_empty));
        }

        if (vCountryCode.getText().toString().length() == 0) {
            ok = false;
            vCountryCode.setError(getString(R.string.error_can_not_empty));
        }

        if(vEmail.getText().toString().length() == 0){
            ok = false;
            vEmail.setError(getString(R.string.error_can_not_empty));
        }

        if(vName.getText().toString().length() == 0){
            ok = false;
            vName.setError(getString(R.string.error_can_not_empty));
        }

        if(vPassword.getText().toString().length() == 0){
            ok = false;
            vPassword.setError(getString(R.string.error_can_not_empty));
        }

        // check format
        if(vPassword.getText().toString().length() < 6){
            ok = false;
            vPassword.setError(getString(R.string.error_data_invalid));
        }

        if(!NetworkUtils.checkEmail(vEmail.getText().toString())){
            ok = false;
            vEmail.setError(getString(R.string.error_data_invalid));
        }

        if (!TextUtils.isDigitsOnly(vContactNum.getText().toString()) || vContactNum.getText().toString().length() < 9 ||  vContactNum.getText().toString().length() > 10 ) {
            ok = false;
            vContactNum.setError(getString(R.string.error_data_invalid));
        }

        if (!TextUtils.isDigitsOnly(vCountryCode.getText().toString()) || vCountryCode.getText().toString().length() != 3) {
            ok = false;
            vCountryCode.setError(getString(R.string.error_data_invalid));
        }

        return ok;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_passenger_register, menu);
        return true;
    }
}
