package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.whilerain.utur.R;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.driver.DriverPackageCheckRequest;
import com.whilerain.utur.net.driver.DriverTaxiCheckRequest;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.driver.manager.VehicleManager;
import com.whilerain.utur.screen.driver.manager.model.driver.car.VehicleListResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class TaxiComfirmAcceptOrderActivity extends AppCompatActivity implements VehicleManager.VehicleManagerListener {

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.list)
    ListView vList;

    private static String sReqId;
    CarAdapter carAdapter;
    int chooseCar = -1;


    public static void launch(Activity activity, String req_id) {
        sReqId = req_id;
        activity.startActivity(new Intent(activity, TaxiComfirmAcceptOrderActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comfirm_accept_order);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VehicleManager.getInstance(this).addVehicleManagerListener(this);
        VehicleManager.getInstance(this).requestVehicleList();
    }

    private void initViews() {
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        carAdapter = new CarAdapter();
        vList.setAdapter(carAdapter);

    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position) {
        chooseCar = position;
        carAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.send)
    public void send() {
        if(chooseCar < 0){
            Toast.makeText(TaxiComfirmAcceptOrderActivity.this, R.string.error_no_car_choosed, Toast.LENGTH_SHORT).show();
            return;
        }

        DriverTaxiCheckRequest.build(DriverUserManager.getInstance(this).getUserInfo().getUuid(),
                sReqId, carAdapter.getItem(chooseCar).getId()).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject json = new JSONObject(body);
                    if(json.getBoolean("status")){
                        showToast(R.string.accept_success);
                        DriverActivity.launch(TaxiComfirmAcceptOrderActivity.this, 0);
                    }else{
                        showToast(json.getJSONObject("result").getString("message"));
                    }
                    return;
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                showToast(R.string.error_data_query_fail);
            }

            @Override
            public void onFail() {
                showToast(R.string.error_connection_fail);
            }
        }).post();
    }

    public void showToast(final int res){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TaxiComfirmAcceptOrderActivity.this, res, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showToast(final String msg){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TaxiComfirmAcceptOrderActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onVehicleUpdateSuccess() {

    }

    @Override
    public void onVehicleUpdateFail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TaxiComfirmAcceptOrderActivity.this, R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVehicleListChange() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                carAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class CarAdapter extends BaseAdapter {
        List<VehicleListResponse.Car> list;

        public CarAdapter() {
            init();
        }

        private void init() {
            VehicleListResponse response = VehicleManager.getInstance(TaxiComfirmAcceptOrderActivity.this).getVehicleListResponse();
            if (response == null || response.getResult() == null) {
                list = new ArrayList<>();
            } else {
                list = response.getResult();
            }
        }

        @Override
        public void notifyDataSetChanged() {
            init();
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public VehicleListResponse.Car getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(TaxiComfirmAcceptOrderActivity.this).inflate(R.layout.layout_choose_car_item, null);
                vh = new ViewHolder(convertView);
                convertView.setTag(vh);
            }
            vh = (ViewHolder) convertView.getTag();
            vh.loadCar(getItem(position));
            if (position == chooseCar) {
                vh.setCheck(true);
            } else {
                vh.setCheck(false);
            }
            return convertView;
        }
    }

    class ViewHolder {
        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.content)
        TextView vContent;

        @BindView(R.id.check)
        ImageView vCheck;

        private VehicleListResponse.Car car;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void loadCar(VehicleListResponse.Car car) {
            this.car = car;
            vName.setText(car.getModel());
            String content = getString(R.string.car_num) + " : " + car.getLicenseplate() + " | " + getString(R.string.available_seat) + car.getSeat();
            vContent.setText(content);
        }

        public void setCheck(boolean check) {
            if (check) {
                vCheck.setVisibility(View.VISIBLE);
            } else {
                vCheck.setVisibility(View.INVISIBLE);
            }
        }
    }
}
