package com.whilerain.utur.screen.passenger.fragment;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.taxi.PassengerTaxiApi;
import com.whilerain.utur.screen.passenger.HistoryActivity;
import com.whilerain.utur.screen.passenger.PassengerActivity;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.screen.passenger.model.TimeSlot;
import com.whilerain.utur.utils.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by shawn on 2017/4/19.
 */

public class TaxiFragment extends Fragment implements TaxiContract.View {
    private static final String TAG_DEPARTURE_MAP = "TAG_DEPARTURE_MAP";
    private static final String TAG_DESTINATION_MAP = "TAG_DESTINATION_MAP";
    private static final int PLACE_DESTINATION_REQUEST = 2;
    private static final int PLACE_DEPARTURE_REQUEST = 1;

    final int MAP_ZOOMING = 15;

    @BindView(R.id.root_step1)
    ViewGroup vRootStep1;

    @BindView(R.id.departure_location)
    TextView vDepartureLocation;

    @BindView(R.id.root_step2)
    ViewGroup vRootStep2;

    @BindView(R.id.destination_location)
    TextView vDestinationLocation;

    @BindView(R.id.root_step3)
    ViewGroup vRootStep3;

    @BindView(R.id.time_slot_list)
    ListView vTimeSlotList;

    @BindView(R.id.order_time)
    TextView vOrderTime;

    @BindView(R.id.root_step4)
    ViewGroup vRootStep4;

    @BindView(R.id.departure_map)
    ViewGroup vDepartureMapFrame;

    @BindView(R.id.final_departure_location)
    TextView vFinalDepartureLocation;

    @BindView(R.id.final_destination_location)
    TextView vFinalDestinationLocation;

    @BindView(R.id.final_destination_time)
    TextView vFinalOrderTime;

    @BindView(R.id.final_order_count)
    TextView vFinalOrderCount;

    @BindView(R.id.final_price)
    TextView vPrice;

    @BindView(R.id.country_code)
    TextView vCountryCode;

    @BindView(R.id.contact_num)
    TextView vContactNum;

    private Marker destinationMarker;
    private Marker departureMarker;


    @OnClick(R.id.counter_add)
    void onCounterAdd() {
        int count = Integer.valueOf(vFinalOrderCount.getText().toString());
        count++;
        vFinalOrderCount.setText(String.valueOf(count));
        vPrice.setText(String.valueOf(count * taxiPriceResponse.getResult().getCost_per_ppl()));
    }

    @OnClick(R.id.counter_minus)
    void onCounterMinus() {
        int count = Integer.valueOf(vFinalOrderCount.getText().toString());
        if (count > 1) {
            count--;
            vFinalOrderCount.setText(String.valueOf(count));
        }

        vPrice.setText(String.valueOf(count * taxiPriceResponse.getResult().getCost_per_ppl()));
    }

    @OnClick( R.id.edit_departure_wraper)
    void onEditDeparture() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_DEPARTURE_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick( R.id.edit_destination_wraper)
    void onEditDestination() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_DESTINATION_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.step1_next)
    void onStep1Next() {
        if (!PassengerUserManager.getInstance(getContext()).isLogin()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(R.string.require_loged_in)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((PassengerActivity) getActivity()).switchToPageNum(3);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null);
            builder.create().show();
        }

        if (departurePlace == null) {
            Toast.makeText(getContext(), R.string.location_not_set, Toast.LENGTH_SHORT).show();
        } else {
            String name = (String) departurePlace.getName();

            vFinalDepartureLocation.setText(name.contains(".")?departurePlace.getAddress():name);
            presenter.beginStep2(departurePlace);
        }
    }

    @OnClick(R.id.step2_next)
    void onStep2Next() {
        if (destinationPlace == null) {
            Toast.makeText(getContext(), R.string.location_not_set, Toast.LENGTH_SHORT).show();
        } else {
            String name = (String) destinationPlace.getName();
            vFinalDestinationLocation.setText(name.contains(".")?destinationPlace.getAddress():name);
            presenter.beginStep3(destinationPlace);
        }
    }

    @OnClick(R.id.step3_next)
    void onStep3Next() {
        if (selectedTimeSlot == null) {
            Toast.makeText(getContext(), R.string.time_not_set, Toast.LENGTH_SHORT).show();
        } else {
            vFinalOrderTime.setText(selectedTimeSlot.toString().substring(0, 16));
            presenter.beginStep4(selectedTimeSlot);
        }
    }

    @OnClick(R.id.back_to_step1)
    void onBackToStep1() {
        presenter.backToStep(1);
    }

    @OnClick(R.id.back_to_step2)
    void onBackToStep2() {
        presenter.backToStep(2);
    }

    @OnClick(R.id.back_to_step3)
    void onBackToStep3() {
        presenter.backToStep(3);
    }

    @OnItemClick(R.id.time_slot_list)
    void onItemSlot(View view, int position) {
        selectedTimeSlot = adapter.getItem(position);
        vOrderTime.setText(selectedTimeSlot.toString());
        vOrderTime.setTextColor(Color.BLACK);
    }

    @OnClick(R.id.send)
    void onSend() {
        if (validate()) {
            int cost = Integer.valueOf(vPrice.getText().toString());
            String contact = StringUtils.getAccountString(vCountryCode.getText().toString(), vContactNum.getText().toString());
            int pipoCount = Integer.valueOf(vFinalOrderCount.getText().toString());
            presenter.sendRequest(pipoCount, cost, contact);
        }
    }

    private boolean validate() {
        vCountryCode.setError(null);
        vContactNum.setError(null);
        if (TextUtils.isEmpty(vCountryCode.getText().toString())) {
            vCountryCode.setError(getString(R.string.error_can_not_empty));
            return false;
        }

        if (TextUtils.isEmpty(vContactNum.getText().toString())) {
            vContactNum.setError(getString(R.string.error_can_not_empty));
            return false;
        }
        return true;
    }

    private PassengerTaxiApi.TaxiQueryPriceResponse taxiPriceResponse;

    private GoogleMap map;

    private Place departurePlace;

    private Place destinationPlace;

    private TaxiContract.Presenter presenter;

    private TimeSlotAdapter adapter;

    private TimeSlot selectedTimeSlot;

    private ProgressDialog progressDialog;

    private SupportMapFragment mapFragment;

    private Location lastLocation;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_DEPARTURE_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(getContext(), data);

            if(isFeasiblePlace(place)) {
                handleDeparturePlace(place);
                departurePlace = place;
                CameraUpdate u = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), MAP_ZOOMING);
                map.animateCamera(u);
            }
        }

        if (requestCode == PLACE_DESTINATION_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(getContext(), data);

            if(isFeasiblePlace(place)) {
                handleDestinationPlace(place);
                destinationPlace = place;
                CameraUpdate u = CameraUpdateFactory.newLatLngZoom(place.getLatLng(), MAP_ZOOMING);
                map.animateCamera(u);
            }
        }
    }

    private boolean isFeasiblePlace(Place place) {
        if(place.getLatLng().latitude < 22.061105){
            return true;
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.error_no_service_area)
                    .setMessage(R.string.error_no_service_area_desc)
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show();
            return false;
        }

    }

    private void handleDestinationPlace(Place place) {
        vDestinationLocation.setTextColor(Color.BLACK);
        String text = String.format("%s\n%s", place.getName(), place.getAddress());
        vDestinationLocation.setText(text);

        if (destinationMarker != null) {
            destinationMarker.remove();
            destinationMarker = null;
        }
        destinationMarker = map.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), MAP_ZOOMING));
    }

    private void handleDeparturePlace(Place place) {
        vDepartureLocation.setTextColor(Color.BLACK);
        String text = String.format("%s\n%s", place.getName(), place.getAddress());
        vDepartureLocation.setText(text);

        if (departureMarker != null) {
            departureMarker.remove();
            departureMarker = null;
        }
        departureMarker = map.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), MAP_ZOOMING));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new TaxiPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_anytimetrip, null);
        ButterKnife.bind(this, view);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        showInitStaus();

        initDepartureMap();


        //setup time slot
        List<TimeSlot> slots = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        if (minute + 20 < 30) {
            calendar.set(Calendar.MINUTE, 30);

        } else {
            calendar.set(Calendar.MINUTE, 0);
            calendar.add(Calendar.HOUR, 1);
        }
        for (int i = 1; i < 4; i++) {
            slots.add(new TimeSlot(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
            calendar.add(Calendar.MINUTE, 30);
        }
        adapter = new TimeSlotAdapter(slots);
        vTimeSlotList.setAdapter(adapter);
    }

    void initDepartureMap() {
        // Add departure map
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentByTag(TAG_DEPARTURE_MAP);
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.departure_map, mapFragment, TAG_DEPARTURE_MAP);
            ft.commit();
            fm.executePendingTransactions();
        }

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                map.setMyLocationEnabled(true);
                map.getUiSettings().setAllGesturesEnabled(false);
                map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        lastLocation = location;
                        if (departurePlace == null) {
                            CameraUpdate u = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), MAP_ZOOMING);
                            map.moveCamera(u);
                        }
                    }
                });
            }
        });

        vDepartureLocation.setTextColor(Color.RED);
    }


    @Override
    public void setPresenter(TaxiContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showInitStaus() {
        vRootStep1.setVisibility(View.VISIBLE);
        vRootStep2.setVisibility(View.GONE);
        vRootStep3.setVisibility(View.GONE);
        vRootStep4.setVisibility(View.GONE);
        departurePlace = null;
        destinationPlace = null;
        selectedTimeSlot = null;
        if(map != null) {
            map.clear();
        }
        if (lastLocation != null) {
            CameraUpdate u = CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), MAP_ZOOMING);
            map.animateCamera(u);
        }
    }

    @Override
    public void showStep2In() {
        vRootStep2.setVisibility(View.VISIBLE);
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
        vRootStep2.startAnimation(a);

        if (destinationPlace != null) {
            CameraUpdate u = CameraUpdateFactory.newLatLngZoom(destinationPlace.getLatLng(), MAP_ZOOMING);
            map.animateCamera(u);
        } else if (lastLocation != null) {
            CameraUpdate u = CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), MAP_ZOOMING);
            map.animateCamera(u);
        }

        if (destinationPlace == null) {
            vDestinationLocation.setText(R.string.location_not_set);
        }
    }

    @Override
    public void showStep3In() {
        vRootStep3.setVisibility(View.VISIBLE);
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
        vRootStep3.startAnimation(a);

        if (selectedTimeSlot == null) {
            vOrderTime.setText(R.string.time_not_set);
        }
    }

    @Override
    public void showStep4In(PassengerTaxiApi.TaxiQueryPriceResponse r) {
        taxiPriceResponse = r;
        vRootStep4.setVisibility(View.VISIBLE);
        String[] phone = StringUtils.getCountryCodeAndPhoneViaAccount(PassengerUserManager.getInstance(getContext()).getAccount(getContext()));
        vContactNum.setText(phone[1]);
        vCountryCode.setText(phone[0]);
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
        vRootStep4.startAnimation(a);
        int peopleCount = Integer.valueOf(vFinalOrderCount.getText().toString());
        vPrice.setText(String.valueOf(peopleCount * r.getResult().getCost_per_ppl()));
    }

    @Override
    public void showStep2Out() {
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_right);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                vRootStep2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        vRootStep2.startAnimation(a);
        if (departurePlace != null) {
            CameraUpdate u = CameraUpdateFactory.newLatLngZoom(departurePlace.getLatLng(), MAP_ZOOMING);
            map.animateCamera(u);
        } else {
            vDepartureLocation.setText(R.string.location_not_set);
        }
    }

    @Override
    public void showStep3Out() {
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_right);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                vRootStep3.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        vRootStep3.startAnimation(a);
        if (destinationPlace == null) {
            vDestinationLocation.setText(R.string.location_not_set);
        }
    }

    @Override
    public void showStep4Out() {
        Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_right);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                vRootStep4.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        vRootStep4.startAnimation(a);

        if (selectedTimeSlot == null) {
            vOrderTime.setText(R.string.time_not_set);
        }
    }

    @Override
    public void showErrorMessage(String msg) {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setMessage(msg)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void setProgressingVisible(boolean show) {
        if (show) {
            progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.progressing));
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    @Override
    public void showCompleteSuccessfull() {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setMessage(R.string.request_success)
                .setPositiveButton(android.R.string.ok, null)
                .setNeutralButton(R.string.launch_history, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        HistoryActivity.launch(getActivity());
                    }
                })
                .create()
                .show();
    }


    private class TimeSlotAdapter extends BaseAdapter {

        private final List<TimeSlot> list;

        TimeSlotAdapter(List<TimeSlot> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public TimeSlot getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder vh;
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.layout_time_slot, null);
                vh = new ViewHolder(view);
                view.setTag(vh);
            }
            vh = (ViewHolder) view.getTag();
            vh.loadData(getItem(i));
            return view;
        }
    }

    class ViewHolder {
        @BindView(R.id.text)
        TextView vText;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void loadData(TimeSlot ts) {
            vText.setText(ts.toString().substring(0, 16));
        }
    }
}
