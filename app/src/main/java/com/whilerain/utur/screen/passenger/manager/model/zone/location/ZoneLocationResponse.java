
package com.whilerain.utur.screen.passenger.manager.model.zone.location;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ZoneLocationResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * 
     * @return
     *     The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    public void setResult(Result result) {
        this.result = result;
    }
    public static class Result {

        @SerializedName("focus")
        @Expose
        private List<Double> focus = new ArrayList<Double>();
        @SerializedName("area")
        @Expose
        private List<List<List<Double>>> area = new ArrayList<List<List<Double>>>();
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("parent")
        @Expose
        private Object parent;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("locations")
        @Expose
        private List<Location> locations = new ArrayList<Location>();

        /**
         *
         * @return
         *     The focus
         */
        public List<Double> getFocus() {
            return focus;
        }

        /**
         *
         * @param focus
         *     The focus
         */
        public void setFocus(List<Double> focus) {
            this.focus = focus;
        }

        /**
         *
         * @return
         *     The area
         */
        public List<List<List<Double>>> getArea() {
            return area;
        }

        /**
         *
         * @param area
         *     The area
         */
        public void setArea(List<List<List<Double>>> area) {
            this.area = area;
        }

        /**
         *
         * @return
         *     The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The parent
         */
        public Object getParent() {
            return parent;
        }

        /**
         *
         * @param parent
         *     The parent
         */
        public void setParent(Object parent) {
            this.parent = parent;
        }

        /**
         *
         * @return
         *     The createdAt
         */
        public String getCreatedAt() {
            return createdAt;
        }

        /**
         *
         * @param createdAt
         *     The createdAt
         */
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         *
         * @return
         *     The updatedAt
         */
        public String getUpdatedAt() {
            return updatedAt;
        }

        /**
         *
         * @param updatedAt
         *     The updatedAt
         */
        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        /**
         *
         * @return
         *     The locations
         */
        public List<Location> getLocations() {
            return locations;
        }

        /**
         *
         * @param locations
         *     The locations
         */
        public void setLocations(List<Location> locations) {
            this.locations = locations;
        }

    }

    public static class Location {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("zone_id")
        @Expose
        private Integer zoneId;
        @SerializedName("gps")
        @Expose
        private List<Double> gps = new ArrayList<Double>();
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("distance")
        @Expose
        private Double distance;

        /**
         *
         * @return
         *     The id
         */
        public Integer getId() {
            return id;
        }

        /**
         *
         * @param id
         *     The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         *
         * @return
         *     The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         *     The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         *     The zoneId
         */
        public Integer getZoneId() {
            return zoneId;
        }

        /**
         *
         * @param zoneId
         *     The zone_id
         */
        public void setZoneId(Integer zoneId) {
            this.zoneId = zoneId;
        }

        /**
         *
         * @return
         *     The gps
         */
        public List<Double> getGps() {
            return gps;
        }

        /**
         *
         * @param gps
         *     The gps
         */
        public void setGps(List<Double> gps) {
            this.gps = gps;
        }

        /**
         *
         * @return
         *     The address
         */
        public String getAddress() {
            return address;
        }

        /**
         *
         * @param address
         *     The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         *
         * @return
         *     The distance
         */
        public Double getDistance() {
            return distance;
        }

        /**
         *
         * @param distance
         *     The distance
         */
        public void setDistance(Double distance) {
            this.distance = distance;
        }

    }
}
