package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.BootstrapApi;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FromChooserActivity extends AppCompatActivity {

    public static final int REQUEST = 200;

    private static List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean> fromStartLocations;
    private static List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean> fromEndLocations;
    private boolean isFromStart = true;
    private static int selected = 0;
    private LocationAdapter adapter;
    public static Object sChoosedLocation;
    @BindView(R.id.list)
    ListView vList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from);

        ButterKnife.bind(this);
        if (fromStartLocations != null) {
            isFromStart = true;
        } else {
            isFromStart = false;
        }
        initViews();
    }


    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new LocationAdapter();
        vList.setAdapter(adapter);
        vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected = position;
                adapter.notifyDataSetChanged();
            }
        });
    }

    public static void launch(Fragment fragment, List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean> startLocations, List<BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean> endLocations) {
        fromStartLocations = startLocations;
        fromEndLocations = endLocations;
        fragment.startActivityForResult(new Intent(fragment.getContext(), FromChooserActivity.class), REQUEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        fromEndLocations = null;
        fromStartLocations = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            sChoosedLocation = adapter.getItem(selected);
            setResult(RESULT_OK);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    private class LocationAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (isFromStart) {
                return fromStartLocations.size();
            } else {
                return fromEndLocations.size();
            }
        }

        @Override
        public Object getItem(int position) {
            if (isFromStart) {
                return fromStartLocations.get(position);
            } else {
                return fromEndLocations.get(position);
            }
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (isFromStart) {
                BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean b = (BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean) getItem(position);
                ViewHolder viewHolder;
                if(convertView == null){
                    convertView = LayoutInflater.from(FromChooserActivity.this).inflate(R.layout.layout_places_chooser, null);
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                }

                viewHolder = (ViewHolder) convertView.getTag();
                viewHolder.loadData(b, position);

            } else {
                BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean b = (BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean) getItem(position);
                ViewHolder viewHolder;
                if(convertView == null){
                    convertView = LayoutInflater.from(FromChooserActivity.this).inflate(R.layout.layout_places_chooser, null);
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                }

                viewHolder = (ViewHolder) convertView.getTag();
                viewHolder.loadData(b, position);
            }
            return convertView;
        }
    }

    protected class ViewHolder {
        @BindView(R.id.icon)
        ImageView vImage;

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.checked)
        ImageView vChecked;

        int position = 0;

        BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean fromStartLocationTransportsBean;
        BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean fromEndLocationTransportsBean;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void loadData(BootstrapApi.RequestPopularTourResponse.ResultBean.FromStartLocationTransportsBean b, int index) {
            fromStartLocationTransportsBean = b;
            position = index;
            setupViews();
        }

        public void loadData(BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean b, int index) {
            fromEndLocationTransportsBean = b;
            position = index;
            setupViews();
        }

        private void setupViews() {
            if (isFromStart) {
                vImage.setImageResource(R.drawable.ic_directions_bus_black_24dp);
                vName.setText(fromStartLocations.get(position).getStart_location().getName());

            } else {
                vImage.setImageResource(R.drawable.ic_beach_access_black_24dp);
                vName.setText(fromEndLocations.get(position).getEnd_location().getName());
            }
            if(position == selected){
                vChecked.setVisibility(View.VISIBLE);
            }else{
                vChecked.setVisibility(View.INVISIBLE);
            }
            vChecked.invalidate();
        }
    }
}
