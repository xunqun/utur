package com.whilerain.utur.screen.driver.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.travel.FullDayTripDetailRequest;
import com.whilerain.utur.screen.driver.DriverActivity;
import com.whilerain.utur.screen.driver.DriverFullDayTripListActivity;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.passenger.PackageDetailActivity;
import com.whilerain.utur.screen.passenger.manager.PackageManager;
import com.whilerain.utur.screen.passenger.manager.model.transport.FullDayTripResponse;
import com.whilerain.utur.screen.passenger.manager.model.travel.FullDayTripDetailResponse;
import com.whilerain.utur.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DriverPackageFragment extends Fragment implements PackageManager.FullDayTripRequestListener {


    @BindView(R.id.root)
    FrameLayout root;

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwipe;

    private DataAdapter adapter;


    public DriverPackageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PackageManager.getInstance(getContext()).setFullDayTripRequestListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stationshuttle_list, container, false);
        ButterKnife.bind(this, view);

        // Set the adapter

        adapter = new DataAdapter();
        vList.setAdapter(adapter);
        vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(DriverUserManager.getInstance(getContext()).isLogin()) {
                    DriverFullDayTripListActivity.launch(getActivity(),adapter.getItem(position).getId(), adapter.getItem(position).getName());
                }else{

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.require_loged_in)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ((DriverActivity)getActivity()).switchToPageNum(3);
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, null);
                    builder.create().show();
                }

            }
        });
        vSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListFromServer();
            }
        });

        return view;
    }

    private void requestFromServer(final int id) {
        FullDayTripDetailRequest.build(id).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    Gson gson = new Gson();
                    final FullDayTripDetailResponse response = gson.fromJson(body, FullDayTripDetailResponse.class);
                    if(response.isStatus()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                PackageDetailActivity.launch(getActivity(), response, id);
                            }
                        });
                    }else {
                        showErrorMessage(getString(R.string.error_data_query_fail));
                    }
                    return;
                } catch (Exception e) {

                }
                showErrorMessage(getString(R.string.error_data_query_fail));
            }

            @Override
            public void onFail() {
                showErrorMessage(getString(R.string.error_connection_fail));
            }
        }).get();
    }

    private void showErrorMessage(final String msg) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getListFromServer();
    }

    private void getListFromServer(){
        if(NetworkUtils.isOnline(getContext())) {
            PackageManager.getInstance(getContext()).requestFromServer(getContext());
            vSwipe.setRefreshing(true);
        }else{
            Snackbar snackbar = Snackbar.make(root, R.string.error_no_network, Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(Color.RED);
            snackbar.show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess() {
        if(vSwipe.isRefreshing()) {
            vSwipe.setRefreshing(false);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFail() {
        if(vSwipe.isRefreshing()) {
            vSwipe.setRefreshing(false);
        }
        Toast.makeText(getContext(), R.string.error_data_query_fail, Toast.LENGTH_SHORT).show();
    }

    class DataAdapter extends BaseAdapter {
        List<FullDayTripResponse.ResultBean> results = new ArrayList<>();

        public DataAdapter(){
            initData();
        }

        @Override
        public void notifyDataSetChanged() {
            initData();
            super.notifyDataSetChanged();
        }

        private void initData() {
            results = PackageManager.getInstance(getContext()).getResults();
            if(results == null){
                results = new ArrayList<>();
            }
        }

        @Override
        public int getCount() {
            return results.size();
        }

        @Override
        public FullDayTripResponse.ResultBean getItem(int position) {
            return results.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_fulldaytrip_item, null);
                vh = new ViewHolder(convertView);
                convertView.setTag(vh);
            }
            vh = (ViewHolder) convertView.getTag();
            vh.bindContent(getItem(position));

            return convertView;
        }
    }

    class ViewHolder {

        @BindView(R.id.content)
        TextView vContent;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }

        public void bindContent(FullDayTripResponse.ResultBean result){
            vContent.setText(result.getName());
        }
    }
}
