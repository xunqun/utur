package com.whilerain.utur.screen.driver.manager;

import android.content.Context;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.order.DriverHistoryOrderRequest;
import com.whilerain.utur.utils.SimpleDataCache;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/4/24.
 */
public class DriverHistoryManager {
    private static DriverHistoryManager instance;
    private Context context;
    private DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean resultBean;
    private List<HistoryListener> listeners = new ArrayList<>();

    public interface HistoryListener{
        void onDataSetChange();
        void onFail();
    }

    private DriverHistoryManager(Context context){
        this.context = context;
        SimpleDataCache<DriverHistoryOrderRequest.DriverHistoryResponse> cache = new SimpleDataCache<DriverHistoryOrderRequest.DriverHistoryResponse>();
        DriverHistoryOrderRequest.DriverHistoryResponse response = cache.get(context, DriverHistoryOrderRequest.DriverHistoryResponse.class);
        if(response != null) {
            resultBean = response.getResult();
        }
    }

    public static DriverHistoryManager getInstance(Context context){

        if(instance == null){
            instance = new DriverHistoryManager(context);
        }
        instance.context = context;
        return instance;
    }

    @Nullable
    public DriverHistoryOrderRequest.DriverHistoryResponse.ResultBean getResult(){
        return resultBean;
    }

    public void addHistoryListener(HistoryListener listener){
        listeners.add(listener);
    }

    public void removeHistoryListener(HistoryListener listener){
        listeners.remove(listener);
    }

    private void notifyDatasetChange(){
        for(HistoryListener l : listeners){
            l.onDataSetChange();
        }
    }

    private void notifyFail(){
        for(HistoryListener l : listeners){
            l.onFail();
        }
    }

    public void requestFromServer(){
        String uuid = DriverUserManager.getInstance(context).getUserInfo().getUuid();
        DriverHistoryOrderRequest.build(uuid).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {

                Gson gson = new Gson();
                DriverHistoryOrderRequest.DriverHistoryResponse response = gson.fromJson(body, DriverHistoryOrderRequest.DriverHistoryResponse.class);
                if(response.isStatus()){
                    SimpleDataCache<DriverHistoryOrderRequest.DriverHistoryResponse> cache = new SimpleDataCache<DriverHistoryOrderRequest.DriverHistoryResponse>();
                    cache.set(context, DriverHistoryOrderRequest.DriverHistoryResponse.class, body);
                    resultBean = response.getResult();
                    notifyDatasetChange();
                }else{
                    notifyFail();
                }
            }

            @Override
            public void onFail() {
                notifyFail();
            }
        }).get();
    }
}
