package com.whilerain.utur.screen.passenger.manager.model.transport.schedule;

import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/6/17.
 */
public class HalfDayTripScheduleResponse extends SimpleDataCache.CachePoJo {

    /**
     * status : true
     * result : {"id":8,"name":"西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣","category":"share","back":0,"start_zone":2,"start_location":470,"end_zone":2,"end_location":470,"duration":200,"price":2000,"discount":null,"type":"half_day","timetable":["09:00","13:00"]}
     */

    private boolean status;
    /**
     * id : 8
     * name : 西半島半日遊: 後壁湖 - 貓鼻頭 - 白沙灣
     * category : share
     * back : 0
     * start_zone : 2
     * start_location : 470
     * end_zone : 2
     * end_location : 470
     * duration : 200
     * price : 2000
     * discount : null
     * type : half_day
     * timetable : ["09:00","13:00"]
     */

    private ResultBean result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private int id;
        private String name;
        private String category;
        private int back;
        private int start_zone;
        private int start_location;
        private int end_zone;
        private int end_location;
        private int duration;
        private int price;
        private Object discount;
        private String type;
        private List<String> timetable;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public int getBack() {
            return back;
        }

        public void setBack(int back) {
            this.back = back;
        }

        public int getStart_zone() {
            return start_zone;
        }

        public void setStart_zone(int start_zone) {
            this.start_zone = start_zone;
        }

        public int getStart_location() {
            return start_location;
        }

        public void setStart_location(int start_location) {
            this.start_location = start_location;
        }

        public int getEnd_zone() {
            return end_zone;
        }

        public void setEnd_zone(int end_zone) {
            this.end_zone = end_zone;
        }

        public int getEnd_location() {
            return end_location;
        }

        public void setEnd_location(int end_location) {
            this.end_location = end_location;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public Object getDiscount() {
            return discount;
        }

        public void setDiscount(Object discount) {
            this.discount = discount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<String> getTimetable() {
            return timetable;
        }

        public void setTimetable(List<String> timetable) {
            this.timetable = timetable;
        }
    }
}
