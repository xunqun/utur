package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.SignUpApi;
import com.whilerain.utur.utils.StringUtils;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {

    public static int REQUEST_RESET_PW = 101;
    @BindView(R.id.send)
    Button vSend;

    @BindView(R.id.passcode)
    EditText vPasscode;

    @BindView(R.id.confirm_pw)
    EditText vConfirmPassword;

    @BindView(R.id.pw)
    EditText vPassword;

    private static String account;

    public static void launch(Activity activity, String acc){
        account = acc;
        activity.startActivity(new Intent(activity, ResetPasswordActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.send)
    protected void onSend() {
        if (isValid()) {
            try {
                Call<SignUpApi.ResetPasswordResponse> call = SignUpApi.resetPasswordCode(account, vPasscode.getText().toString(), vPassword.getText().toString());
                call.enqueue(new Callback<SignUpApi.ResetPasswordResponse>() {
                    @Override
                    public void onResponse(Call<SignUpApi.ResetPasswordResponse> call, Response<SignUpApi.ResetPasswordResponse> response) {

                        SignUpApi.ResetPasswordResponse responseBean = response.body();
                        if(responseBean.isStatus()){
                            Toast.makeText(ResetPasswordActivity.this, R.string.reset_password_success, Toast.LENGTH_SHORT).show();
                            PassengerActivity.launchClearTask(ResetPasswordActivity.this, 3);
                        }else{
                            Toast.makeText(ResetPasswordActivity.this, R.string.error_authenticate_fail, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpApi.ResetPasswordResponse> call, Throwable t) {
                        Log.d("xunqun", "onFailure: ");
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isValid() {
        vPasscode.setError(null);
        vPassword.setError(null);
        vConfirmPassword.setError(null);

        boolean error = false;
        if(TextUtils.isEmpty(vPasscode.getText().toString())){
            vPasscode.setError(getString(R.string.error_can_not_empty));
            error = true;
        }

        if(TextUtils.isEmpty(vPassword.getText().toString())){
            vPassword.setError(getString(R.string.error_can_not_empty));
            error = true;
        }

        if(TextUtils.isEmpty(vConfirmPassword.getText().toString())){
            vConfirmPassword.setError(getString(R.string.error_can_not_empty));
            error = true;
        }

        if(!vConfirmPassword.getText().toString().equals(vPassword.getText().toString())){
            vConfirmPassword.setError(getString(R.string.error_confirm_pw_not_match));
            error = true;
        }
        return !error;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
