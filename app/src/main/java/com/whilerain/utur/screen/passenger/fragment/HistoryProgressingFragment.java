package com.whilerain.utur.screen.passenger.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.model.ApiReference;
import com.whilerain.utur.screen.passenger.EngagedDriverInfoOrderActivity;
import com.whilerain.utur.screen.passenger.manager.HistoryManager;
import com.whilerain.utur.screen.passenger.manager.model.passenger.order.PassengerHistoryResponse;
import com.whilerain.utur.utils.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xunqun on 16/4/24.
 */
public class HistoryProgressingFragment extends Fragment implements HistoryManager.HistoryListener {

    @BindView(R.id.swipe)
    SwipeRefreshLayout vSwiper;

    @BindView(R.id.list)
    ListView vList;

    @BindView(R.id.empty_state)
    ViewGroup vEmptyState;

    private HistoryAdapter adapter;

    @Override
    public void onResume() {
        super.onResume();
        HistoryManager.getInstance(getContext()).addHistoryListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        HistoryManager.getInstance(getContext()).addHistoryListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_progressing, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        adapter = new HistoryAdapter();
        vList.setAdapter(adapter);
        vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object object = adapter.getItem(position);
                EngagedDriverInfoOrderActivity.launch(getActivity(), object);
            }
        });

        vSwiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                HistoryManager.getInstance(getContext()).requestFromServer();
            }
        });
    }

    @Override
    public void onDataSetChange() {
        if (vSwiper.isRefreshing()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    vSwiper.setRefreshing(false);
                }
            });

        }

        if (adapter != null) {
            vList.postDelayed(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            }, 2000);
        }
    }

    @Override
    public void onFail() {
        if (vSwiper.isRefreshing()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    vSwiper.setRefreshing(false);
                }
            });
        }
    }

    class HistoryAdapter extends BaseAdapter {
        List<PassengerHistoryResponse.ResultBean.OrdersBean.Order> oList = new ArrayList<>();
        List<PassengerHistoryResponse.ResultBean.RequestsBean.Request> rList = new ArrayList<>();

        HistoryAdapter() {
            initData();
        }

        public void initData() {
            oList.clear();
            rList.clear();
            PassengerHistoryResponse.ResultBean result = HistoryManager.getInstance(getContext()).getResult();
            if (result != null) {
                if (result.getOrders().getNowandthen() != null) {
                    oList.addAll(result.getOrders().getNowandthen());
                }

                if (result.getRequests().getNowandthen() != null) {
                    rList.addAll(result.getRequests().getNowandthen());
                }
            }

            if (oList.size() + rList.size() == 0) {
                vEmptyState.setVisibility(View.VISIBLE);
            } else {
                vEmptyState.setVisibility(View.GONE);
            }
        }

        @Override
        public void notifyDataSetChanged() {
            initData();
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return oList.size() + rList.size();
        }

        @Override
        public Object getItem(int position) {
            if (position < oList.size()) {
                return oList.get(position);
            } else {
                return rList.get(position - oList.size());
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            OrderViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_history_order, null);
                vh = new OrderViewHolder(convertView);
                convertView.setTag(vh);
            }

            vh = (OrderViewHolder) convertView.getTag();
            if (position < oList.size()) {
                vh.setOrder(oList.get(position));
            } else {
                vh.setRequest(rList.get(position - oList.size()));
            }

            return convertView;

        }
    }

    protected class OrderViewHolder {
        @BindView(R.id.time)
        TextView vTime;

        @BindView(R.id.seat)
        TextView vSeat;

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.state)
        TextView vState;

        private PassengerHistoryResponse.ResultBean.OrdersBean.Order order;
        private PassengerHistoryResponse.ResultBean.RequestsBean.Request request;


        public OrderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void setOrder(PassengerHistoryResponse.ResultBean.OrdersBean.Order order) {

            this.order = order;

            PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean startLocation = order.getRequests().get(0).getStart_location();
            startLocation = startLocation == null ? order.getRequests().get(0).getStart_location_custom() : startLocation;
            PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean.LocationBean endLocation = order.getRequests().get(0).getEnd_location();
            endLocation = endLocation == null ? order.getRequests().get(0).getEnd_location_custom() : endLocation;
            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", order.getStart_time());
                vTime.setText(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            PassengerHistoryResponse.ResultBean.OrdersBean.Order.RequestsBean requestsBean = order.getRequests().get(0);
            vSeat.setText(String.format(getString(R.string.n_seat), String.valueOf(requestsBean.getPassenger_count())));

            if (order.getCategory().equals(ApiReference.TransType.PACKAGE.toString())) {
                String name = requestsBean.getName();
                vName.setText(StringUtils.trimSpecialString(name));
            } else if (order.getCategory().equals(ApiReference.TransType.TRANSPORT.toString())) {
                String name = startLocation.getName() + " ► " + endLocation.getName();
                vName.setText(name);
            } else if (order.getCategory().equals(ApiReference.TransType.SHARE.toString())) {
                String name = StringUtils.trimSpecialString(requestsBean.getName());
                vName.setText(name);
            } else if (order.getCategory().equals(ApiReference.TransType.TAXI.toString())) {
                vName.setText(R.string.title_free_routes);
            }


            vState.setText(StringUtils.getStatusString(getContext(), order.getStatus()));
            int colorRes = getStateRes(order.getStatus());

            vState.setCompoundDrawablesWithIntrinsicBounds(colorRes, 0, 0, 0);
        }

        public void setRequest(PassengerHistoryResponse.ResultBean.RequestsBean.Request request) {

            this.request = request;

            PassengerHistoryResponse.ResultBean.RequestsBean.Request.LocationBean startLocation = request.getStart_location();
            startLocation = startLocation == null ? request.getStart_location_custom() : startLocation;
            PassengerHistoryResponse.ResultBean.RequestsBean.Request.LocationBean endLocation = request.getEnd_location();
            endLocation = endLocation == null ? request.getEnd_location_custom() : endLocation;
            try {
                String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", request.getTimerange().get(0));
                vTime.setText(timeStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            vSeat.setText(String.format(getString(R.string.n_seat), String.valueOf(request.getPassenger_count())));


            if (request.getCategory().equals(ApiReference.TransType.TRANSPORT.toString())) {
                String name = startLocation.getName() + " ► " + endLocation.getName();
                vName.setText(name);
            } else if (request.getCategory().equals(ApiReference.TransType.TAXI.toString())) {
                vName.setText(R.string.title_free_routes);
            } else {
                String name = request.getName().replace("\u0096", "");
                vName.setText(name);

            }

            vState.setText(StringUtils.getStatusString(getContext(), request.getStatus()));
            int colorRes = getStateRes(request.getStatus());

            vState.setCompoundDrawablesWithIntrinsicBounds(colorRes, 0, 0, 0);
        }
    }

    private int getStateRes(String status) {
        if (status.equals("queue")) {
            return R.drawable.shape_yellow_circle;
        } else if (status.equals("accepted") || status.equals("ontheroad") || status.equals("proceeding")) {
            return R.drawable.shape_green_circle;
        } else {
            return R.drawable.shape_red_circle;
        }
    }
}
