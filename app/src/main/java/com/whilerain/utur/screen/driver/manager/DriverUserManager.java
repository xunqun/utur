package com.whilerain.utur.screen.driver.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.whilerain.utur.apis.PushApi;
import com.whilerain.utur.gcm.RegistrationIntentService;
import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.login.DriverLoginRequest;
import com.whilerain.utur.net.register.CodeVerifyRequest;
import com.whilerain.utur.net.register.DriverRegisterRequest;
import com.whilerain.utur.screen.driver.manager.model.driver.login.DriverLoginResponse;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.utils.SharePrefHandler;
import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xunqun on 16/4/13.
 */
public class DriverUserManager {
    private static DriverUserManager instance;
    private DriverLoginResponse.Result result;
    private List<DriverUserListener> listeners = new ArrayList<>();
    // Used for un-login state
    private String temp_uuid;
    private String temp_email;
    private String temp_name;



    public interface DriverUserListener {

        void onFail();

        void onLoginStateChange();

        void onRegisterResult(boolean success);

        void onSmsCodeVerifyResult(boolean success);
    }

    private DriverUserManager(Context context) {

        // FIXME: 16/4/13
        SimpleDataCache<DriverLoginResponse> cache = new SimpleDataCache<DriverLoginResponse>();
        DriverLoginResponse response = cache.get(context, DriverLoginResponse.class);
        if (response != null) {
            result = cache.get(context, DriverLoginResponse.class).getResult();
        } else {
            result = null;
        }
    }

    public static DriverUserManager getInstance(Context context) {

        if (instance == null) {
            instance = new DriverUserManager(context);
        }
        return instance;
    }

    public DriverLoginResponse.Result getUserInfo() {
        return result;
    }

    public String getAccount(Context context){
        return SharePrefHandler.getSharedPrefences(context).getString(SharePrefHandler.ACCOUNT, "");
    }

    public boolean isLogin() {
        return result != null;
    }

    public void requestCodeVerify(final Context context, String code) {
        // FIXME: 16/4/13
        CodeVerifyRequest.build(temp_uuid, code).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        Gson gson = new Gson();
                        result = new DriverLoginResponse.Result();
                        result.setUuid(jsonObject.getJSONObject("result").getString("id"));
                        result.setApiKey(jsonObject.getJSONObject("result").getString("api_secret_key"));
                        result.setName(temp_name);
                        result.setEmail(temp_email);

                        SimpleDataCache<DriverLoginResponse> cache = new SimpleDataCache<DriverLoginResponse>();
                        cache.set(context, DriverLoginResponse.class, gson.toJson(result));
                        result = cache.get(context, DriverLoginResponse.class).getResult();

                        dispatchCodeVerifyResult(true);
                        return;
                    } else {
                        dispatchCodeVerifyResult(false);
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchCodeVerifyResult(false);
            }

            @Override
            public void onFail() {
                dispatchCodeVerifyResult(false);
            }
        }).post();
    }

    public void requestRegister(final Context context, String phone, final String email, final String name, String password) {
        
        SharePrefHandler.getEditor(context).putString(SharePrefHandler.ACCOUNT, phone).apply();
        DriverRegisterRequest.build(phone, email, name, password).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        temp_uuid = jsonObject.getJSONObject("result").getString("uuid");
                        temp_name = name;
                        temp_email = email;
                        dispatchRegisterResult(true);
                        return;
                    } else {
                        dispatchRegisterResult(false);
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRegisterResult(false);
            }

            @Override
            public void onFail() {
                dispatchRegisterResult(false);
            }
        }).post();
    }

    public void requestLogin(final Context context, String account, String password) {

        SharePrefHandler.getEditor(context).putString(SharePrefHandler.ACCOUNT, account).apply();
        DriverLoginRequest.build(account, password).setApiListener(new ApiRequest.ApiListener() {
            @Override
            public void onSeccess(ApiRequest apiRequest, String body) {
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    if (jsonObject.getBoolean("status")) {
                        SimpleDataCache<DriverLoginResponse> cache = new SimpleDataCache<DriverLoginResponse>();
                        cache.set(context, DriverLoginResponse.class, body);
                        result = cache.get(context, DriverLoginResponse.class).getResult();
                        dispatchLoginStateChange();
                        RegistrationIntentService.launch(context, PassengerUserManager.getInstance(context).getUserInfo().getUuid());
                        return;
                    } else {

                        dispatchRequestFail();
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dispatchRequestFail();
            }

            @Override
            public void onFail() {
                dispatchRequestFail();
            }
        }).post();
    }

    public void logout(Context context) {
        String gcmToken = SharePrefHandler.getSharedPrefences(context).getString(SharePrefHandler.GCM_TOKEN, "");
        if(gcmToken.length() > 0){
            try {
                PushApi.requestDeleteToken(DriverUserManager.getInstance(context).getUserInfo().getUuid(), gcmToken);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        SharePrefHandler.getEditor(context).clear().apply();
        SimpleDataCache<DriverLoginResponse> cache = new SimpleDataCache<DriverLoginResponse>();
        cache.set(context, DriverLoginResponse.class, null);
        result = null;
        dispatchLoginStateChange();
    }

    public void addDriverUserListener(DriverUserListener l) {
        this.listeners.add(l);
    }

    public void removeDriverUserListener(DriverUserListener l) {
        synchronized (listeners) {
            this.listeners.remove(l);
        }
    }

    public void dispatchCodeVerifyResult(final boolean b) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listeners.size(); i++) {
                    DriverUserListener l = listeners.get(i);
                    l.onSmsCodeVerifyResult(b);
                }
            }
        });
    }

    public void dispatchRegisterResult(final boolean b) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listeners.size(); i++) {
                    DriverUserListener l = listeners.get(i);
                    l.onRegisterResult(b);
                }
            }
        });
    }

    public void dispatchRequestFail() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listeners.size(); i++) {
                    DriverUserListener l = listeners.get(i);
                    l.onFail();
                }
            }
        });

    }

    public void dispatchLoginStateChange() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                synchronized (listeners) {
                    for (int i = 0; i < listeners.size(); i++) {
                        DriverUserListener l = listeners.get(i);
                        l.onLoginStateChange();
                    }
                }
            }
        });
    }
}
