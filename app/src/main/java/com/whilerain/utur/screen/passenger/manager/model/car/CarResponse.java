package com.whilerain.utur.screen.passenger.manager.model.car;

import com.whilerain.utur.utils.SimpleDataCache;

/**
 * Created by xunqun on 16/6/22.
 */
public class CarResponse extends SimpleDataCache.CachePoJo {


    /**
     * status : true
     * result : {"licenseplate":"4321-DX","model":"Tiida","seat":4,"picture":null,"license":"87654321","licensepic":null}
     */

    private boolean status;
    /**
     * licenseplate : 4321-DX
     * model : Tiida
     * seat : 4
     * picture : null
     * license : 87654321
     * licensepic : null
     */

    private ResultBean result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private String licenseplate;
        private String model;
        private String picture;

        public String getLicenseplate() {
            return licenseplate;
        }

        public void setLicenseplate(String licenseplate) {
            this.licenseplate = licenseplate;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

    }
}
