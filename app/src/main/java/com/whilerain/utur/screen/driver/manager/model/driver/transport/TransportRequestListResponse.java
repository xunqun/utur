package com.whilerain.utur.screen.driver.manager.model.driver.transport;

import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

/**
 * Created by xunqun on 16/5/2.
 */
public class TransportRequestListResponse extends SimpleDataCache.CachePoJo{

    /**
     * status : true
     * result : [{"id":"2","request_id":"05010000109000002","transport_id":"000010900","travel":1,"date":"2016-05-31T00:00:00.000Z","seq":null,"timerange":["2016-05-31T01:00:00.000Z","2016-05-31T03:00:00.000Z"],"start_location":{"gps":[22.6886748,120.3075627],"id":474,"name":"台鐵新左營站(客運)","zone_id":1,"address":"高雄市左營區站前北路"},"start_location_custom":null,"end_location":{"gps":[21.94741,120.79952],"id":16,"name":"二手童話民宿","zone_id":2,"address":"屏東縣恆春鎮墾丁路和平巷91號"},"end_location_custom":null,"passenger":4,"passenger_count":2,"order":null,"status":"queue","is_deleted":false,"price":450,"discount":0.8,"time":"09:00"}]
     */

    private boolean status;
    /**
     * id : 2
     * request_id : 05010000109000002
     * transport_id : 000010900
     * travel : 1
     * date : 2016-05-31T00:00:00.000Z
     * seq : null
     * timerange : ["2016-05-31T01:00:00.000Z","2016-05-31T03:00:00.000Z"]
     * start_location : {"gps":[22.6886748,120.3075627],"id":474,"name":"台鐵新左營站(客運)","zone_id":1,"address":"高雄市左營區站前北路"}
     * start_location_custom : null
     * end_location : {"gps":[21.94741,120.79952],"id":16,"name":"二手童話民宿","zone_id":2,"address":"屏東縣恆春鎮墾丁路和平巷91號"}
     * end_location_custom : null
     * passenger : 4
     * passenger_count : 2
     * order : null
     * status : queue
     * is_deleted : false
     * price : 450
     * discount : 0.8
     * time : 09:00
     */

    private List<DriverApi.RequestListResponse.ResultBean> result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DriverApi.RequestListResponse.ResultBean> getResult() {
        return result;
    }

    public void setResult(List<DriverApi.RequestListResponse.ResultBean> result) {
        this.result = result;
    }


}
