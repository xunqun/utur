package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.screen.passenger.manager.model.transport.schedule.Schedule;
import com.whilerain.utur.screen.passenger.manager.model.zone.location.ZoneLocationResponse.Location;
import com.whilerain.utur.utils.StringUtils;
import com.whilerain.utur.utils.UiUtils;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverTransportDetailActivity extends AppCompatActivity {

    public static DriverApi.RequestListResponse.ResultBean data;


    @BindView(R.id.departure)
    TextView vDeparture;

    @BindView(R.id.departure_addr)
    TextView vDepartureAddr;

    @BindView(R.id.destination)
    TextView vDestination;

    @BindView(R.id.destination_addr)
    TextView vDestinationAddr;

    @BindView(R.id.order_date)
    TextView vOrderDate;

    @BindView(R.id.order_time)
    TextView vOrderTime;

    @BindView(R.id.order_count)
    TextView vOrderCount;

    @BindView(R.id.price)
    TextView vPrice;



    private String date;
    private Schedule schedule;
    private Location departure;
    private Location destination;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_station_shuttle_detail);
        ButterKnife.bind(this);
        initView();
    }

    @OnClick(R.id.fab)
    public void onSend(){

        TransportComfirmAcceptOrderActivity.launch(this, data.getRequest_id(), data.getTransport_id());
    }

    @OnClick(R.id.map_departure)
    public void onMapDeparture(){
        UiUtils.showLocation(this, String.valueOf(getStartlocation(data).getGps().get(0)), String.valueOf(getStartlocation(data).getGps().get(1)), getStartlocation(data).getAddress());
    }

    @OnClick(R.id.map_destination)
    public void onMapDestination(){
        UiUtils.showLocation(this, String.valueOf(getEndlocation(data).getGps().get(0)), String.valueOf(getEndlocation(data).getGps().get(1)), getEndlocation(data).getAddress());
    }

    private void initView() {
        DriverApi.RequestListResponse.ResultBean.LocationBean startLocation = getStartlocation(data);
        DriverApi.RequestListResponse.ResultBean.LocationBean endLocation = getEndlocation(data);
        vDeparture.setText(startLocation.getName() == null ? "": startLocation.getName());
        vDepartureAddr.setText(startLocation.getAddress());
        vDestination.setText(endLocation.getName());
        vDestinationAddr.setText(endLocation.getAddress());
        try {
            String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", data.getTimerange().get(0));
            vOrderDate.setText(timeStr.substring(0, 10));
            vOrderTime.setText(timeStr.substring(11));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        vOrderCount.setText(String.valueOf(data.getPassenger_count()));

        vPrice.setText(String.valueOf(data.getPrice() * data.getPassenger_count() * data.getDiscount()));

    }

    private DriverApi.RequestListResponse.ResultBean.LocationBean getEndlocation(DriverApi.RequestListResponse.ResultBean data) {
        if(data.getEnd_location() != null) {
            return data.getEnd_location();
        }else{
            return data.getEnd_location_custom();
        }
    }

    private DriverApi.RequestListResponse.ResultBean.LocationBean getStartlocation(DriverApi.RequestListResponse.ResultBean data) {
        if(data.getStart_location() != null) {
            return data.getStart_location();
        }else{
            return data.getStart_location_custom();
        }
    }

    public static void launch(Activity activity, DriverApi.RequestListResponse.ResultBean r) {
        data = r;
        Intent it = new Intent(activity, DriverTransportDetailActivity.class);
        activity.startActivity(it);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showErrorDialog(int msgRes) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(msgRes))
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }
}
