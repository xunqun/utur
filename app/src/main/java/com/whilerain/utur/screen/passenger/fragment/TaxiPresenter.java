package com.whilerain.utur.screen.passenger.fragment;

import com.google.android.gms.location.places.Place;
import com.whilerain.utur.MyApplication;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.taxi.PassengerTaxiApi;
import com.whilerain.utur.screen.passenger.manager.PassengerUserManager;
import com.whilerain.utur.utils.StringUtils;

import org.json.JSONException;

import java.text.ParseException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shawn on 2017/4/21.
 */

public class TaxiPresenter implements TaxiContract.Presenter {

    private final TaxiContract.View view;
    private Place departurePlace;
    private Place destinationPlace;
    private com.whilerain.utur.screen.passenger.model.TimeSlot timeSlot;


    public TaxiPresenter(TaxiContract.View view){
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void beginStep2(Place departure) {
        this.departurePlace = departure;
        view.showStep2In();
    }

    @Override
    public void beginStep3(Place destination) {
        this.destinationPlace = destination;
        view.showStep3In();
    }

    @Override
    public void beginStep4(com.whilerain.utur.screen.passenger.model.TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
        view.setProgressingVisible(true);
        try {
            Call<PassengerTaxiApi.TaxiQueryPriceResponse> call = PassengerTaxiApi.taxiQueryPrice(PassengerUserManager.getInstance(MyApplication.getInstance()).getUserInfo().getUuid(),
                    timeSlot.toString(), departurePlace.getAddress().toString(), departurePlace.getLatLng().latitude, departurePlace.getLatLng().longitude,
                    destinationPlace.getAddress().toString(), destinationPlace.getLatLng().latitude, destinationPlace.getLatLng().longitude);

            call.enqueue(new Callback<PassengerTaxiApi.TaxiQueryPriceResponse>() {
                @Override
                public void onResponse(Call<PassengerTaxiApi.TaxiQueryPriceResponse> call, Response<PassengerTaxiApi.TaxiQueryPriceResponse> response) {
                    view.setProgressingVisible(false);
                    PassengerTaxiApi.TaxiQueryPriceResponse r = response.body();
                    if(r.isStatus()){
                        view.showStep4In(r);
                    }else{

                        view.showErrorMessage(r.getResult().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<PassengerTaxiApi.TaxiQueryPriceResponse> call, Throwable t) {
                    view.setProgressingVisible(false);
                    view.showErrorMessage(MyApplication.getInstance().getString(R.string.error_connection_fail));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void backToStep(int i) {
        switch (i){
            case 1:
                view.showStep2Out();
                break;

            case 2:
                view.showStep3Out();
                break;

            case 3:
                view.showStep4Out();
                break;
        }
    }

    @Override
    public void sendRequest(int pipoCount, int total, String contact_phone) {
        view.setProgressingVisible(true);
        try {
            String utcTime = StringUtils.localTimeToUtc("yyyy-MM-dd HH:mm:ss", timeSlot.toString());
            Call<PassengerTaxiApi.TaxiRequestResponse> call = PassengerTaxiApi.taxiRequest(PassengerUserManager.getInstance(MyApplication.getInstance()).getUserInfo().getUuid(),
                    utcTime,pipoCount, departurePlace.getAddress().toString(), departurePlace.getLatLng().latitude, departurePlace.getLatLng().longitude, destinationPlace.getAddress().toString(),
                    destinationPlace.getLatLng().latitude, destinationPlace.getLatLng().longitude, total, contact_phone );

            call.enqueue(new Callback<PassengerTaxiApi.TaxiRequestResponse>() {
                @Override
                public void onResponse(Call<PassengerTaxiApi.TaxiRequestResponse> call, Response<PassengerTaxiApi.TaxiRequestResponse> response) {
                    view.setProgressingVisible(false);
                    PassengerTaxiApi.TaxiRequestResponse r = response.body();
                    if(r.isStatus()){
                        view.showCompleteSuccessfull();
                        view.showInitStaus();
                    }else{
                        view.showErrorMessage(r.getResult().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<PassengerTaxiApi.TaxiRequestResponse> call, Throwable t) {
                    view.setProgressingVisible(false);
                    view.showErrorMessage(MyApplication.getInstance().getString(R.string.error_connection_fail));
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
