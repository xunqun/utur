
package com.whilerain.utur.screen.passenger.manager.model.transport.schedule;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Result {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("back")
    @Expose
    private Integer back;
    @SerializedName("start_zone")
    @Expose
    private Integer startZone;
    @SerializedName("start_location")
    @Expose
    private Location startLocation;
    @SerializedName("end_zone")
    @Expose
    private Integer endZone;
    @SerializedName("end_location")
    @Expose
    private Location endLocation;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("travel")
    @Expose
    private Object travel;
    @SerializedName("schedule")
    @Expose
    private List<Schedule> schedule = new ArrayList<Schedule>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The back
     */
    public Integer getBack() {
        return back;
    }

    /**
     * 
     * @param back
     *     The back
     */
    public void setBack(Integer back) {
        this.back = back;
    }

    /**
     * 
     * @return
     *     The startZone
     */
    public Integer getStartZone() {
        return startZone;
    }

    /**
     * 
     * @param startZone
     *     The start_zone
     */
    public void setStartZone(Integer startZone) {
        this.startZone = startZone;
    }

    /**
     * 
     * @return
     *     The startLocation
     */
    public Location getStartLocation() {
        return startLocation;
    }

    /**
     * 
     * @param startLocation
     *     The start_location
     */
    public void setStartLocation(Location startLocation) {
        this.startLocation = startLocation;
    }

    /**
     * 
     * @return
     *     The endZone
     */
    public Integer getEndZone() {
        return endZone;
    }

    /**
     * 
     * @param endZone
     *     The end_zone
     */
    public void setEndZone(Integer endZone) {
        this.endZone = endZone;
    }

    /**
     * 
     * @return
     *     The endLocation
     */
    public Location getEndLocation() {
        return endLocation;
    }

    /**
     * 
     * @param endLocation
     *     The end_location
     */
    public void setEndLocation(Location endLocation) {
        this.endLocation = endLocation;
    }

    /**
     * 
     * @return
     *     The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updatedAt
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The travel
     */
    public Object getTravel() {
        return travel;
    }

    /**
     * 
     * @param travel
     *     The travel
     */
    public void setTravel(Object travel) {
        this.travel = travel;
    }

    /**
     * 
     * @return
     *     The schedule
     */
    public List<Schedule> getSchedule() {
        return schedule;
    }

    /**
     * 
     * @param schedule
     *     The schedule
     */
    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }

}
