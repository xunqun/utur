package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.driver.manager.DriverUserManager;
import com.whilerain.utur.screen.driver.manager.model.driver.car.VehicleListResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleActivity extends AppCompatActivity {

    @BindView(R.id.model)
    TextView vModel;

    @BindView(R.id.seat)
    TextView vSeat;

    @BindView(R.id.vehicle_num)
    TextView vVehicleNum;

    @BindView(R.id.pic)
    ImageView vPic;

    @BindView(R.id.license_num)
    TextView vLicenseNum;

    @BindView(R.id.license_pic)
    ImageView vLicensePic;

    private static VehicleListResponse.Car car;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);
        ButterKnife.bind(this);
        initViews();
    }

    public static void launch(Activity activity, VehicleListResponse.Car c){
        car = c;
        activity.startActivity(new Intent(activity, VehicleActivity.class));
    }

    private void initViews() {
        vModel.setText(car.getModel());
        vSeat.setText(String.valueOf(car.getSeat()));
        vVehicleNum.setText(car.getLicenseplate());
    }
}
