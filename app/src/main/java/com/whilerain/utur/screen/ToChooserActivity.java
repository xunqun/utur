package com.whilerain.utur.screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.BootstrapApi;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ToChooserActivity extends AppCompatActivity {

    public static final int REQUEST = 300;
    @BindView(R.id.list)
    ListView vList;

    TransportAdapter adapter;

    private static List<BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean> transports;
    public static BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean sChooseedTransport;
    private int selected = 0;

    public static void launch(Fragment fragment, List<BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean> l) {
        transports = l;
        fragment.startActivityForResult(new Intent(fragment.getContext(), ToChooserActivity.class), REQUEST);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_chooser);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new TransportAdapter();
        vList.setAdapter(adapter);
        vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected = position;
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            sChooseedTransport = adapter.getItem(selected);
            setResult(RESULT_OK);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    public class TransportAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return transports.size();
        }

        @Override
        public BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean getItem(int position) {
            return transports.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(ToChooserActivity.this).inflate(R.layout.layout_places_chooser, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }

            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.loadData(getItem(position), position);
            return convertView;
        }
    }

    protected class ViewHolder {
        @BindView(R.id.icon)
        ImageView vImage;

        @BindView(R.id.name)
        TextView vName;

        @BindView(R.id.checked)
        ImageView vChecked;

        int position = 0;

        BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean transportsBean;
        BootstrapApi.RequestPopularTourResponse.ResultBean.FromEndLocationTransportsBean fromEndLocationTransportsBean;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void loadData(BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean b, int index) {
            transportsBean = b;
            position = index;
            setupViews();
        }

        private void setupViews() {

            vImage.setImageResource(R.drawable.ic_directions_bus_black_24dp);
            if(transportsBean.getStart_location() != null) {
                vName.setText(transportsBean.getStart_location().getName());
            }else{
                vName.setText(transportsBean.getEnd_location().getName());
            }

            if (position == selected) {
                vChecked.setVisibility(View.VISIBLE);
            } else {
                vChecked.setVisibility(View.INVISIBLE);
            }
            vChecked.invalidate();
        }
    }


}
