package com.whilerain.utur.screen.driver.model;

import com.whilerain.utur.apis.BootstrapApi;

/**
 * Created by xunqun on 2017/2/6.
 */

public class TransportWraper {
    BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean bean;
    BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean fromLocation;

    public BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean getBean() {
        return bean;
    }

    public void setBean(BootstrapApi.RequestPopularTourResponse.ResultBean.TransportsBean bean) {
        this.bean = bean;
    }

    public BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(BootstrapApi.RequestPopularTourResponse.ResultBean.LocationBean fromLocation) {
        this.fromLocation = fromLocation;
    }
}
