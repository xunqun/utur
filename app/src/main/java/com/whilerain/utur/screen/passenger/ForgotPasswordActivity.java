package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.whilerain.utur.R;
import com.whilerain.utur.apis.SignUpApi;
import com.whilerain.utur.screen.ModeOptionActivity;
import com.whilerain.utur.utils.SharePrefHandler;
import com.whilerain.utur.utils.StringUtils;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.whilerain.utur.apis.SignUpApi.resendRegisterCode;

public class ForgotPasswordActivity extends AppCompatActivity {
    @BindView(R.id.country_code)
    EditText vCountryCode;

    @BindView(R.id.contact_num)
    EditText vContactNum;

    @BindView(R.id.send)
    Button vSend;

    public static void launch(Activity activity){
        activity.startActivity(new Intent(activity, ForgotPasswordActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.send)
    protected void onSend(){
        final String account = StringUtils.getAccountString(vCountryCode.getText().toString(), vContactNum.getText().toString());
        try {
            Call<SignUpApi.SendRegisterCodeResponse> call = SignUpApi.resendRegisterCode(account);
            call.enqueue(new Callback<SignUpApi.SendRegisterCodeResponse>() {
                @Override
                public void onResponse(Call<SignUpApi.SendRegisterCodeResponse> call, Response<SignUpApi.SendRegisterCodeResponse> response) {

                    if(response.body().isStatus()){
                        if(response.body().getResult().getRole().equalsIgnoreCase("Driver")){
                            SharePrefHandler.getEditor(ForgotPasswordActivity.this).putInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_DRIVER).apply();
                            com.whilerain.utur.screen.driver.ResetPasswordActivity.launch(ForgotPasswordActivity.this, account);
                        }else {
                            SharePrefHandler.getEditor(ForgotPasswordActivity.this).putInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_PASSENGER).apply();
                            ResetPasswordActivity.launch(ForgotPasswordActivity.this, account);
                        }
                    }else{
                        Toast.makeText(ForgotPasswordActivity.this, R.string.error_operation_fail, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SignUpApi.SendRegisterCodeResponse> call, Throwable t) {

                }
            });
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
