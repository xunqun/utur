package com.whilerain.utur.screen;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.viewpagerindicator.CirclePageIndicator;
import com.whilerain.utur.R;
import com.whilerain.utur.utils.SharePrefHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class TutorialActivity extends AppCompatActivity {

    private static final String FORCE_OPEN = "force_open";

    @BindView(R.id.pager)
    ViewPager vPager;

    @BindView(R.id.titles)
    CirclePageIndicator vIndicator;

    @BindView(R.id.skip)
    Button vSkip;

    @OnClick(R.id.skip)
    void onClick(){
        ModeOptionActivity.launch(this);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        boolean isForceOpen = getIntent().getBooleanExtra(FORCE_OPEN, false);
        boolean isShown = SharePrefHandler.getSharedPrefences(this, "tutorial.pref").getBoolean(SharePrefHandler.TOTURIAL_SHOWN, false);
        if(!isShown || isForceOpen){
            //continue
            // To avoid to be cleared in ModeOptionActivity, we use another file to store this value
            SharePrefHandler.getEditor(this, "tutorial.pref").putBoolean(SharePrefHandler.TOTURIAL_SHOWN, true).commit();
        }else{
            ModeOptionActivity.launch(this);
            finish();
        }

        initViews();
    }

    public static void launch(Activity activity){
        Intent it = new Intent(activity, TutorialActivity.class);
        it.putExtra(FORCE_OPEN, true);
        activity.startActivity(it);
    }

    private void initViews() {
        //Set the pager with an adapter
        vPager.setAdapter(new MyAdapter());

        //Bind the title indicator to the adapter
        vIndicator.setViewPager(vPager);
    }

    class MyAdapter extends PagerAdapter {

        Integer[] pages = new Integer[]{R.layout.tutorial0, R.layout.tutorial1, R.layout.tutorial2, R.layout.tutorial3};

        @Override
        public int getCount() {
            return pages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(TutorialActivity.this).inflate(pages[position], null);
            container.addView(view);
            if (position == 3) {
                ((Button) ButterKnife.findById(view, R.id.go)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModeOptionActivity.launch(TutorialActivity.this);
                        finish();
                    }
                });
            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            object = null;
        }
    }
}
