
package com.whilerain.utur.screen.passenger;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.whilerain.utur.R;
import com.whilerain.utur.screen.ModeOptionActivity;
import com.whilerain.utur.screen.driver.DriverActivity;
import com.whilerain.utur.utils.SharePrefHandler;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {


    public static void launchClearTop(Activity activity){
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.free_route)
    void onFreeRoute(){
        if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_PASSENGER) == ModeOptionActivity.MODE_PASSENGER){
            PassengerActivity.launchClearTask(this, 2);
        }else{
            DriverActivity.launch(this, 2);

        }
    }

    @OnClick(R.id.station)
    void onStation(){
        if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_PASSENGER) == ModeOptionActivity.MODE_PASSENGER){
            PassengerActivity.launchClearTask(this, 0);
        }else{
            DriverActivity.launch(this, 0);
        }
    }

    @OnClick(R.id.halfday)
    void onHalfday(){
        if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_PASSENGER) == ModeOptionActivity.MODE_PASSENGER){
            PassengerActivity.launchClearTask(this, 1);
        }else{
            DriverActivity.launch(this, 1);
        }
    }

    @OnClick(R.id.profile)
    void onProfile(){
        if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_PASSENGER) == ModeOptionActivity.MODE_PASSENGER){
            PassengerActivity.launchClearTask(this, 3);
        }else{
            DriverActivity.launch(this, 3);
        }
    }
}
