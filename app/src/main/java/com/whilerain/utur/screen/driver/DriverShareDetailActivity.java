

package com.whilerain.utur.screen.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;
import com.whilerain.utur.R;
import com.whilerain.utur.apis.DriverApi;
import com.whilerain.utur.screen.passenger.ShareScheduleChooserActivity;
import com.whilerain.utur.screen.passenger.HistoryActivity;
import com.whilerain.utur.screen.passenger.manager.model.travel.HalfDayTripDetailResponse;
import com.whilerain.utur.utils.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DriverShareDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.photo)
    SliderLayout vPhoto;

    @BindView(R.id.desc)
    TextView vDesc;

    @BindView(R.id.duration)
    TextView vDuration;

    @BindView(R.id.order_date)
    TextView vOrderDate;

    @BindView(R.id.order_time)
    TextView vOrderTime;

    @BindView(R.id.order_count)
    TextView vOrderCount;

    @BindView(R.id.price)
    TextView vPrice;

    static HalfDayTripDetailResponse response;
    static DriverApi.RequestListResponse.ResultBean requestBean;
    private String date;
    private String schedule;
    private int returnedPrice = 0;

    public static void launch(Activity activity, HalfDayTripDetailResponse r, DriverApi.RequestListResponse.ResultBean bean) {
        response = r;
        requestBean = bean;
        activity.startActivity(new Intent(activity, DriverShareDetailActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_share_detail);
        ButterKnife.bind(this);
        setSupportActionBar(vToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView(response);
    }

    @Override
    protected void onStop() {
        vPhoto.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Gson gson = new Gson();
        if (requestCode == ShareScheduleChooserActivity.REQUEST_SCHEDULE && resultCode == RESULT_OK) {
            schedule = data.getStringExtra("schedule");
            vOrderTime.setText(schedule);
            returnedPrice = data.getIntExtra("price", 0);
            setupPrice(returnedPrice);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView(final HalfDayTripDetailResponse response) {

        getSupportActionBar().setTitle(StringUtils.trimSpecialString(response.getResult().getName()));
        vDesc.setText(response.getResult().getIntroduction());
        vDuration.setText(String.format(getString(R.string.duration), response.getResult().getDuration() / 60f));

        try {
            String timeStr = StringUtils.utcToLocalTime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd HH:mm", requestBean.getTimerange().get(0));
            vOrderDate.setText(timeStr.substring(0, 10));
            vOrderTime.setText(timeStr.substring(11));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        vOrderCount.setText(String.valueOf(requestBean.getPassenger_count()));
        vPrice.setText(String.valueOf(requestBean.getPrice()));

        new AsyncTask<Void, Void, List<File>>(){
            List<File> pics = new ArrayList<>();
            List<String> titles = new ArrayList<String>();
            @Override
            protected List<File> doInBackground(Void... params) {

                Bitmap bitmap;
                try {

                    for (HalfDayTripDetailResponse.ResultBean.ScenesBean scenesBean : response.getResult().getScenes()) {

                        byte[] decodedString = Base64.decode(scenesBean.getPic(), Base64.DEFAULT);
                        bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                        //create a file to write bitmap data
                        File f = new File(getCacheDir(), scenesBean.getName().replace("\u0096", ""));
                        try {
                            f.createNewFile();

                            //Convert bitmap to byte array
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                            byte[] bitmapdata = bos.toByteArray();

                            //write the bytes in file
                            FileOutputStream fos = new FileOutputStream(f);
                            fos.write(bitmapdata);
                            fos.flush();
                            fos.close();
                            pics.add(f);
                            titles.add(scenesBean.getName().replace("\u0096", ""));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                return pics;
            }

            @Override
            protected void onPostExecute(List<File> files) {
                super.onPostExecute(files);
                if(DriverShareDetailActivity.this != null) {

                    for(int i = 0; i < files.size(); i++) {
                        TextSliderView textSliderView = new TextSliderView(DriverShareDetailActivity.this);
                        textSliderView
                                .description(titles.get(i))
                                .image(files.get(i));

                        vPhoto.addSlider(textSliderView);
                    }
                }
            }
        }.execute();
    }

    private void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.order_scceed))
                .setCancelable(false)
                .setPositiveButton(R.string.launch_history, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HistoryActivity.launchClearTop(DriverShareDetailActivity.this);
                    }
                })
                .create().show();
    }

    private void showErrorDialog(int msgRes) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(msgRes))
                .setPositiveButton(R.string.ok, null)
                .create().show();
    }

    @OnClick(R.id.fab)
    protected void onSend(){
        ShareComfirmAcceptOrderActivity.launch(this, requestBean.getRequest_id());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setupPrice(int price) {
        if (schedule != null) {
            int num = Integer.valueOf(vOrderCount.getText().toString());
            vPrice.setText(String.valueOf(price * num));
        } else {
            vPrice.setText("---");
        }
    }
}
