package com.whilerain.utur.screen.passenger;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.whilerain.utur.R;

import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.net.googleplace.NearbyPlacesRequest;
import com.whilerain.utur.screen.passenger.manager.model.zone.location.ZoneLocationResponse.Location;
import com.whilerain.utur.utils.SimpleDataCache;
import com.whilerain.utur.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by xunqun on 16/4/8.
 */
public class LocationListControler {


    LocationChooserActivity mActivity;
    ImageButton vNum;
    ExpandableListView vList;
    LinearLayout vListFrame;
    ExpandableAdapter adapter;
    String searchWord = "";
    List<NearbyPlacesRequest.Response.Place> placeResult = new ArrayList<>();
    DataSetObserver dataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
        }
    };

    public LocationListControler(final LocationChooserActivity activity, ImageButton btn, ExpandableListView listView, LinearLayout frame) {
        this.mActivity = activity;
        this.vNum = btn;
        this.vList = listView;
        this.vListFrame = frame;

        adapter = new ExpandableAdapter();
        vList.setAdapter(adapter);

        vList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                hideList();
                UiUtils.hideImeKeyboard(activity);
                if(groupPosition == 0) {
                    mActivity.selectLocation((Location) adapter.getChild(groupPosition, childPosition));
                }else{
                    mActivity.selectPlace((NearbyPlacesRequest.Response.Place) adapter.getChild(groupPosition, childPosition));
                }
                return true;
            }
        });

        vList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                UiUtils.hideImeKeyboard(activity);
            }
        });
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;


        if (searchWord.length() > 0) {
            NearbyPlacesRequest.build(mActivity.getLocationBean().getGps().get(0), mActivity.getLocationBean().getGps().get(1), searchWord).setApiListener(new ApiRequest.ApiListener() {

                @Override
                public void onSeccess(ApiRequest apiRequest, String body) {
                    Log.d("xunqun", "nearby seach seccess");
                    try {
                        SimpleDataCache<NearbyPlacesRequest.Response> cache = new SimpleDataCache<NearbyPlacesRequest.Response>();
                        cache.set(mActivity, NearbyPlacesRequest.Response.class, body);
                        NearbyPlacesRequest.Response response = cache.get(mActivity, NearbyPlacesRequest.Response.class);
                        placeResult = response.getResults();
                    } catch (Exception e) {
                        placeResult = new ArrayList<NearbyPlacesRequest.Response.Place>();
                        e.printStackTrace();
                    }

                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            adapter.notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onFail() {
                    Log.d("xunqun", "nearby seach fail");
                    placeResult = new ArrayList<NearbyPlacesRequest.Response.Place>();
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            adapter.notifyDataSetChanged();
                        }
                    });

                }
            }).get();
        } else {
            placeResult = new ArrayList<NearbyPlacesRequest.Response.Place>();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void notifyLocationUpdate() {

        adapter.notifyDataSetChanged();
    }

    public void showList() {

        adapter.notifyDataSetChanged();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            int cx = (vNum.getLeft() + vNum.getRight()) / 2;
            int cy = (vNum.getTop() + vNum.getBottom()) / 2;

            int finalRadius = Math.max(vListFrame.getWidth(), vListFrame.getHeight());
            vNum.setVisibility(View.INVISIBLE);
            Animator anim = ViewAnimationUtils.createCircularReveal(vListFrame, cx, cy, 0, finalRadius);
            anim.setDuration(400);
            anim.setInterpolator(new AccelerateInterpolator());
            anim.start();
            vListFrame.setVisibility(View.VISIBLE);
        } else {
            vListFrame.setVisibility(View.VISIBLE);
            vNum.setVisibility(View.INVISIBLE);
        }


    }

    public void hideList() {


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            int cx = (vNum.getLeft() + vNum.getRight()) / 2;
            int cy = (vNum.getTop() + vNum.getBottom()) / 2;
            int initialRadius = Math.max(vListFrame.getWidth(), vListFrame.getHeight());
            Animator anim = null;
            anim = ViewAnimationUtils.createCircularReveal(vListFrame, cx, cy, initialRadius, 0);

            anim.setDuration(400);
            anim.setInterpolator(new AccelerateInterpolator());
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    vListFrame.setVisibility(View.INVISIBLE);
                    vNum.setVisibility(View.VISIBLE);
                }
            });
            anim.start();
        } else {
            vListFrame.setVisibility(View.INVISIBLE);
            vNum.setVisibility(View.VISIBLE);
        }
    }

    private class ExpandableAdapter extends BaseExpandableListAdapter {
        List<Location> locations = new ArrayList<>();
        List<NearbyPlacesRequest.Response.Place> places = new ArrayList<>();

        public ExpandableAdapter() {
            initData();
        }

        void initData() {
            List<Location> list;
            try {
                list = new ArrayList<>(mActivity.getLocationList());
            } catch (NullPointerException e) {
                list = new ArrayList<>();
            }

            searchKeyword(list);


            places = new ArrayList<>(placeResult);
//            searchPlaceKeyword(list1);
        }

        @Override
        public void notifyDataSetInvalidated() {
            initData();
            super.notifyDataSetInvalidated();
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
            super.registerDataSetObserver(observer);
        }

        private void searchKeyword(List<Location> list) {
            if (searchWord.length() > 0) {
                locations.clear();
                for (Location l : list) {
                    if (l.getName().indexOf(searchWord) > -1) {
                        locations.add(l);
                    }
                }
            } else {
                try {
                    locations = new ArrayList<>(mActivity.getLocationList());
                } catch (NullPointerException e) {
                    list = new ArrayList<>();
                }
            }
        }

        private void searchPlaceKeyword(List<NearbyPlacesRequest.Response.Place> list) {
            if (searchWord.length() > 0) {
                places.clear();
                for (NearbyPlacesRequest.Response.Place l : list) {
                    if (l.getName().indexOf(searchWord) > -1) {
                        places.add(l);
                    }
                }
            } else {
                try {
                    places = new ArrayList<>(placeResult);
                } catch (NullPointerException e) {
                    places = new ArrayList<>();
                }
            }
        }

        @Override
        public void notifyDataSetChanged() {
            initData();
            super.notifyDataSetChanged();
        }

        @Override
        public int getGroupCount() {
            return 2;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            if (groupPosition == 0) {
                return locations.size();
            } else {
                return places.size();
            }
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupPosition;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            if (groupPosition == 0) {
                return locations.get(childPosition);
            } else {
                return places.get(childPosition);
            }
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.layout_search_group_item, null);
            }

            if (groupPosition == 0) {

                TextView textView = ButterKnife.findById(convertView, R.id.text);
                textView.setText(String.format(mActivity.getString(R.string.utur_search_result), String.valueOf(locations.size())));
                return convertView;
            } else {
                TextView textView = ButterKnife.findById(convertView, R.id.text);
                textView.setText(String.format(mActivity.getString(R.string.google_places_result), String.valueOf(places.size())));
                return convertView;
            }
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mActivity).inflate(R.layout.layout_location_item, null);
            }
            if(groupPosition == 0){
                TextView title = ButterKnife.findById(convertView, R.id.title);
                TextView address = ButterKnife.findById(convertView, R.id.address);
                String name = ((Location)getChild(0, childPosition)).getName();
                String addr = ((Location)getChild(0, childPosition)).getAddress();
                Log.d("xunqun", "getItem(position).getName() " + name);
                title.setText(name);
                address.setText(addr);
            }else{
                TextView title = ButterKnife.findById(convertView, R.id.title);
                TextView address = ButterKnife.findById(convertView, R.id.address);
                Log.d("xunqun", "google place: " + ((NearbyPlacesRequest.Response.Place)getChild(1, childPosition)).getName());
                title.setText(((NearbyPlacesRequest.Response.Place)getChild(1, childPosition)).getName());
                address.setText(((NearbyPlacesRequest.Response.Place)getChild(1, childPosition)).getVicinity());
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }


}
