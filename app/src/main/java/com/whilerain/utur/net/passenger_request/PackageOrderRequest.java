package com.whilerain.utur.net.passenger_request;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/6.
 */
public class PackageOrderRequest extends ApiRequest {

    private int travel_id;
    private String uuid;
    private String date;
    private int passenger_count;
    private String contact_phone;

    private PackageOrderRequest(int travId, String uuid, String date, int passengerCount, String contact_phone){

        this.travel_id = travId;
        this.uuid = uuid;
        this.date = date;
        this.passenger_count = passengerCount;
        this.contact_phone = contact_phone;
    }

    public static PackageOrderRequest build(int travId, String uuid, String date,  int passengerCount, String contact_phone){
        return new PackageOrderRequest(travId, uuid, date, passengerCount, contact_phone);

    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + PASSENGER_REQUEST + "/package");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("travel_id", travel_id);
            json.put("uuid", uuid);
            json.put("contact_phone", contact_phone);
            json.put("date", date);
            json.put("passenger_count", passenger_count);

            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
