package com.whilerain.utur.net.order;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/18.
 */
public class CancelRequestRequest extends ApiRequest {
    String id;
    public static CancelRequestRequest build(String id) {
        return new CancelRequestRequest(id);
    }

    private CancelRequestRequest(String id){
        this.id = id;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + PASSENGER_REQUEST + "/" + id + "/cancel");
    }

    @Override
    public RequestBody getRequestBody() {
        return RequestBody.create(JSON, "{}");

    }
}
