package com.whilerain.utur.net.googleplace;

import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.utils.SimpleDataCache;

import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/7/25.
 */
public class NearbyPlacesRequest extends ApiRequest {
    private final String keyword;
    double lat, lng;
    String place_key = "AIzaSyAfZtaQoRlWm_CE3Q1HeRe1w8dMFUHEe6U";

    public static NearbyPlacesRequest build(double lat, double lng, String keyword){
        return new NearbyPlacesRequest(lat, lng, keyword);
    }

    NearbyPlacesRequest(double lat, double lng, String keyword){
        this.lat = lat;
        this.lng = lng;
        this.keyword = keyword;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse("https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=zh_TW&location=" + lat + "," + lng + "&radius=5000&name=" + keyword + "&key=" + place_key);
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }

    public static class Response extends SimpleDataCache.CachePoJo{

        /**
         * results : [{"geometry":{"location":{"lat":-33.867551,"lng":151.200817}},"id":"b0277cade7696e575824681aba949d68814f9efe","name":"Sydney New Year's Eve Cruises","opening_hours":{"open_now":false},"photos":[{"height":813,"html_attributions":["asdf"],"photo_reference":"CoQBcwAAAI1RyenFQ","width":1300}],"place_id":"ChIJ__8_hziuEmsR27ucFXECfOg","reference":"CoQBcQAAAM6aQUZErzTJ","scope":"GOOGLE","types":["travel_agency","restaurant","food","point_of_interest","establishment"],"vicinity":"32 The Promenade, King Street Wharf 5, Sydney Nsw 2000, Sydney"}]
         * status : OK
         */

        private String status;
        /**
         * geometry : {"location":{"lat":-33.867551,"lng":151.200817}}
         * id : b0277cade7696e575824681aba949d68814f9efe
         * name : Sydney New Year's Eve Cruises
         * opening_hours : {"open_now":false}
         * photos : [{"height":813,"html_attributions":["asdf"],"photo_reference":"CoQBcwAAAI1RyenFQ","width":1300}]
         * place_id : ChIJ__8_hziuEmsR27ucFXECfOg
         * reference : CoQBcQAAAM6aQUZErzTJ
         * scope : GOOGLE
         * types : ["travel_agency","restaurant","food","point_of_interest","establishment"]
         * vicinity : 32 The Promenade, King Street Wharf 5, Sydney Nsw 2000, Sydney
         */

        private List<Place> results;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Place> getResults() {
            return results;
        }

        public void setResults(List<Place> results) {
            this.results = results;
        }

        public static class Place {
            /**
             * location : {"lat":-33.867551,"lng":151.200817}
             */

            private GeometryBean geometry;
            private String id;
            private String name;
            /**
             * open_now : false
             */

            private OpeningHoursBean opening_hours;
            private String place_id;
            private String reference;
            private String scope;
            private String vicinity;
            /**
             * height : 813
             * html_attributions : ["asdf"]
             * photo_reference : CoQBcwAAAI1RyenFQ
             * width : 1300
             */

            private List<PhotosBean> photos;
            private List<String> types;

            public GeometryBean getGeometry() {
                return geometry;
            }

            public void setGeometry(GeometryBean geometry) {
                this.geometry = geometry;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public OpeningHoursBean getOpening_hours() {
                return opening_hours;
            }

            public void setOpening_hours(OpeningHoursBean opening_hours) {
                this.opening_hours = opening_hours;
            }

            public String getPlace_id() {
                return place_id;
            }

            public void setPlace_id(String place_id) {
                this.place_id = place_id;
            }

            public String getReference() {
                return reference;
            }

            public void setReference(String reference) {
                this.reference = reference;
            }

            public String getScope() {
                return scope;
            }

            public void setScope(String scope) {
                this.scope = scope;
            }

            public String getVicinity() {
                return vicinity;
            }

            public void setVicinity(String vicinity) {
                this.vicinity = vicinity;
            }

            public List<PhotosBean> getPhotos() {
                return photos;
            }

            public void setPhotos(List<PhotosBean> photos) {
                this.photos = photos;
            }

            public List<String> getTypes() {
                return types;
            }

            public void setTypes(List<String> types) {
                this.types = types;
            }

            public static class GeometryBean {
                /**
                 * lat : -33.867551
                 * lng : 151.200817
                 */

                private LocationBean location;

                public LocationBean getLocation() {
                    return location;
                }

                public void setLocation(LocationBean location) {
                    this.location = location;
                }

                public static class LocationBean {
                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }

            public static class OpeningHoursBean {
                private boolean open_now;

                public boolean isOpen_now() {
                    return open_now;
                }

                public void setOpen_now(boolean open_now) {
                    this.open_now = open_now;
                }
            }

            public static class PhotosBean {
                private int height;
                private String photo_reference;
                private int width;
                private List<String> html_attributions;

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public String getPhoto_reference() {
                    return photo_reference;
                }

                public void setPhoto_reference(String photo_reference) {
                    this.photo_reference = photo_reference;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public List<String> getHtml_attributions() {
                    return html_attributions;
                }

                public void setHtml_attributions(List<String> html_attributions) {
                    this.html_attributions = html_attributions;
                }
            }
        }
    }
}
