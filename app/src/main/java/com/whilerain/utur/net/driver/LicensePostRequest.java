package com.whilerain.utur.net.driver;

import android.util.Log;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import java.io.File;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/27.
 */
public class LicensePostRequest extends ApiRequest {

    protected String uuid;
    protected String base64Img;

    private LicensePostRequest(String uuid, String base64Img) {
        this.uuid = uuid;
        this.base64Img = base64Img;
    }

    public static LicensePostRequest build(String uuid, String base64Img) {
        return new LicensePostRequest(uuid, base64Img);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/" + uuid + "/license");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("license", base64Img);
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
