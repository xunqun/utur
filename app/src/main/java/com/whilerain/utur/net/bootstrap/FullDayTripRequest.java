package com.whilerain.utur.net.bootstrap;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/15.
 */
public class FullDayTripRequest extends ApiRequest {

    public static FullDayTripRequest build(){
        return new FullDayTripRequest();
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + BOOTSTRAP + "/package");
    }

    @Override
    public RequestBody getRequestBody() {
        JSONObject json = new JSONObject();
        RequestBody formBody = RequestBody.create(JSON, json.toString());

        return formBody;
    }
}
