package com.whilerain.utur.net.zone;

import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.screen.passenger.LocationChooserActivity;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/7.
 */
public class ZoneLocationRequest extends ApiRequest {

    private int zoneId;
    private int range = 100;
    private double lng;
    private double lat;

    private ZoneLocationRequest(int zoneId, double lat, double lng){
        this.lat = lat;
        this.lng = lng;
        this.zoneId = zoneId;
        this.range = LocationChooserActivity.DISTANCE_TO_REFRESH;
    }

    public static ZoneLocationRequest build(int zoneId, double lat, double lng){
        return new ZoneLocationRequest(zoneId, lat, lng);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + ZONE + "/" + zoneId + "?" + String.format("r=%s&long=%s&lat=%s", range, lng, lat));
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
