package com.whilerain.utur.net.bootstrap;


import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/3/29.
 */
public class TransportRequest extends ApiRequest {

    public static TransportRequest build(){
        return new TransportRequest();
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + BOOTSTRAP + "/transport");
    }

    @Override
    public RequestBody getRequestBody() {
        JSONObject json = new JSONObject();
        RequestBody formBody = RequestBody.create(JSON, json.toString());

        return formBody;
    }
}
