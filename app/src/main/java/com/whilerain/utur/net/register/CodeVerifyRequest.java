package com.whilerain.utur.net.register;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/3/31.
 */
public class CodeVerifyRequest extends ApiRequest {

    private String uuid;
    private String code;

    public static CodeVerifyRequest build(String uuid, String code){
        return new CodeVerifyRequest(uuid, code);
    }

    private CodeVerifyRequest(String uuid, String code){
        this.uuid = uuid;
        this.code = code;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + SIGNUP + "/verify");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("uuid", uuid);
            json.put("code", code);
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
