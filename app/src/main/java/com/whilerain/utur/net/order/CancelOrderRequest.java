package com.whilerain.utur.net.order;

import com.whilerain.utur.net.ApiRequest;
import com.whilerain.utur.utils.SimpleDataCache;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/18.
 */
public class CancelOrderRequest extends ApiRequest {
    String id;
    public static CancelOrderRequest build(String id) {
        return new CancelOrderRequest(id);
    }

    private CancelOrderRequest(String id){
        this.id = id;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + ORDER + "/" + id + "/cancel");
    }

    @Override
    public RequestBody getRequestBody() {
        return RequestBody.create(JSON, "{}");

    }
}
