package com.whilerain.utur.net.passenger_request;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/6.
 */
public class TransportOrderRequest extends ApiRequest {
    private String transport_id;
    private int travel_id;
    private String uuid;
    private String date;
    private int passenger_count;
    private int start_location;
    private int end_location;
    private String start_location_custom;
    private String end_location_custom;
    private String contact_phone;


    private TransportOrderRequest(String transId, int travId, String uuid, String date, int passengerCount, int from, int to, String start_location_custom, String end_location_custom, String contact_phone){
        this.transport_id = transId;
        this.travel_id = travId;
        this.uuid = uuid;
        this.date = date;
        this.passenger_count = passengerCount;
        this.start_location = from;
        this.end_location = to;
        this.start_location_custom = start_location_custom;
        this.end_location_custom = end_location_custom;
        this.contact_phone = contact_phone;
    }

    public static TransportOrderRequest build(String transId, int travId, String uuid, String date, int passengerCount, int from, int to, String start_location_custom, String end_location_custom, String contact_phone){
        return new TransportOrderRequest(transId, travId, uuid, date, passengerCount, from, to, start_location_custom, end_location_custom, contact_phone);

    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + PASSENGER_REQUEST + "/transport");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("transport_id", transport_id);
            json.put("travel_id", travel_id);
            json.put("uuid", uuid);
            json.put("contact_phone", contact_phone);
            json.put("date", date);
            json.put("passenger_count", passenger_count);
            if(start_location_custom == null || start_location_custom.length() == 0){
                json.put("start_location", start_location);
            }else {
                json.put("start_location_custom", new JSONObject(start_location_custom));
            }

            if(end_location_custom == null || end_location_custom.length() == 0){
                json.put("end_location", end_location);
            }else {
                json.put("end_location_custom", new JSONObject(end_location_custom));
            }

            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
