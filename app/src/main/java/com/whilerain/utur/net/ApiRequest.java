package com.whilerain.utur.net;

import android.os.AsyncTask;
import android.util.Log;



import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by xunqun on 15/9/18.
 */
public abstract class ApiRequest {
    public static final MediaType MEDIA_TYPE_PNG
            = MediaType.parse("image/png; charset=utf-8");
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static final String TAG = "ApiRequest";
    public static final String HOST = "http://api.utur.tw:80";
    public static final String BOOTSTRAP = "/v1/bootstrap";
    public static final String PASSENGER = "/v1/passenger";
    public static final String DRIVER = "/v1/driver";
    public static final String SIGNUP = "/v1/signup";
    public static final String TRAVEL = "/v1/travel";
    public static final String ORDER = "/v1/order";
    public static final String PASSENGER_REQUEST = "/v1/request";
    public static final String ZONE = "/v1/zone";


    private OkHttpClient client = new OkHttpClient();
    private ApiListener listener;

    public interface ApiListener {
        void onSeccess(ApiRequest apiRequest, String body);
        void onFail();
    }

    public abstract HttpUrl getUrl();

    public abstract RequestBody getRequestBody();

    public ApiRequest setApiListener(ApiListener l) {
        listener = l;
        return this;
    }

    public void get(){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                Request request = new Request.Builder()
                        .url(getUrl())
                        .get()
                        .build();
                Log.d(TAG, request.toString());
                try {
                    Response response = client.newCall(request).execute();
                    Log.d(TAG, response.toString());
                    if (!response.isSuccessful()) {
                        listener.onFail();
                        throw new IOException("Unexpected code " + response);
                    } else {
                        String body = response.body().string();
                        Log.d(TAG, body);
                        if(listener != null) {
                            listener.onSeccess(ApiRequest.this, body);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFail();

                }
                return null;
            }
        }.execute();
    }



    public void post() {

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                Request request = new Request.Builder()
                        .url(getUrl())
                        .post(getRequestBody())
                        .build();

                Log.d(TAG, "url: " + getUrl() + " body: ");
                try {
                    Response response = client.newCall(request).execute();
                    Log.d(TAG, response.toString());
                    if (!response.isSuccessful()) {
                        listener.onFail();
                        throw new IOException("Unexpected code " + response);
                    } else {
                        String body = response.body().string();
                        Log.d(TAG, body);
                        if(listener != null) {
                            listener.onSeccess(ApiRequest.this, body);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFail();

                }
                return null;
            }
        }.execute();

    }

    public void put(){
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                Request request = new Request.Builder()
                        .url(getUrl())
                        .put(getRequestBody())
                        .build();

                Log.d(TAG, "url: " + getUrl() + " body: ");
                try {
                    Response response = client.newCall(request).execute();
                    Log.d(TAG, response.toString());
                    if (!response.isSuccessful()) {
                        listener.onFail();
                        throw new IOException("Unexpected code " + response);
                    } else {
                        String body = response.body().string();
                        Log.d(TAG, body);
                        if(listener != null) {
                            listener.onSeccess(ApiRequest.this, body);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFail();

                }
                return null;
            }
        }.execute();
    }


}
