package com.whilerain.utur.net.bootstrap;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/15.
 */
public class HalfDayTripRequest extends ApiRequest {

    public static HalfDayTripRequest build(){
        return new HalfDayTripRequest();
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + BOOTSTRAP + "/share");
    }

    @Override
    public RequestBody getRequestBody() {
        JSONObject json = new JSONObject();
        RequestBody formBody = RequestBody.create(JSON, json.toString());

        return formBody;
    }
}
