package com.whilerain.utur.net.order;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/8/9.
 */
public class FinishOrderRequest extends ApiRequest {

    private final String id;

    public static FinishOrderRequest build(String id) {
        return new FinishOrderRequest(id);
    }

    private FinishOrderRequest(String id){
        this.id = id;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + ORDER + "/" + id + "/finish");
    }

    @Override
    public RequestBody getRequestBody() {
        return RequestBody.create(JSON, "{}");
    }
}
