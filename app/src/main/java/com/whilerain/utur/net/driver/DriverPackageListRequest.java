package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/5/2.
 */
public class DriverPackageListRequest extends ApiRequest {
    int id;



    public static DriverPackageListRequest build(int id){
        return new DriverPackageListRequest(id);
    }

    public DriverPackageListRequest(int id){
        this.id = id;
    }
    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/request/list/package/" + id);
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
