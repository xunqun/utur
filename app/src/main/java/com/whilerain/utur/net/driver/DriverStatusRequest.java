package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/5/1.
 */
public class DriverStatusRequest extends ApiRequest {

    protected String uuid;

    private DriverStatusRequest(String uuid) {
        this.uuid = uuid;
    }

    public static DriverStatusRequest build(String uuid) {
        return new DriverStatusRequest(uuid);
    }


    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/" + uuid + "/status");
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
