package com.whilerain.utur.net.bootstrap;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/22.
 */
public class CarDetailRequest extends ApiRequest{
    int carId;

    public static CarDetailRequest build(int carId){
        return new CarDetailRequest(carId);
    }

    CarDetailRequest(int id){
        carId = id;
    }


    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + BOOTSTRAP + "/car/" + carId);
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
