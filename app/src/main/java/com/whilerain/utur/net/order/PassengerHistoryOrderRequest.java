package com.whilerain.utur.net.order;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/24.
 */
public class PassengerHistoryOrderRequest extends ApiRequest {
    private String uuid;

    public static PassengerHistoryOrderRequest build(String uuid) {
        return new PassengerHistoryOrderRequest(uuid);
    }

    private PassengerHistoryOrderRequest(String uuid){
        this.uuid = uuid;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + ORDER + "/list/passenger/" + uuid);
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
