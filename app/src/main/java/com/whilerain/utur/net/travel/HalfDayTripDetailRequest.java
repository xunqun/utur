package com.whilerain.utur.net.travel;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/16.
 */
public class HalfDayTripDetailRequest extends ApiRequest {
    private int id;

    private HalfDayTripDetailRequest(int id){
        this.id = id;
    }

    public static HalfDayTripDetailRequest build(int id){
        return new HalfDayTripDetailRequest(id);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + TRAVEL + "/share/" + String.valueOf(id) + "/detail");

    }

    @Override
    public RequestBody getRequestBody() {
        JSONObject json = new JSONObject();
        RequestBody formBody = RequestBody.create(JSON, json.toString());
        return formBody;
    }
}
