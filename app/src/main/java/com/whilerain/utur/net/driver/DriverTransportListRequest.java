package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/5/2.
 */
public class DriverTransportListRequest extends ApiRequest {
    int id;



    public static DriverTransportListRequest build(int id){
        return new DriverTransportListRequest(id);
    }

    public DriverTransportListRequest(int id){
        this.id = id;
    }
    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/request/list/transport/" + id);
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
