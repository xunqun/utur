package com.whilerain.utur.net.register;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/3/31.
 */
public class PassengerRegisterRequest extends ApiRequest {

    private String phone;
    private String email;
    private String name;
    private String password;

    public static PassengerRegisterRequest build(String phone, String email, String name, String password){
        return new PassengerRegisterRequest(phone, email, name, password);
    }

    private PassengerRegisterRequest(String phone, String email, String name, String password){
        this.phone = phone;
        this.email = email;
        this.name = name;
        this.password = password;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + SIGNUP + "/passenger");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("phone", phone);
            json.put("email", email);
            json.put("name", name);
            json.put("password", password);
            json.put("lang", "zh-hant");
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
