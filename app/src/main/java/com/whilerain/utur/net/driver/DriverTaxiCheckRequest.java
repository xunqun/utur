package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/27.
 */
public class DriverTaxiCheckRequest extends ApiRequest {

    protected String uuid;
    protected String request_id;
    protected int car_id;

    private DriverTaxiCheckRequest(String uuid, String request_id,  int car_id) {
        this.uuid = uuid;
        this.request_id = request_id;
        this.car_id = car_id;
    }

    public static DriverTaxiCheckRequest build(String uuid, String request_id,  int car_id) {
        return new DriverTaxiCheckRequest(uuid, request_id, car_id);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/request/taxi");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("uuid", uuid);
            json.put("request_id", request_id);
            json.put("car_id", car_id);
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
