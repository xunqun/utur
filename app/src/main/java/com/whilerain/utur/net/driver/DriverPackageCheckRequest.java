package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/27.
 */
public class DriverPackageCheckRequest extends ApiRequest {

    protected String uuid;
    protected String request_id;
    protected String order_id;
    protected int car_id;

    private DriverPackageCheckRequest(String uuid, String request_id, String order_id, int car_id) {
        this.uuid = uuid;
        this.request_id = request_id;
        this.order_id = order_id;
        this.car_id = car_id;
    }

    public static DriverPackageCheckRequest build(String uuid, String request_id, String order_id, int car_id) {
        return new DriverPackageCheckRequest(uuid, request_id, order_id, car_id);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/request/package/check");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("uuid", uuid);
            json.put("request_id", request_id);
            json.put("order_id", order_id);
            json.put("car_id", car_id);
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
