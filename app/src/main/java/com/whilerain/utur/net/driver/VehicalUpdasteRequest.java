package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/5/1.
 */
public class VehicalUpdasteRequest extends ApiRequest {

    protected String uuid;
    protected String model;
    protected String licenseplate;
    protected String license;
    protected String licensepic;
    protected String picture;
    protected int seat;

    private VehicalUpdasteRequest(String uuid, String model, String licenseplate, String license, String licensepic, String picture, int seat ) {
        this.uuid = uuid;
        this.model = model;
        this.licenseplate = licenseplate;
        this.license = license;
        this.licensepic = licensepic;
        this.picture = picture;
        this.seat = seat;
    }

    public static VehicalUpdasteRequest build(String uuid, String model, String licenseplate, String license, String licensepic, String picture, int seat ) {
        return new VehicalUpdasteRequest(uuid, model, licenseplate, license, licensepic, picture, seat);
    }


    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/" + uuid + "/car");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();

            JSONObject car = new JSONObject();
            car.put("model", model);
            car.put("licenseplate", licenseplate);
            car.put("license", license);
            car.put("licensepic", picture);
            car.put("picture", licensepic);
            car.put("seat", seat);

            json.put("car", car);
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
