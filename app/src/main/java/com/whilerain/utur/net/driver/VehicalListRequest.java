package com.whilerain.utur.net.driver;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/5/1.
 */
public class VehicalListRequest extends ApiRequest {

    protected String uuid;

    private VehicalListRequest(String uuid) {
        this.uuid = uuid;
    }

    public static VehicalListRequest build(String uuid ) {
        return new VehicalListRequest(uuid);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + DRIVER + "/" + uuid + "/car");
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
