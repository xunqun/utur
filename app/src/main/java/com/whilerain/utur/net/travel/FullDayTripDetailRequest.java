package com.whilerain.utur.net.travel;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/6/16.
 */
public class FullDayTripDetailRequest extends ApiRequest {
    private int id;

    private FullDayTripDetailRequest(int id){
        this.id = id;
    }

    public static FullDayTripDetailRequest build(int id){
        return new FullDayTripDetailRequest(id);
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + TRAVEL + "/package/" + String.valueOf(id) + "/detail");

    }

    @Override
    public RequestBody getRequestBody() {
        JSONObject json = new JSONObject();
        RequestBody formBody = RequestBody.create(JSON, json.toString());
        return formBody;
    }
}
