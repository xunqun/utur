package com.whilerain.utur.net.login;

import com.whilerain.utur.net.ApiRequest;

import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/3/30.
 */
public class PassengerLoginRequest extends ApiRequest {

    String account;
    String password;

    public static PassengerLoginRequest build(String account, String password) {
        return new PassengerLoginRequest(account, password);
    }

    private PassengerLoginRequest(String account, String password) {
        this.account = account;
        this.password = password;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + PASSENGER + "/login");
    }

    @Override
    public RequestBody getRequestBody() {
        try {
            JSONObject json = new JSONObject();
            json.put("account", account);
            json.put("password", password);
            RequestBody formBody = RequestBody.create(JSON, json.toString());
            return formBody;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
