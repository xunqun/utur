package com.whilerain.utur.net.schedule;

import android.support.annotation.NonNull;

import com.whilerain.utur.net.ApiRequest;
import org.json.JSONObject;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;

/**
 * Created by xunqun on 16/4/5.
 */
public class TransportScheduleRequest extends ApiRequest {
    public int id;
    public String date;

    public TransportScheduleRequest(int id, @NonNull String date) {
        this.id = id;
        this.date = date;
    }

    @Override
    public HttpUrl getUrl() {
        return HttpUrl.parse(HOST + TRAVEL + "/transport/schedule?" + String.format("travel=%s&date=%s", id, date));
    }

    @Override
    public RequestBody getRequestBody() {
        JSONObject json = new JSONObject();
        RequestBody formBody = RequestBody.create(JSON, json.toString());
        return formBody;
    }

    public static TransportScheduleRequest build(int id, String date) {
        return new TransportScheduleRequest(id, date);
    }
}
