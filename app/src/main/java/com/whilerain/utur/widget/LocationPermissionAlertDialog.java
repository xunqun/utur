package com.whilerain.utur.widget;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.whilerain.utur.R;
import com.whilerain.utur.utils.UiUtils;

/**
 * Created by shawn on 2017/4/25.
 */

public class LocationPermissionAlertDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog d = new AlertDialog.Builder(getContext())
                .setTitle(R.string.permission_requirement)
                .setMessage(R.string.permission_storage_description)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UiUtils.startInstalledAppDetailsActivity(getActivity());
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        return d;
    }
}
