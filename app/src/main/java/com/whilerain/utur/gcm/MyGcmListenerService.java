
/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.whilerain.utur.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.whilerain.utur.R;
import com.whilerain.utur.screen.ModeOptionActivity;
import com.whilerain.utur.screen.passenger.HistoryActivity;
import com.whilerain.utur.utils.SharePrefHandler;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        try {
            Bundle message = data.getBundle("notification");
            Log.d(TAG, "From: " + from);
            Log.d(TAG, "Notification: " + message.toString());
            for (String s : message.keySet()) {

                Log.i(TAG, s + " : " + message.get(s));
            }

            sendNotification(message.getString("title"), message.getString("body"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void sendNotification(String title, String body) {
        Intent intent;
        if(SharePrefHandler.getSharedPrefences(this).getInt(SharePrefHandler.APP_MODE, ModeOptionActivity.MODE_PASSENGER) == ModeOptionActivity.MODE_PASSENGER){
            intent = new Intent(this, HistoryActivity.class);
        }else{
            intent = new Intent(this, com.whilerain.utur.screen.driver.HistoryActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(title)
                .setContentText(body)
                .setColor(getColor(R.color.colorPrimary))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
