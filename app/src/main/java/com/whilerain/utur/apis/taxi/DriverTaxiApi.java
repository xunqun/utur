package com.whilerain.utur.apis.taxi;

import com.whilerain.utur.apis.BaseApi;

import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by shawn on 2017/4/25.
 */

public class DriverTaxiApi extends BaseApi {
    public interface TaxiRequestListService {
        @GET("v1/driver/request/list/taxi")
        Call<TaxiRequestListResponse> send(@Query("lat") double lat, @Query("lng") double lng);
    }

    public static Call<TaxiRequestListResponse> requestList(double endLat, double endLng) throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        return retrofit.create(TaxiRequestListService.class).send(endLat, endLng);
    }

    public static class TaxiRequestListResponse{

        /**
         * status : true
         * result : [{"id":"83","request_id":"201704259999999990006","date":"2017-04-25T00:00:00.000Z","timerange":["2017-04-25T14:00:00.000Z","2017-04-26T00:00:00.000Z"],"start_location_custom":{"gps":[22.984972,120.178408],"address":"台南市安平區健康一街71巷19號\u201361號"},"end_location_custom":{"gps":[22.98500397312708,120.219058433468],"address":"台南市東區長榮路一段234巷17號"},"contact_phone":"886921278507","passenger":2,"passenger_count":2,"status":"accepted","is_deleted":false,"type":2,"createdAt":"2017-04-25T13:36:43.365Z","passenger_name":"vv","phone":"886921278507","distance":4869.19237889},{"id":"81","request_id":"201704259999999990004","date":"2017-04-25T00:00:00.000Z","timerange":["2017-04-25T15:00:00.000Z","2017-04-25T17:00:00.000Z"],"start_location_custom":{"gps":[22.9742175,120.22177734374995],"address":"701台灣台南市東區中華東路三段330號"},"end_location_custom":{"gps":[22.999608,120.16418399999999],"address":"708台灣臺南市安平區"},"contact_phone":"886952117393","passenger":9,"passenger_count":4,"status":"queue","is_deleted":false,"type":2,"createdAt":"2017-04-25T06:37:03.172Z","passenger_name":"shawn","phone":"886952117393","distance":35.24874123}]
         */

        private boolean status;
        private List<TaxiResultBean> result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public List<TaxiResultBean> getResult() {
            return result;
        }

        public void setResult(List<TaxiResultBean> result) {
            this.result = result;
        }

        public static class TaxiResultBean {

            private String id;
            private String request_id;
            private String date;
            private int total_cost;
            private LocationBean start_location_custom;
            private LocationBean end_location_custom;
            private String contact_phone;
            private int passenger;
            private int passenger_count;
            private String status;
            private boolean is_deleted;
            private int type;
            private String createdAt;
            private String passenger_name;
            private String phone;
            private double distance;
            private int price;
            private List<String> timerange;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRequest_id() {
                return request_id;
            }

            public void setRequest_id(String request_id) {
                this.request_id = request_id;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(int total_cost) {
                this.total_cost = total_cost;
            }

            public LocationBean getStart_location_custom() {
                return start_location_custom;
            }

            public void setStart_location_custom(LocationBean start_location_custom) {
                this.start_location_custom = start_location_custom;
            }

            public LocationBean getEnd_location_custom() {
                return end_location_custom;
            }

            public void setEnd_location_custom(LocationBean end_location_custom) {
                this.end_location_custom = end_location_custom;
            }

            public String getContact_phone() {
                return contact_phone;
            }

            public void setContact_phone(String contact_phone) {
                this.contact_phone = contact_phone;
            }

            public int getPassenger() {
                return passenger;
            }

            public void setPassenger(int passenger) {
                this.passenger = passenger;
            }

            public int getPassenger_count() {
                return passenger_count;
            }

            public void setPassenger_count(int passenger_count) {
                this.passenger_count = passenger_count;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public boolean isIs_deleted() {
                return is_deleted;
            }

            public void setIs_deleted(boolean is_deleted) {
                this.is_deleted = is_deleted;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getPassenger_name() {
                return passenger_name;
            }

            public void setPassenger_name(String passenger_name) {
                this.passenger_name = passenger_name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public double getDistance() {
                return distance;
            }

            public void setDistance(double distance) {
                this.distance = distance;
            }

            public Object getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public List<String> getTimerange() {
                return timerange;
            }

            public void setTimerange(List<String> timerange) {
                this.timerange = timerange;
            }

            public static class LocationBean {
                /**
                 * gps : [22.975592500000012,120.22052734374992]
                 * address : 701台灣台南市東區崇明十街39號
                 */

                private String address;
                private List<Double> gps;

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public List<Double> getGps() {
                    return gps;
                }

                public void setGps(List<Double> gps) {
                    this.gps = gps;
                }
            }


        }
    }
}
