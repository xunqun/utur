package com.whilerain.utur.apis;

import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by xunqun on 2017/2/5.
 */

public class DriverApi extends BaseApi{

    public interface RequestListService {
        @GET("v1/driver/request/list")
        Call<RequestListResponse> send(@Query("no") int no, @Query("lat") double lat, @Query("lng") double lng);
    }

    public static Call<RequestListResponse> RequestList(int no, double lat, double lng) throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(RequestListService.class).send(no, lat, lng);
    }

    public static class RequestListResponse{


        /**
         * status : true
         * result : [{"id":"22","request_id":"01230000109000007","transport_id":"000010900","travel":1,"date":"2017-03-01T00:00:00.000Z","seq":null,"timerange":["2017-03-01T01:00:00.000Z","2017-03-01T03:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "},"end_location":{"gps":[21.974295,120.754095],"id":150,"name":"墾丁瑪沙露湖畔旅館","zone_id":2,"address":"屏東縣恆春鎮南灣路649號"},"passenger":1,"passenger_count":3,"contact_phone":null,"order":null,"status":"queue","is_deleted":false,"createdAt":"2017-01-22T16:52:18.434Z","category":"transport","price":450,"discount":1,"time":"09:00"},{"id":"21","request_id":"01220000109000001","transport_id":"000010900","travel":3,"date":"2017-01-25T00:00:00.000Z","seq":null,"timerange":["2017-01-25T01:00:00.000Z","2017-01-25T03:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "},"end_location":{"gps":[21.974295,120.754095],"id":150,"name":"墾丁瑪沙露湖畔旅館","zone_id":2,"address":"屏東縣恆春鎮南灣路649號"},"passenger":1,"passenger_count":4,"contact_phone":null,"order":null,"status":"queue","is_deleted":false,"createdAt":"2017-01-21T17:13:14.280Z","category":"transport","price":450,"discount":1,"time":"09:00"},{"id":"20","request_id":"01210000113000008","transport_id":"000011300","travel":1,"date":"2017-02-12T00:00:00.000Z","seq":null,"timerange":["2017-02-12T05:00:00.000Z","2017-02-12T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "},"end_location_custom":{"gps":[123.1,29.9],"address":"NO. 188"},"passenger":null,"passenger_count":3,"contact_phone":"0921322122","order":null,"status":"queue","is_deleted":false,"createdAt":"2017-01-21T11:33:17.045Z","category":"transport","price":450,"discount":0.9,"time":"13:00"},{"id":"19","request_id":"01210000113000007","transport_id":"000011300","travel":1,"date":"2017-02-12T00:00:00.000Z","seq":null,"timerange":["2017-02-12T05:00:00.000Z","2017-02-12T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "},"end_location_custom":{"gps":[123.1,29.9],"address":"NO. 188"},"passenger":null,"passenger_count":3,"contact_phone":"0921322122","order":null,"status":"queue","is_deleted":false,"createdAt":"2017-01-21T11:33:03.237Z","category":"transport","price":450,"discount":0.9,"time":"13:00"},{"id":"18","request_id":"01210000113000006","transport_id":"000011300","travel":1,"date":"2017-02-12T00:00:00.000Z","seq":null,"timerange":["2017-02-12T05:00:00.000Z","2017-02-12T07:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "},"end_location_custom":{"gps":[123.1,29.9],"address":"NO. 188"},"passenger":null,"passenger_count":3,"contact_phone":"0921322122","order":null,"status":"queue","is_deleted":false,"createdAt":"2017-01-21T11:32:58.437Z","category":"transport","price":450,"discount":0.9,"time":"13:00"},{"id":"17","request_id":"01210000109000007","transport_id":"000010900","travel":1,"date":"2017-02-01T00:00:00.000Z","seq":null,"timerange":["2017-02-01T01:00:00.000Z","2017-02-01T03:00:00.000Z"],"start_location":{"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "},"end_location":{"gps":[21.974295,120.754095],"id":150,"name":"墾丁瑪沙露湖畔旅館","zone_id":2,"address":"屏東縣恆春鎮南灣路649號"},"passenger":1,"passenger_count":2,"contact_phone":null,"order":null,"status":"queue","is_deleted":false,"createdAt":"2017-01-21T11:21:30.003Z","category":"transport","price":450,"discount":1,"time":"09:00"}]
         */

        private boolean status;
        private List<ResultBean> result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public List<ResultBean> getResult() {
            return result;
        }

        public void setResult(List<ResultBean> result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * id : 22
             * request_id : 01230000109000007
             * transport_id : 000010900
             * travel : 1
             * date : 2017-03-01T00:00:00.000Z
             * seq : null
             * timerange : ["2017-03-01T01:00:00.000Z","2017-03-01T03:00:00.000Z"]
             * start_location : {"gps":[22.6871528,120.3088077],"id":475,"name":"高鐵左營站(客運)","zone_id":1,"address":"高雄市左營區高鐵路105-107號 "}
             * end_location : {"gps":[21.974295,120.754095],"id":150,"name":"墾丁瑪沙露湖畔旅館","zone_id":2,"address":"屏東縣恆春鎮南灣路649號"}
             * passenger : 1
             * passenger_count : 3
             * contact_phone : null
             * order : null
             * status : queue
             * is_deleted : false
             * createdAt : 2017-01-22T16:52:18.434Z
             * category : transport
             * price : 450
             * discount : 1
             * time : 09:00
             * end_location_custom : {"gps":[123.1,29.9],"address":"NO. 188"}
             */

            private String id;
            private String request_id;
            private String transport_id;
            private int travel;
            private String date;
            private Object seq;
            private LocationBean start_location;
            private LocationBean start_location_custom;
            private LocationBean end_location;
            private LocationBean end_location_custom;
            private int passenger;
            private int passenger_count;
            private Object contact_phone;
            private Object order;
            private String status;
            private boolean is_deleted;
            private String createdAt;
            private String category;
            private int price;
            private float discount;
            private String time;
            private List<String> timerange;
            private String name;

            private int total_cost;
            private int type;
            private String passenger_name;
            private String phone;
            private String distance;

            public void setName(String name) {
                this.name = name;
            }

            public int getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(int total_cost) {
                this.total_cost = total_cost;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getPassenger_name() {
                return passenger_name;
            }

            public void setPassenger_name(String passenger_name) {
                this.passenger_name = passenger_name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRequest_id() {
                return request_id;
            }

            public void setRequest_id(String request_id) {
                this.request_id = request_id;
            }

            public String getTransport_id() {
                return transport_id;
            }

            public void setTransport_id(String transport_id) {
                this.transport_id = transport_id;
            }

            public int getTravel() {
                return travel;
            }

            public void setTravel(int travel) {
                this.travel = travel;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public Object getSeq() {
                return seq;
            }

            public void setSeq(Object seq) {
                this.seq = seq;
            }

            public LocationBean getStart_location() {
                return start_location;
            }

            public void setStart_location(LocationBean start_location) {
                this.start_location = start_location;
            }

            public LocationBean getEnd_location() {
                return end_location;
            }

            public void setEnd_location(LocationBean end_location) {
                this.end_location = end_location;
            }

            public int getPassenger() {
                return passenger;
            }

            public void setPassenger(int passenger) {
                this.passenger = passenger;
            }

            public int getPassenger_count() {
                return passenger_count;
            }

            public void setPassenger_count(int passenger_count) {
                this.passenger_count = passenger_count;
            }

            public Object getContact_phone() {
                return contact_phone;
            }

            public void setContact_phone(Object contact_phone) {
                this.contact_phone = contact_phone;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public boolean isIs_deleted() {
                return is_deleted;
            }

            public void setIs_deleted(boolean is_deleted) {
                this.is_deleted = is_deleted;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public float getDiscount() {
                return discount;
            }

            public void setDiscount(int discount) {
                this.discount = discount;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public LocationBean getEnd_location_custom() {
                return end_location_custom;
            }

            public void setEnd_location_custom(LocationBean end_location_custom) {
                this.end_location_custom = end_location_custom;
            }

            public List<String> getTimerange() {
                return timerange;
            }

            public void setTimerange(List<String> timerange) {
                this.timerange = timerange;
            }

            public LocationBean getStart_location_custom() {
                return start_location_custom;
            }

            public void setStart_location_custom(LocationBean start_location_custom) {
                this.start_location_custom = start_location_custom;
            }

            public boolean is_deleted() {
                return is_deleted;
            }

            public void setDiscount(float discount) {
                this.discount = discount;
            }

            public String getName() {
                return name;
            }

            public static class LocationBean {
                /**
                 * gps : [22.6871528,120.3088077]
                 * id : 475
                 * name : 高鐵左營站(客運)
                 * zone_id : 1
                 * address : 高雄市左營區高鐵路105-107號
                 */

                private int id;
                private String name;
                private int zone_id;
                private String address;
                private List<Double> gps;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getZone_id() {
                    return zone_id;
                }

                public void setZone_id(int zone_id) {
                    this.zone_id = zone_id;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public List<Double> getGps() {
                    return gps;
                }

                public void setGps(List<Double> gps) {
                    this.gps = gps;
                }
            }
        }
    }


}
