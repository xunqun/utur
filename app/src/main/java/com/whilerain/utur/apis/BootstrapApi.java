package com.whilerain.utur.apis;

import com.whilerain.utur.utils.SimpleDataCache;

import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by xunqun on 2017/1/22.
 */

public class BootstrapApi extends BaseApi {
    public interface RequestPopularTourService {
        @GET("v1/bootstrap/transport")
        Call<RequestPopularTourResponse> send();
    }

    public static Call<RequestPopularTourResponse> RequestPopularTour() throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(RequestPopularTourService.class).send();
    }

    public static class RequestPopularTourResponse extends SimpleDataCache.CachePoJo {


        /**
         * status : true
         * result : {"from_start_location_transports":[{"category":"station","start_location":{"id":460,"name":"屏東火車站","gps":[22.669306,120.4862022],"address":"屏東市光復路43號 ","zone_id":2},"transports":[{"transports_id":5,"category":"station","end_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]},{"category":"station","start_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2},"transports":[{"transports_id":6,"category":"station","end_location":{"id":460,"name":"屏東火車站","gps":[22.669306,120.4862022],"address":"屏東市光復路43號 ","zone_id":2}},{"transports_id":4,"category":"station","end_location":{"id":478,"name":"高雄火車站","gps":[22.639623,120.3021087],"address":"高雄市三民區建國二路318號","zone_id":1}},{"transports_id":2,"category":"station","end_location":{"id":475,"name":"高鐵左營站(客運)","gps":[22.6871528,120.3088077],"address":"高雄市左營區高鐵路105-107號 ","zone_id":1}}]},{"category":"station","start_location":{"id":475,"name":"高鐵左營站(客運)","gps":[22.6871528,120.3088077],"address":"高雄市左營區高鐵路105-107號 ","zone_id":1},"transports":[{"transports_id":1,"category":"station","end_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]},{"category":"station","start_location":{"id":478,"name":"高雄火車站","gps":[22.639623,120.3021087],"address":"高雄市三民區建國二路318號","zone_id":1},"transports":[{"transports_id":3,"category":"station","end_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]}],"from_end_location_transports":[{"category":"station","end_location":{"id":460,"name":"屏東火車站","gps":[22.669306,120.4862022],"address":"屏東市光復路43號 ","zone_id":2},"transports":[{"transports_id":6,"category":"station","start_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]},{"category":"station","end_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2},"transports":[{"transports_id":5,"category":"station","start_location":{"id":460,"name":"屏東火車站","gps":[22.669306,120.4862022],"address":"屏東市光復路43號 ","zone_id":2}},{"transports_id":3,"category":"station","start_location":{"id":478,"name":"高雄火車站","gps":[22.639623,120.3021087],"address":"高雄市三民區建國二路318號","zone_id":1}},{"transports_id":1,"category":"station","start_location":{"id":475,"name":"高鐵左營站(客運)","gps":[22.6871528,120.3088077],"address":"高雄市左營區高鐵路105-107號 ","zone_id":1}}]},{"category":"station","end_location":{"id":475,"name":"高鐵左營站(客運)","gps":[22.6871528,120.3088077],"address":"高雄市左營區高鐵路105-107號 ","zone_id":1},"transports":[{"transports_id":2,"category":"station","start_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]},{"category":"station","end_location":{"id":478,"name":"高雄火車站","gps":[22.639623,120.3021087],"address":"高雄市三民區建國二路318號","zone_id":1},"transports":[{"transports_id":4,"category":"station","start_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]}]}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }



        public static class ResultBean {
            private List<FromStartLocationTransportsBean> from_start_location_transports;
            private List<FromEndLocationTransportsBean> from_end_location_transports;

            public List<FromStartLocationTransportsBean> getFrom_start_location_transports() {
                return from_start_location_transports;
            }

            public void setFrom_start_location_transports(List<FromStartLocationTransportsBean> from_start_location_transports) {
                this.from_start_location_transports = from_start_location_transports;
            }

            public List<FromEndLocationTransportsBean> getFrom_end_location_transports() {
                return from_end_location_transports;
            }

            public void setFrom_end_location_transports(List<FromEndLocationTransportsBean> from_end_location_transports) {
                this.from_end_location_transports = from_end_location_transports;
            }

            public static class FromStartLocationTransportsBean {
                /**
                 * category : station
                 * start_location : {"id":460,"name":"屏東火車站","gps":[22.669306,120.4862022],"address":"屏東市光復路43號 ","zone_id":2}
                 * transports : [{"transports_id":5,"category":"station","end_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]
                 */

                private String category;
                private LocationBean start_location;
                private List<TransportsBean> transports;

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public LocationBean getStart_location() {
                    return start_location;
                }

                public void setStart_location(LocationBean start_location) {
                    this.start_location = start_location;
                }

                public List<TransportsBean> getTransports() {
                    return transports;
                }

                public void setTransports(List<TransportsBean> transports) {
                    this.transports = transports;
                }


            }

            public static class LocationBean {
                /**
                 * id : 460
                 * name : 屏東火車站
                 * gps : [22.669306,120.4862022]
                 * address : 屏東市光復路43號
                 * zone_id : 2
                 */

                private int id;
                private String name;
                private String address;
                private int zone_id;
                private List<Double> gps;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public int getZone_id() {
                    return zone_id;
                }

                public void setZone_id(int zone_id) {
                    this.zone_id = zone_id;
                }

                public List<Double> getGps() {
                    return gps;
                }

                public void setGps(List<Double> gps) {
                    this.gps = gps;
                }
            }

            public static class TransportsBean {
                /**
                 * transports_id : 5
                 * category : station
                 * end_location : {"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}
                 */

                private int transports_id;
                private String category;
                private LocationBean end_location;
                private LocationBean start_location;

                public int getTransports_id() {
                    return transports_id;
                }

                public void setTransports_id(int transports_id) {
                    this.transports_id = transports_id;
                }

                public LocationBean getStart_location() {
                    return start_location;
                }

                public void setStart_location(LocationBean start_location) {
                    this.start_location = start_location;
                }

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public LocationBean getEnd_location() {
                    return end_location;
                }

                public void setEnd_location(LocationBean end_location) {
                    this.end_location = end_location;
                }


            }

            public static class FromEndLocationTransportsBean {
                /**
                 * category : station
                 * end_location : {"id":460,"name":"屏東火車站","gps":[22.669306,120.4862022],"address":"屏東市光復路43號 ","zone_id":2}
                 * transports : [{"transports_id":6,"category":"station","start_location":{"id":470,"name":"墾丁","gps":[21.9456877,120.7972097],"address":"屏東縣恆春鎮墾丁路237號","zone_id":2}}]
                 */

                private String category;
                private LocationBean end_location;
                private List<TransportsBean> transports;

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public LocationBean getEnd_location() {
                    return end_location;
                }

                public void setEnd_location(LocationBean end_location) {
                    this.end_location = end_location;
                }

                public List<TransportsBean> getTransports() {
                    return transports;
                }

                public void setTransports(List<TransportsBean> transports) {
                    this.transports = transports;
                }


            }
        }
    }
}
