package com.whilerain.utur.apis.taxi;

import com.whilerain.utur.apis.BaseApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by xunqun on 2017/2/5.
 */

public class PassengerTaxiApi extends BaseApi {

    public interface TaxiQueryPriceService {
        @POST("v1/request/taxi/cost-estimate")
        Call<TaxiQueryPriceResponse> send(@Body RequestBody body);
    }

    public interface TaxiRequestService {
        @POST("v1/request/taxi")
        Call<TaxiRequestResponse> send(@Body RequestBody body);
    }


    public static Call<TaxiRequestResponse> taxiRequest(String uuid, String time, int pCount, String startAddr, double startLat, double startLng, String endAddr, double endLat, double endLng, int total_cost, String contact_phone) throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", uuid);
        jsonObject.put("time", time);
        jsonObject.put("passenger_count", pCount);

        JSONObject startLocation = new JSONObject();
        startLocation.put("address", startAddr);
        JSONArray startgps = new JSONArray();
        startgps.put(startLat);
        startgps.put(startLng);
        startLocation.put("gps", startgps);

        jsonObject.put("start_location", startLocation);

        JSONObject endLocation = new JSONObject();
        endLocation.put("address", endAddr);
        JSONArray endgps = new JSONArray();
        endgps.put(endLat);
        endgps.put(endLng);
        endLocation.put("gps", endgps);

        jsonObject.put("end_location", endLocation);
        jsonObject.put("total_cost", total_cost);
        jsonObject.put("contact_phone", contact_phone);

        RequestBody body = RequestBody.create(JSON,jsonObject.toString());

        return retrofit.create(TaxiRequestService.class).send(body);
    }

    public static Call<TaxiQueryPriceResponse> taxiQueryPrice(String uuid, String time, String startAddr, double startLat, double startLng, String endAddr, double endLat, double endLng) throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", uuid);
        jsonObject.put("time", time);

        JSONObject startLocation = new JSONObject();
        startLocation.put("address", startAddr);
        JSONArray startgps = new JSONArray();
        startgps.put(startLat);
        startgps.put(startLng);
        startLocation.put("gps", startgps);

        jsonObject.put("start_location", startLocation);

        JSONObject endLocation = new JSONObject();
        endLocation.put("address", endAddr);
        JSONArray endgps = new JSONArray();
        endgps.put(endLat);
        endgps.put(endLng);
        endLocation.put("gps", endgps);

        jsonObject.put("end_location", endLocation);

        RequestBody body = RequestBody.create(JSON, jsonObject.toString());

        return retrofit.create(TaxiQueryPriceService.class).send(body);
    }

    public static class TaxiRequestResponse{

        /**
         * status : true
         * result : {"reqeust_id":"201704249999999990001"}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * reqeust_id : 201704249999999990001
             */

            private String reqeust_id;
            private int code;
            private String message;

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getReqeust_id() {
                return reqeust_id;
            }

            public void setReqeust_id(String reqeust_id) {
                this.reqeust_id = reqeust_id;
            }
        }
    }

    public static class TaxiQueryPriceResponse {


        /**
         * status : true
         * result : {"distance_estimate":239,"cost_per_ppl":0,"cost_per_km":20,"total_cost":null}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * distance_estimate : 239
             * cost_per_ppl : 0
             * cost_per_km : 20
             * total_cost : null
             */

            private int distance_estimate;
            private int cost_per_ppl;
            private int cost_per_km;
            private Object total_cost;
            private int code;
            private String message;

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public int getDistance_estimate() {
                return distance_estimate;
            }

            public void setDistance_estimate(int distance_estimate) {
                this.distance_estimate = distance_estimate;
            }

            public int getCost_per_ppl() {
                return cost_per_ppl;
            }

            public void setCost_per_ppl(int cost_per_ppl) {
                this.cost_per_ppl = cost_per_ppl;
            }

            public int getCost_per_km() {
                return cost_per_km;
            }

            public void setCost_per_km(int cost_per_km) {
                this.cost_per_km = cost_per_km;
            }

            public Object getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(Object total_cost) {
                this.total_cost = total_cost;
            }
        }
    }


}
