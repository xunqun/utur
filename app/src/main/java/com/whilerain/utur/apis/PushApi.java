package com.whilerain.utur.apis;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by xunqun on 2017/2/15.
 */

public class PushApi extends BaseApi {
    public interface RequestUploadTokenService {
        @POST("v1/gcm/{uuid}/token")
        Call<RequestUploadTokenResponse> send(@Path("uuid") String uuid, @Body RequestBody body);
    }

    public interface RequestDeleteTokenService {
        @DELETE("v1/gcm/{uuid}/token")
        Call<RequestDeleteTokenResponse> send(@Path("uuid") String uuid, @Query("token") String token);
    }

    public static Call<RequestDeleteTokenResponse> requestDeleteToken(String uuid, String deviceToken) throws JSONException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(RequestDeleteTokenService.class).send(uuid, deviceToken);
    }

    public static Call<RequestUploadTokenResponse> RequestUploadToken(String uuid, String deviceToken) throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", deviceToken);

        RequestBody body = RequestBody.create(JSON,jsonObject.toString());

        return retrofit.create(RequestUploadTokenService.class).send(uuid, body);
    }

    public static class RequestDeleteTokenResponse{

        /**
         * status : false
         * result : {"code":400,"message":"Token is not exist."}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * code : 400
             * message : Token is not exist.
             */

            private int code;
            private String message;

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }

    public static class RequestUploadTokenResponse{

        /**
         * status : false
         * result : {"code":400,"message":"This token was added before."}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * code : 400
             * message : This token was added before.
             */

            private int code;
            private String message;

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }
}
