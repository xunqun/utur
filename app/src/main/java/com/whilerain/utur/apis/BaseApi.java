package com.whilerain.utur.apis;

import okhttp3.MediaType;

/**
 * Created by xunqun on 2017/1/17.
 */

public class BaseApi {
    public static final MediaType MEDIA_TYPE_PNG
            = MediaType.parse("image/png; charset=utf-8");
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static final String TAG = "ApiRequest";
    public static final String HOST = "http://api.utur.tw:80";
    public static final String BOOTSTRAP = "/v1/bootstrap";
    public static final String PASSENGER = "/v1/passenger";
    public static final String DRIVER = "/v1/driver";
    public static final String SIGNUP = "/v1/signup";
    public static final String TRAVEL = "/v1/travel";
    public static final String ORDER = "/v1/order";
    public static final String PASSENGER_REQUEST = "/v1/request";
    public static final String ZONE = "/v1/zone";
}
