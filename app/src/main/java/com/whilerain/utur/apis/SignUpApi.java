package com.whilerain.utur.apis;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by xunqun on 2017/1/17.
 */

public class SignUpApi extends BaseApi {


    public interface ResendRegisterCodeService {
        @POST("v1/signup/resend-register-code")
        Call<SendRegisterCodeResponse> send(@Body RequestBody body);
    }

    public interface ResetPasswordService{
        @POST("v1/signup/reset-password")
        Call<ResetPasswordResponse> send(@Body RequestBody body);
    }

    public static Call<ResetPasswordResponse> resetPasswordCode(String phone, String code, String password) throws JSONException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", phone);
        jsonObject.put("code", code);
        jsonObject.put("password", password);

        RequestBody body = RequestBody.create(JSON,jsonObject.toString());
        return retrofit.create(ResetPasswordService.class).send(body);
    }

    public static Call<SendRegisterCodeResponse> resendRegisterCode(String account) throws JSONException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phone", account);
        RequestBody body = RequestBody.create(JSON,jsonObject.toString());
        return retrofit.create(ResendRegisterCodeService.class).send(body);
    }

    public static class ResetPasswordResponse{

        /**
         * status : true
         * result : {"uuid":"8ae13a08-82e4-4270-aa40-b7a2e0120c53","phone":"886952117393","email":"lu.shangchiun@gmail.com"}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * uuid : 8ae13a08-82e4-4270-aa40-b7a2e0120c53
             * phone : 886952117393
             * email : lu.shangchiun@gmail.com
             */

            private String uuid;
            private String phone;
            private String email;
            private String message;
            private int code;

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }
        }
    }

    public static class SendRegisterCodeResponse{

        /**
         * status : false
         * result : {"code":400,"message":"Error"}
         */

        private boolean status;
        private ResultBean result;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public ResultBean getResult() {
            return result;
        }

        public void setResult(ResultBean result) {
            this.result = result;
        }

        public static class ResultBean {
            /**
             * code : 400
             * message : Error
             */

            private int code;
            private String message;
            private String uuid;
            private String phone;
            private String role;

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }
}
