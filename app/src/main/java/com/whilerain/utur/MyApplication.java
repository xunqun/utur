package com.whilerain.utur;

import android.app.Application;

/**
 * Created by xunqun on 16/6/20.
 */
public class MyApplication extends Application {
    private static MyApplication instance;
    public MyApplication(){
        instance = this;
    }

    public static MyApplication getInstance(){
        return instance;
    }
}
